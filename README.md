# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is JarHeads' implementation of the MASE group project

### Links to Documents ###
* [Sprint 1 Documents](https://drive.google.com/drive/u/0/folders/0BwAimrhKlUTJRDdRdFNYMDExQ0U/0BwAimrhKlUTJfmt1YkFwZFYwS3FFNDNUemhRLWxKc3JmX29BWURkcUhja1NMZ1RXdl9nOWc/0B-jx9Qb3p7_AfnVhTG5nMFhYNHZVMXAtSHdTNXBoUmZfb1RXQ3ZNdU1fQjEwYkJCY21SRVE?ltmpl=drive)

* [Use Case Specification Document](https://docs.google.com/document/d/1DbPtfsD4XceopRa-l3tBKot1pk5dA5N8BOtdNrhykEw/edit)

* [Test Document](https://docs.google.com/spreadsheets/d/10A2BTWW3-amFx5QGae_YtVyfVDIO8OE0uPWunzBWVHk/edit#gid=0)
* [JIRA](https://maseait.atlassian.net/secure/Dashboard.jspa)

### Directory Setup on YOUR local machine###
Add **{your Jboss 7.1.1 Final dir}/Temp/ErrorLogs**  

 
 and 

**{your Jboss 7.1.1 Final dir}/Temp/Import**   

folders.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact