var renderStory12Graph = function(date) {

	var displayDate = date.split('&'); 
	var startDateAndTime = displayDate[0]+'/'+displayDate[1]+'/'+displayDate[2]+' '+
	displayDate[3]+':'+displayDate[4];
	var endDateAndTime = displayDate[5]+'/'+displayDate[6]+'/'+displayDate[7]+' '+
	displayDate[8]+':'+displayDate[9];

	var myTitle = "Top 10 IMSI's with call failures";
	var mySubtitle = 'Date between: ' + startDateAndTime+' and '+endDateAndTime;
	var yAxisLabel = "Number of Failures";
	var xAxisLabel = "IMSI's";
	var level1URL = 'http://localhost:8080/Jarheads/rest/basedata/Top10ImsiWithFailureList/'+date
	var level2URL = 'http://localhost:8080/Jarheads/rest/basedata/failureCountPerImsiPerDateGroupByEvent/'
		getBaseData(function(myDataList) {
			baseData = processData(myDataList);

			$('#graphResultsTab').empty();
			var myGraph = $('#graphResultsTab')
			.highcharts(
					{
						chart : {
							type : 'column'
						},
						title : {
							text : myTitle
						},

						subtitle : {
							text : mySubtitle
						},
						xAxis : {
							type : 'category',
							title : {
								text : xAxisLabel
							},
							labels : {
								rotation : -45,
								style : {
									fontSize : '10px',
									fontFamily : 'Verdana, sans-serif'
								}
							}

						},
						yAxis : {
							min : 0,
							title : {
								text : yAxisLabel
							}
						},

						legend : {
							enabled : false
						},

						plotOptions : {
							series : {
								borderWidth : 0,
								dataLabels : {
									enabled : true
								},
								point : {
									events : {
										click : function(event) {
											var chart = this.series.chart;
											console
											.log(this.series.chart.title.textStr)
											var imsi = this.name;
											$
											.ajax({
												url : level2URL+date+'&'+imsi,
												success : function(
														data) {
													console
													.log('url is: '
															+ this.url)
															swapSeries(
																	chart,
																	"Failures",
																	data);
												},
												dataType : "json"
											});
											//}
										}
									}
								}
							}
						},

						series : [ {
							name : 'Things',
							colorByPoint : true,
							data : baseData
						} ],
						exporting: {
							buttons: {
								customButton: {
									text: 'Go back',
									onclick: function () {
										$('#graphResultsTab').empty();
										renderStory12Graph(date)
									}
								}
							}
						}
					});



		});

	function swapSeries(chart, name, data) {
		chart.series[0].remove();
		chart.addSeries({
			data : data,
			name : name,
			colorByPoint : true
		});
	}

	$('#iniChart').click(function() {
		swapSeries(Highcharts.charts[0], 'Failures', baseData);
	});

	function getBaseData(callback) {
		$
		.ajax({
			url : level1URL,
			success : function(data) {
				var myDataList = []
				var list = data == null ? []
				: (data instanceof Array ? data : [ data ]);
				$.each(list, function(index, myData) {
					myDataList
					.push([ myData[0], myData[1] ]); // Description,
					// failures,
					// Devicemodel,
				});
				console.log(myDataList);

				if (typeof callback === "function")
					callback(myDataList);
			},
			dataType : "json"
		});

	}

	function processData(myDataList) {
		var myLevel1Data = myDataList// [['INITIAL CTXT SETUP-CSFB LICENSE
		// MISSING',5,'GX-28'],['UE CTXT
		// RELEASE-AUTHENTICATION
		// FAILURE',3,'GX-28']] // sample lvl1
		// data coming from WS
		// http://localhost:8080/Jarheads/rest/basedata/failuresPerDeviceModel/GX-28
		// loop through level 1 data and build the drildowns data object
		var drilldownsData = {};
		var convertedLevel1DataArrayToObject = [];
		myLevel1Data.forEach(function(entry) {
			// for building initial data series
			convertedLevel1DataArrayToObject.push({
				name : entry[0],
				y : entry[1],
				drilldown : true

			});
		});

		return convertedLevel1DataArrayToObject;

	}

}