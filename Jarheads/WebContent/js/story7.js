/***  Query: List all failures over a given time period. */

var makeStory7=function(){
	$('#tableResults').empty();
	$('#queryPanel').empty();			
	buildGenericCalendarHTML();
	$('#queryPanel').append("<span class=\"glyphicon glyphicon-info-sign\" id=\"dateInfo\" data-toggle=\"tooltip\" ");
	$('#placeWhereQueriesAreDisplayed').hide();
	buildTabsNoGraph();
	$('#tableResultsTab').empty();
	$('#queryPanel').empty();			
	buildGenericCalendarHTML();
	$('#queryPanel').append("<span class=\"glyphicon glyphicon-info-sign\" id=\"dateInfo\" data-toggle=\"tooltip\" "
			+"data-placement=\"bottom\""
			+"title=\"This query returns all the IMSI's that had a failure during the " +
					"defined time period.\">"
			+"</span>");
	$('#tableResultsTab').append("<div class=\"table-responsive\">"
			+"		<table id=\"imsiFailuresOverGivenTimePeriod\" class=\"table table-striped table-bordered\">"
			+"			<thead>"
			+"				<tr>"
			+"										<th>Date Time</th>"
			+"										<th>IMSI</th>"
			+"					<th>Failure class</th>"
			+"					<th>UE Type</th>"
			+"					<th>Duration</th>"
			+"					<th>Cause Code</th>"
			+"				</tr>"
			+"			</thead>"
			+"			<tbody>"
			+"			</tbody>"

			+"			</table>"
			+"			</div>"
	)	
	$('#imsiFailuresOverGivenTimePeriod').hide();
	buildCalendar();
}


var renderList7 = function(datesInfo) {

	$('#imsiFailuresOverGivenTimePeriod').dataTable( {
		"bFilter" : true,
		"sPaginationType" : "full_numbers",
		"bProcessing" :true,
		"sProcessing": "<img src='images/loading.gif'>",
		"bServerSide" : true,
		aoColumns : [
		             {"mData" : "date"},
		             {"mData" : "imsi"},
		             {"mData" : "failureClass" },
		             {"mData" : "ueType"},
		             {"mData" : "duration"},
		             {"mData" : "causeCode"}
		             ],
		             sAjaxSource : "rest/basedata/ImsiFailuresForGivenTimeRange/"+datesInfo,
	});	

	console.log('rendering invalid data List finished.');
	clearFileSelectionField(); 
	$('#ImsiFailuresOverGivenPeriodList').show();
	$('#placeWhereQueriesAreDisplayed').show();
};
