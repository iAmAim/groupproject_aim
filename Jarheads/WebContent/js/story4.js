var distinctList;

var makeStory4=function(){
	$('#queryPanel').empty();
	$('#queryPanel').append(
			"<p id=\"story7Description\">" 	
			+"<p id=\"query4Validation\" class=\"text-danger\"></p>"
			+"<label for=\"searchKey\">Enter IMSI:</label> "
			+"<input type=\"text\" id=\"searchKey\" list=\"s4dropdown\" name=\"searchKey\">" 
			+"<datalist id=s4dropdown></datalist>"
			+"<span class=\"glyphicon glyphicon-info-sign\" id=\"dateInfo\" data-toggle=\"tooltip\" "
			+"data-placement=\"bottom\""
			+"title=\"This query returns the event id, cause code and description for all failures affecting a defined IMSI.\">"
			+"</span>"
			+"<button type=\"button\" class=\"btn btn-info\" id=\"searchButton\">"
			+" Search</button>"
			+"<br>"
	)
};

var story4Tbl = function(){
	$('#placeWhereQueriesAreDisplayed').hide();
	buildTabsNoGraph();
	$('#tableResultsTab').empty();
	$('#tableResultsTab').append("<div class=\"table-responsive\">"
			+"		<table id=\"query4\" class=\"table table-striped table-bordered\">"
			+"			<thead>"
			+"				<tr>"
			+"					<th>IMSI</th>"
			+"					<th>Event Id</th>"
			+"					<th>Cause Code</th>"
			+"					<th>Description</th>"
			+"				</tr>"
			+"			</thead>"
			+"			<tbody id=\"dataTableBody\">"
			+""
			+"			</tbody>"
			+"			</table>"
			+"	</div>"
	)
	$('#query4').hide();		
}


var search = function(searchKey) {
	console.log('In search method')
	if (searchKey == '') {
		$("#query4Validation").text(
		"Search fields cannot be blank.").show();
	}
	else if(isNaN(searchKey)){
		$("#query4Validation").text("IMSI's contain only numbers").show();
	}
	else if(searchKey.length!=15){
		$("#query4Validation").text("Input must be a valid IMSI number (15 digits)").show();
	}
	else{
		$("#query4Validation").hide();
		findByImsi(searchKey);
	}
};


var findByImsi = function(searchKey) {
	console.log('findByImsi: ' +searchKey);
	$.ajax({
		type: 'GET',
		url: 'http://localhost:8080/Jarheads/rest/basedata/findImsi/'+searchKey,
		dataType: "json",
		success: renderImsi,
	});
};


var renderImsi = function(data) {
	console.log('renderImsi');

	var dataTable = $('#query4').dataTable();
	dataTable.fnClearTable();
	dataTable.fnDraw();
	dataTable.fnDestroy();
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);

	if(list.length==0){
		showNoDataPopUp();
		$('#noDataModalLabel').text("No failures for this IMSI during this time period");
	}
	else{
		$.each(list,function(index, baseData) {	
			console.log("baseData"+baseData)
			var t = $('#query4').DataTable();
			t.row.add(
					[baseData[0],baseData[1],baseData[2], baseData[3]
					]).draw();
		});

		console.log('rendering imsi finished.');
		clearFileSelectionField();
		window.scrollTo(0, 0);
		$('#query4').show();
		$('#placeWhereQueriesAreDisplayed').show();	
	}
};



var findDistinctIMSIList = function(){
	console.log('findDistinctImsi');
	$.ajax({
		type: 'GET',
		url: 'http://localhost:8080/Jarheads/rest/basedata/findDistinctImsi/',
		dataType: "json",
		success: renderDistinctImsi
	});
};

var renderDistinctImsi = function(data) {
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$.each(list,function(index, errorData) {	
		distinctList+='<option value='+errorData+'>';
	});
	$('#s4dropdown').append(distinctList);
};




