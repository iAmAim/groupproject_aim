var renderStory11Graph = function(date) {

	var myTitle = "Top 10 Market/Operator/Cell Id Story 11"
	var mySubtitle = 'Date between: ' + date
	var yAxisLabel = "Failures"
	var level1URL = 'http://localhost:8080/Jarheads/rest/basedata/top10FailuresWithDate/'+date
	var level2URL = 'http://localhost:8080/Jarheads/rest/basedata/imsiPerMarketOperatorPerCellPerDate/'
	getBaseData(function(myDataList) {
		baseData = processData(myDataList);
	
		$('#graphResultsTab').empty();
		
		var count = 0;
		var myGraph = $('#graphResultsTab')
				.highcharts(
						{
							chart : {
								type : 'column'
							},
							title : {
								text : myTitle
							},

							subtitle : {
								text : mySubtitle
							},
							xAxis : {
								max: 10,
								type : 'category',
								labels : {
									rotation : -45,
									style : {
										fontSize : '10px',
										fontFamily : 'Verdana, sans-serif'
									}
								}

							},
							yAxis : {
								min : 0,
								title : {
									text : yAxisLabel
								}
							},
				             scrollbar: {
					 	            enabled:true,
					 				barBackgroundColor: 'gray',
					 				barBorderRadius: 7,
					 				barBorderWidth: 0,
					 				buttonBackgroundColor: 'gray',
					 				buttonBorderWidth: 0,
					 				buttonArrowColor: 'yellow',
					 				buttonBorderRadius: 7,
					 				rifleColor: 'yellow',
					 				trackBackgroundColor: 'white',
					 				trackBorderWidth: 1,
					 				trackBorderColor: 'silver',
					 				trackBorderRadius: 7
					 		    },

							legend : {
								enabled : false
							},
				

							plotOptions : {
								
								series : {
									turboThreshold : 10000,
									borderWidth : 0,
									dataLabels : {
										enabled : true
									},
									point : {
										events : {
											click : function(event) {
							                    console.log(this);
							                    console.log(this.name);
							                    console.log(this.y);
							                    console.log(this.drilldown);
												
												var chart = this.series.chart;
												console.log(this.series.chart.title.textStr)
												var market = this.market;
												var operator = this.operator;
												var cellId = this.cellId;
												var drillDownURL = "";
												var currentLevel = this.level;
												if(this.level == 1){
													console.log('Current level is 1. Going to  Level 2..')
													 drillDownURL = level2URL+date+'&'+cellId+'&'+operator
												}
								
													$
													.ajax({
														url : drillDownURL,
														success : function(
																data) {
															console
																	.log('url is: '
																			+ this.url)
															swapSeries(
																	chart,
																	"Failures",
																	data,++currentLevel);
														},
														dataType : "json"
													});
											}
										}
									}
								}
							},

							series : [ {
								name : 'Failures',
								colorByPoint : true,
								data : baseData
							} ],
							 exporting: {
							        buttons: {
							            customButton: {
							                text: 'Go back',
							                onclick: function () {
							                	$('#graphResultsTab').empty();
							                	renderStory11Graph(date)
							                }
							            }
							        }
							    }
						});
		
			

	});
	/**
	 * @param name = name of the Y axis on hover */
	function swapSeries(chart, name, data,currentLevel) {
		console.log('current level is now: ' + currentLevel)
		chart.series[0].remove();
		
		var currentLevelData = [];
		if(currentLevel ==2){
			data.forEach(function(entry) {
				console.log('entry: '+entry[0]+','+ entry[1])
				
				currentLevelData.push({
					name : entry[0],
					y : entry[1],
					drilldown : true,
					level: currentLevel
			});
		  });
			chart.setTitle(null, { text: 'Imsis affected in this Cell '});
			
			chart.options.plotOptions.turboThreshold = 10000;
		}		
		else{
			currentLevelData = data;
		}
		
		chart.xAxis[0].update({
	    	max: 20
        });

		chart.addSeries({
			data : currentLevelData,
			name : name,
			colorByPoint : true
		});
		
	}

	$('#iniChart').click(function() {
		swapSeries(Highcharts.charts[0], 'Failures', baseData);
	});

	function getBaseData(callback) {
		$
				.ajax({
					url : level1URL,
					success : function(data) {
						var myDataList = []
						var list = data == null ? []
								: (data instanceof Array ? data : [ data ]);
						$.each(list, function(index, myData) {
							myDataList
									.push([ myData[0], myData[3],myData[1],myData[2] ]); // Description,
							// failures,
							// Devicemodel,
						});
						console.log(myDataList);

						if (typeof callback === "function")
							callback(myDataList);
					},
					dataType : "json"
				});

	}

	function processData(myDataList) {
		var myLevel1Data = myDataList
		var drilldownsData = {};
		var convertedLevel1DataArrayToObject = [];
		myLevel1Data.forEach(function(entry) {
			// for building initial data series
			convertedLevel1DataArrayToObject.push({
				name : entry[0]+' '+entry[2]+' Cell '+ entry[3],
				y : entry[1],
				drilldown : true,
				market: entry[0],
				operator: entry[2],
				cellId: entry[3],
				level: 1

			});
		});
		
		console.log(convertedLevel1DataArrayToObject[0].name, + ' ' + convertedLevel1DataArrayToObject[0].y)

		return convertedLevel1DataArrayToObject;

	}

}


