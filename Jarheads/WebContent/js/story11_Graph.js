var renderStory11Graph = function(date) {

	var displayDate = date.split('&'); 
	var startDateAndTime = displayDate[0]+'/'+displayDate[1]+'/'+displayDate[2]+' '+
	displayDate[3]+':'+displayDate[4];
	var endDateAndTime = displayDate[5]+'/'+displayDate[6]+'/'+displayDate[7]+' '+
	displayDate[8]+':'+displayDate[9];

	var myTitle = "Top 10 Worst Market/Operator/Cell Id Combos";
	var mySubtitle = 'Date between: ' + startDateAndTime+' and '+endDateAndTime;
	var yAxisLabel = "Failures"
		var level1URL = 'http://localhost:8080/Jarheads/rest/basedata/Top10MarketByDate/'+date
		var level2URL = 'http://localhost:8080/Jarheads/rest/basedata/Top10OperatorsPerMarketByDate/'
			var level3URL = 'http://localhost:8080/Jarheads/rest/basedata/Top10CellsPerOperatorByDate/' //TODO refactor this to show Cells. Then move to level 4!
				getBaseData(function(myDataList) {
					baseData = processData(myDataList);

					$('#graphResultsTab').empty();

					var count = 0;
					var myGraph = $('#graphResultsTab')
					.highcharts(
							{
								chart : {
									type : 'pie'
								},
								title : {
									text : myTitle
								},

								subtitle : {
									text : mySubtitle
								},
								xAxis : {
									// max: 6,
									type : 'category',
									labels : {
										rotation : -45,
										style : {
											fontSize : '10px',
											fontFamily : 'Verdana, sans-serif'
										}
									}

								},
								yAxis : {
									min : 0,
									title : {
										text : yAxisLabel
									}
								},

								legend : {
									enabled : false
								},


								plotOptions : {
									series : {
										borderWidth : 0,
										dataLabels : {
											enabled : true
										},
										point : {
											events : {
												click : function(event) {
													console.log(this);
													console.log(this.name);
													console.log(this.y);
													console.log(this.drilldown);

													var chart = this.series.chart;
													console.log(this.series.chart.title.textStr)
													var market = this.market;
													var operator = this.operator;
													var drillDownURL = "";
													var currentLevel = this.level;
													if(this.level == 1){
														console.log('Current level is 1. Going to  Level 2..')
														drillDownURL = level2URL+date+'&'+market
													}
													if(this.level == 2){
														console.log('Current level is 2. Going to Level 3..')
														drillDownURL = level3URL+date+'&'+operator
													}

													$
													.ajax({
														url : drillDownURL,
														success : function(
																data) {
															console
															.log('url is: '
																	+ this.url)
																	swapSeries(
																			chart,
																			"Failures",
																			data,++currentLevel);
														},
														dataType : "json"
													});
												}
											}
										}
									}
								},

								series : [ {
									name : 'Failures',
									colorByPoint : true,
									data : baseData
								} ],
								exporting: {
									buttons: {
										customButton: {
											text: 'Go back',
											onclick: function () {
												$('#graphResultsTab').empty();
												renderStory11Graph(date)
											}
										}
									}
								}
							});



				});
	/**
	 * @param name = name of the Y axis on hover */
	function swapSeries(chart, name, data,currentLevel) {
		console.log('current level is now: ' + currentLevel)
		chart.series[0].remove();

		var currentLevelData = [];
		if(currentLevel ==2){
			data.forEach(function(entry) {
				currentLevelData.push({
					name : entry[0],
					y : entry[1],
					drilldown : true,
					operator: entry[0],
					level: currentLevel
				});
			});
		}
		else if(currentLevel ==3){
			data.forEach(function(entry) {
				currentLevelData.push({
					name : 'Cell '+entry[0],
					y : entry[1],
					drilldown : true,
					operator: entry[0],
					level: currentLevel
				});
			});
		}

		else{
			currentLevelData = data;
		}

		chart.addSeries({
			data : currentLevelData,
			name : name,
			colorByPoint : true
		});
	}

	$('#iniChart').click(function() {
		swapSeries(Highcharts.charts[0], 'Failures', baseData);
	});

	function getBaseData(callback) {
		$
		.ajax({
			url : level1URL,
			success : function(data) {
				var myDataList = []
				var list = data == null ? []
				: (data instanceof Array ? data : [ data ]);
				$.each(list, function(index, myData) {
					myDataList
					.push([ myData[0], myData[1] ]); // Description,
					// failures,
					// Devicemodel,
				});
				console.log(myDataList);

				if (typeof callback === "function")
					callback(myDataList);
			},
			dataType : "json"
		});

	}

	function processData(myDataList) {
		var myLevel1Data = myDataList
		var drilldownsData = {};
		var convertedLevel1DataArrayToObject = [];
		myLevel1Data.forEach(function(entry) {
			// for building initial data series
			convertedLevel1DataArrayToObject.push({
				name : entry[0],
				y : entry[1],
				drilldown : true,
				market: entry[0],
				level: 1

			});
		});

		return convertedLevel1DataArrayToObject;

	}

}


