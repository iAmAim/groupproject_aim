/**
 * This javascript file provides methods that are used by the system administrator 
 * while managing the users on the system. The methods enable the system administrator
 * to add new users to the system, and update and delete registered users.
 * 
 * @version 1.0
 */

var registeredUsersList;
var isUsernameRegistered;
var currentUserIdToEdit;
var currentUserIdToDelete;
var currentUserNameToEdit;
var userTypes={};

/**
 * This method has many internal functions that are carried out when the DOM is loaded.
 */
$(document).ready(function() {

	$("body").tooltip({ selector: '[data-toggle=tooltip]' });
	
	constructTableBody();
	contructRegistrationModal();
	constructConfirmationModal();
	findAllUserTypes();
	findAllRegisteredUsers();
	
	/**
	 * This function is used to hide the confirmation modal when the cancel button
	 * is clicked.
	 */
	$('#cancel').click(function (e) {
		e.preventDefault();
		hideConfirmationModal();
	});

	/**
	 * This function is used to carry out a specific CRUD function when the
	 * yes button is clicked on the confirmation modal. If the yes button is clicked when
	 * registering a new user the addUser() method is called. If the yes button is clicked
	 * when editing a currently registered user the updateUser() method is called. If the
	 * the yes button is clicked when deleting a currently registered user the deleteUser()
	 * method is called.
	 */
	$('#yes').click(function (e) {
		e.preventDefault();
		var actionTaken = $('#confirmationRegisterLable').text();
		if(actionTaken=="Do you want to register this user?"){
			addUser();
		}
		else if(actionTaken=="Do you want to edit this user?"){
			updateUser();
		}
		else if(actionTaken=="Do you want to delete this user?"){
			deleteUser();
		}
		else{
			hideModals();
		}
	});
	
	/**
	 * This function is used to edit the details modal form for edit user use when the
	 * edit link is clicked. It also call the findUserById() method to populate 
	 * the form with the selected user's details.
	 */
	$(document).on("click",'#edit',function(){
		currentUserIdToEdit = $(this).data('identity');
		findUserById(currentUserIdToEdit);
		clearErrorFields();
		$("#registrationModalLabel").text("Edit User Details");
		$("#registrationSubmitBtn").text("Save");
		resetConfirmationButtons();
		$('#registrationModal').modal('show');
	});

	/**
	 * This function is used to pop up the delete confirmation modal when
	 * a the delete link is clicked.
	 */
	$(document).on("click",'#delete',function(){
		currentUserIdToDelete = $(this).data('identity');
		$('#confirmationRegisterLable').text('Do you want to delete this user?');
		resetConfirmationButtons();
		$('#confirmation').modal('show');
	});

	/**
	 * This function is used to edit the details modal form for register user use when
	 * the register user button is clicked.
	 */
	$('#registerBtn').click(function(){
		console.log('show registration modal');
		clearErrorFields();
		clearTextFields();
		$("#registrationModalLabel").text("Register User");
		$("#registrationSubmitBtn").text("Register");
		$('#registrationModal').modal('show');
	});

	/**
	 * This function is used to check the input fields for validity once the save/register
	 * button is clicked during the edit/registration process respectively. It also edits
	 * the confirmation modal's text to coincide with the process being carried out.
	 */
	$('#registrationSubmitBtn').click(function(){
		clearErrorFields();
		var submitType = $("#registrationSubmitBtn").text();
		console.log('Submit button type: '+submitType);
		if(!areFieldsEmpty()){
			clearErrorFields();
			isUsernameRegistered=false;
			if(submitType=="Register"){
				console.log('Register btn selected');
				currentUserNameToEdit = null;
				if(areUserCredentialsValid()){
					resetConfirmationButtons();
					$('#confirmationRegisterLable').text('Do you want to register this user?');
					$('#confirmation').modal('show');
				}
			}
			else{
				console.log('Save btn selected');
				if(areUserCredentialsValid()){
					$('#confirmationRegisterLable').text('Do you want to edit this user?');
					$('#confirmation').modal('show');
				}
			}
		}
	});
});

/**
 * This method is used to construct the confirmation modal which is used during the registration and
 * edit user process.
 */
var constructConfirmationModal = function(){
	console.log('constructConfirmationModal');
	$('#user_management').append('<div class="modal fade bs-example-modal-sm" id="confirmation"'+
						'tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"'+
						'aria-hidden="true">'+
						'<div class="modal-dialog modal-sm">'+
							'<div class="modal-content">'+
								'<div class="modal-header">'+
									'<h4 class="modal-title" id="confirmationRegisterLable">Do you want'+
										' to register this user?</h4>'+
								'</div>'+
								'<div class="modal-body">'+
									'<button type="submit" class="btn btn-success btn-sm" id="yes">Yes</button>'+
									'<button type="submit" class="btn btn-danger btn-sm"'+
										'data-dismiss="modal" id="cancel">Cancel</button>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>');
}

/**
 * This method is used to construct the registration form modal that is displayed
 * to the user when the click the register user button or edit link.
 */
var contructRegistrationModal = function(){
	$('#user_management').append('<div class="modal fade" id="registrationModal" tabindex="-1"'+
						'role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'+
						'<div class="modal-dialog">'+
							'<div class="modal-content">'+
								'<div class="modal-header">'+
									'<button type="button" class="close" data-dismiss="modal"'+
										'aria-hidden="true">&times;</button>'+
									'<h4 class="modal-title" id="registrationModalLabel">Register'+-
										'User</h4>'+
								'</div>'+
								'<div class="modal-body">'+
									'<form class="form-horizontal" method="post">'+
										'<div class="form-group">'+
											'<label class="col-sm-4 control-label"><strong>'+
													'Username:</strong></label>'+
													'<input type="text" class="input-xlarge"'+
												'style="height: 32px;" name="username" id="username"/>'+
											'<span class="glyphicon glyphicon-info-sign" data-toggle="tooltip"'+
												'data-placement="bottom" title="Username must adhere to the'+
										'following:'+
										'It must contain only letters (A-Z),numbers [0-9] and'+
											'underscores'+
										'It must be at least 4 characters in length and not'+
											'exceed 10 characters in length"></span>'+
											'<label id="username_error" class="text-danger"></label>'+
										'</div>'+

										'<div class="form-group">'+
											'<label class="col-sm-4 control-label"><strong>User'+
													'Type:</strong></label> <select id="userTypeDropDownList" name="userType"'+
												'style="height: 32px;">'+

											'</select> <label id="userType_error" class="text-danger"></label>'+
										'</div>'+

										'<div class="form-group">'+
											'<label class="col-sm-4 control-label"><strong>'+
													'Password:</strong></label>'+
													'<input type="password" id="password1"'+
												'name="password1" placeholder="" class="input-xlarge"'+
												'style="height: 32px;">'+
											'<span class="glyphicon glyphicon-info-sign" data-toggle="tooltip"'+
												'data-placement="bottom" title="Password must adhere to the'+
										'following:'+
										'It must contain only letters (A-Z),numbers [0-9] and'+
											'underscores'+
										'It must contain at least one uppercase letter,one'+
											'lowercase letter and one number'+
										'It must be at least 6 characters in length and not'+
											'exceed 10 characters in length"></span>'+
   											'<label id="password1_error" class="text-danger"></label>'+
										'</div>'+

										'<div class="form-group">'+
											'<label class="col-sm-4 control-label"><strong>Re-type'+
													'Password:</strong></label> <input type="password" id="password2"'+
												'name="password2" placeholder="" class="input-xlarge"'+
												'style="height: 32px;" /> <label id="password2_error"'+
												'class="text-danger"></label>'+
										'</div>'+

									'</form>'+
								'</div>'+
								'<div class="modal-footer">'+
									'<button type="button" class="btn btn-default"'+
										'data-dismiss="modal">Close</button>'+
									'<button type="button" class="btn btn-success"'+
										'id="registrationSubmitBtn">Register</button>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>');
}

/**
 * This method is used to create the data table's body that contains 
 * the registered users details.
 */
var constructTableBody = function() {
	$('#user_management').append('<div class="container">'+
						'<h3 id="registeredUsersTableTitle">Registered System Users</h3>'+
						'<div class="table-responsive">'+
							'<table id="registeredUsersTable"'+
								'class="table table-striped table-bordered">'+
								'<thead>'+
									'<tr>'+
										'<th>ID</th>'+
										'<th>Username</th>'+
										'<th>Password</th>'+
										'<th>UserType</th>'+
										'<th>Edit</th>'+
										'<th>Delete</th>'+
									'</tr>'+
								'</thead>'+
								'<tbody id="dataTableBody">'+


								'</tbody>'+
							'</table>'+
						'</div>'+
					'</div>');
	
}

/**
 * This method finds all the different user types on the system by using 
 * a GET method to retrieve the user types from the database.
 */
var findAllUserTypes = function() {
	console.log("findAllUserTypes()");
	$.ajax({
		type : 'GET',
		url : "http://localhost:8080/Jarheads/rest/usertypes",
		dataType : "json",
		success :  renderUserTypeList
	});
};

/**
 * This method populates the user types drop down list in the register/edit form
 * with the user types retrieved by the GET method in findAllUserTypes.
 * 
 * @param data the user types retrieved from the database.
 */
var renderUserTypeList = function(data) {
	console.log("renderUserTypeList()");
	var $select = $('#userTypeDropDownList');
	$select.find('option').remove();

	userTypes = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$.each(userTypes ,function(key, usertype) {
		$select.append('<option value=' + usertype.userTypeId + '>' + usertype.name + '</option>');
		console.log('Order of items in userTypes array: '+usertype.userTypeId+' name: '+usertype.name);
	});

};

/**
 * This method returns the user type in the form of a string for a 
 * given user type id.
 * 
 * @param id the id of the user type.
 */
var getUserTypeById = function(id){
	console.log('getUserTypeById()');
	console.log("User Type Id: "+id);
	return userTypes[id-1];
};

/**
 * This method resets the confirmation modal's buttons as the same modal is used
 * for confirmation and success messages.
 */
var resetConfirmationButtons = function(){
	console.log('resetConfirmationButtons()');
	$('#yes').text('Yes');
	$('#cancel').show();
};

/**
 * This method hides the confirmation modal.
 */
var hideConfirmationModal = function(){
	console.log('hideConfirmationModal()');
	$('#confirmation').modal('hide');
};

/**
 * This method hides both the confirmation modal and the registration/edit modal.
 */
var hideModals = function(){
	console.log('hideModals()');
	$('#confirmation').modal('hide');
	$('#registrationModal').modal('hide');
};

/**
 * This method edits the confirmation modal's buttons so that the same modal 
 * can be used for the success messages.
 */
var editConfirmationModalButtons = function(){
	console.log('editConfirmationModalButtons()');
	$('#yes').text('OK');
	$('#cancel').hide();
};

/**
 * This method checks to see if any of the input fields are empty during 
 * registration/editing of a user. It displays an error message beside the fields
 * that are empty.
 */
var areFieldsEmpty = function(){
	console.log('areFieldsEmpty()');
	var usernameField = $('#username').val();
	var password1Field = $('#password1').val();
	var password2Field = $('#password2').val();

	var emptyFields = false;
	if(usernameField==""){
		$('#username_error').text("* This field must be filled");
		emptyFields = true;
	}
	if(password1Field==""){
		$('#password1_error').text("* This field must be filled");
		emptyFields = true;
	}
	if(password2Field==""){
		$('#password2_error').text("* This field must be filled");
		emptyFields = true;
	}
	return emptyFields;
};

/**
 * This method clears all the input fields text.
 */
var clearTextFields = function(){
	console.log('clearTextFields()');
	$('#username').val('');
	$('#password1').val('');
	$('#password2').val('');
};

/**
 * This method clears all the error messages beside the input fields.
 */
var clearErrorFields = function(){
	console.log('clearErrorFields()');
	$('#username_error').text("");
	$('#userType_error').text("");
	$('#password1_error').text("");
	$('#password2_error').text("");
};

/**
 * This method finds a user registered on the system for a given id. This is used when
 * loading the user's data into the edit form when the system administrator uses the 
 * edit function.
 * 
 * @param id the id of the current user being edited.
 */
var findUserById = function (id){
	console.log('findUserById()');
	$.ajax({
		type: 'GET',
		url: "http://localhost:8080/Jarheads/rest/users/searchbyid/"+id,
		dataType: "json",
		success: renderListCurrentUserToEdit
	});
};

/**
 * This method populates the edit form with the details of the current user
 * being edited.
 * 
 * @param data the current user's registered details.
 */
var renderListCurrentUserToEdit = function(data){
	console.log('renderListCurrentUserToEdit()');
	var list = data ==null ? [] : (data instanceof Array ? data : [data]);

	$.each(list,function(index,user){
		$('#username').val(user.userName);
		$('#password1').val(user.password);
		$('#password2').val(user.password);
		$('#userTypeDropDownList').val(user.userType.userTypeId);
		currentUserNameToEdit = user.userName;
	});
	currentUserToEdit = list;
};

/**
 * This method checks if the details entered by the system administrator during the 
 * registration/edit process are valid according to username and password credentials 
 * and whether the username entered is already registered or not.
 */
var areUserCredentialsValid = function(){
	console.log('areUserCredentialsValid()');

	var username = $('#username').val();

	if(currentUserNameToEdit!=username){
		$.each(registeredUsersList,function(index,user){
			if(username==user.userName){
				isUsernameRegistered=true;
			}
		});
	}

	if(isUsernameRegistered){
		$('#username_error').text("* Username is already registered");
		return false;
	}
	else if(!usernameCheck()){
		$('#username_error').text("* Invalid username credentials");
		return false;
	}
	else if(!passwordCheck()){
		$('#password1_error').text("* Invalid password credentials");
		return false;
	}
	else if(!passwordsMatch()){
		$('#password2_error').text('* Passwords do not match');
		return false;
	}
	return true;
};

/**
 * This method checks if the passwords entered in the password and 
 * re-type password input fields match during registration/edit.
 */
var passwordsMatch = function(){
	console.log('passwordsMatch()');
	var password1 = $('#password1').val();
	var password2 = $('#password2').val();
	if(password1!=password2){
		return false;
	}
	return true;
};

/**
 * This method checks if the username adheres to the username credentials.
 */
var usernameCheck = function() {
	console.log('usernameCheck()');

	var username = $('#username').val();
	var pattern = /^\w+$/;
	var containsOnlyLetterNumbersAndUnderscores = pattern.test(username);

	if(username.length<4||username.length>10){
		console.log('Username outside of length boundaries');
		return false;
	}
	if(!containsOnlyLetterNumbersAndUnderscores){
		console.log('Username doesnt contain only letters,numbers and underscores');
		return false;
	}
	return true;
};

/**
 * This method checks if the password adheres to the password credentials.
 */
var passwordCheck = function(){
	console.log('passwordCheck()');

	var password = $('#password1').val();
	var pattern = /^\w+$/;
	var containsOnlyLetterNumbersAndUnderscores = pattern.test(password);

	if(password.length<6||password.length>10){
		console.log('Password outside of length boundaries');
		return false;
	}
	if(!containsOnlyLetterNumbersAndUnderscores){
		console.log('Password doesnt contain only letters,numbers and underscores');
		return false;
	}
	if(!containsAtLeastOneUpperCaseOneLowerCaseLetterAndOneDigit(password)){
		console.log('Password doesnt contain at least one uppercase letter,one lower case letter and one digit');
		return false;
	}
	return true;
};

/**
 * This method checks that the password contains at least one upper case letter, one
 * lower case letter and one digit which is part of the password credentials check.
 * 
 * @param password the password being checked.
 */
var containsAtLeastOneUpperCaseOneLowerCaseLetterAndOneDigit = function(password){

	console.log('containsAtLeastOneUpperCaseOneLowerCaseLetterAndOneDigit()');
	if(!/\d/.test(password)){
		return false; 
	}
	if(!/[a-z]/.test(password)){
		return false; 
	}
	if(!/[A-Z]/.test(password)){
		return false;
	}
	return true;
};

/**
 * This method is used to find all the registered users on the system by using 
 * a GET method to retrieve these registered users from the database.
 */
var findAllRegisteredUsers = function(){
	console.log('findAllRegisteredUsers()');
	$.ajax({
		type: 'GET',
		url: "http://localhost:8080/Jarheads/rest/users",
		dataType: "json",
		success: renderListOfRegisteredUsers
	});
};

/**
 * This method is used to populate the registered users table with the registered users
 * that have be retrieved from the database
 * 
 * @param data the retrieved registered users from the database
 */
var renderListOfRegisteredUsers = function(data){

	console.log('renderListOfRegisteredUsers()');

	var dataTable = $('#registeredUsersTable').dataTable();
	dataTable.fnClearTable();
	dataTable.fnDraw();
	dataTable.fnDestroy();

	var list = data ==null ? [] : (data instanceof Array ? data : [data]);

	$.each(list, function(index, user) {
		var t = $('#registeredUsersTable').DataTable();
		t.row.add(
				[user.userId,user.userName,user.password,user.userType.name,
				 '<a href="#" id="edit" data-identity="'+user.userId+'">Edit</a>',
				 '<a href="#" id="delete" data-identity="'+user.userId+'">Delete</a>']).draw();
	});

	registeredUsersList = list;
};

/**
 * This method uses a POST to add a new user's details to the database.
 */
var addUser = function() {
	console.log('addUser()');
	$.ajax({
		type: 'POST',
		contentType: 'application/json',
		url: "http://localhost:8080/Jarheads/rest/users",
		dataType : "json",
		data: formToJSONAddUser(),
		success: function(data,textStatus,jqXHR){
			editConfirmationModalButtons();
			$('#confirmationRegisterLable').text('Successfully added user');
			$('#confirmation').modal('show');
			findAllRegisteredUsers();
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('addUser error: '+textStatus);
		}

	});

};

/**
 * This method uses a DELETE to delete a registered user from the database.
 */
var deleteUser = function() {
	console.log('deleteUser()');
	$.ajax({
		type: 'DELETE',
		contentType: 'application/json',
		url: "http://localhost:8080/Jarheads/rest/users/delete/"+currentUserIdToDelete,
		dataType : "json",
		data: formToJSONDeleteUser(),
		success: function(data,textStatus,jqXHR){
			editConfirmationModalButtons();
			$('#confirmationRegisterLable').text('Successfully deleted user');
			$('#confirmation').modal('show');
			findAllRegisteredUsers();
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('deleteUser error: '+textStatus);
		}

	});

};

/**
 * This method uses a PUT to update a registered user's details in the database.
 */
var updateUser = function() {
	console.log('updateUser()');
	$.ajax({
		type: 'PUT',
		contentType: 'application/json',
		url: "http://localhost:8080/Jarheads/rest/users/"+currentUserIdToEdit,
		dataType : "json",
		data: formToJSONUpdateUser(),
		success: function(data,textStatus,jqXHR){
			editConfirmationModalButtons();
			$('#confirmationRegisterLable').text('Successfully updated user');
			$('#confirmation').modal('show');
			findAllRegisteredUsers();
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('userUpdate error: '+textStatus);
		}

	});

};

/**
 * This method is used by the addUser() method to convert the user's details into
 * JSON format.
 */
var formToJSONAddUser=function() {
	console.log('formToJSONAddUser()');
	console.log('User Tyoe Id: '+$('#userTypeDropDownList').val());
	console.log('UserType: '+getUserTypeById($('#userTypeDropDownList').val()));
	return JSON.stringify({
		"userId" : null,
		"userName" : $('#username').val(),
		"password" : $('#password1').val(),
		"userType" : getUserTypeById($('#userTypeDropDownList').val())
	});
};

/**
 * This method is used by the deleteUser() method to convert the user's details into
 * JSON format.
 */
var formToJSONDeleteUser=function() {
	console.log('formToJSONDeleteUser()');
	console.log('ID to delete in form to JSON:'+ currentUserIdToDelete);
	return JSON.stringify({
		"userId" : currentUserIdToDelete,
		"userName" : $('#username').val(),
		"password" : $('#password1').val(),
		"userType" : getUserTypeById($('#userTypeDropDownList').val())
	});
};

/**
 * This method is used by the updateUser() method to convert the user's details into
 * JSON format.
 */
var formToJSONUpdateUser=function() {
	console.log('formToJSONUpdateUser()');
	return JSON.stringify({
		"userId" : currentUserIdToEdit,
		"userName" : $('#username').val(),
		"password" : $('#password1').val(),
		"userType" : getUserTypeById($('#userTypeDropDownList').val())
	});
};