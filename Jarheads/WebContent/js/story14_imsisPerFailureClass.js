/** 
 * This JavaScript file provides method for the query that retrieves all the IMSI's
 * that have had a failure associated with the failure class provided by the user.
 * 
 * @version 1.0
 */

$(document).ready(function () {
	/**
	 * This function is used to execute the query when it is clicked.
	 */
	$(document).on('click', '#searchIMSIPerFailureClassBtn', function() {
		console.log('searchIMSIPerFailureClassBtn clicked');
		var failureClass = $('#failureClassDropDownList').val();
		createIMSITbl();
		findAllIMSISPerFailureClass(failureClass);
	});
});

/**
 * This method is used to create the data table to 
 * display the results of the query.
 */
var createIMSITbl = function (){
	$('#placeWhereQueriesAreDisplayed').empty();
	buildTabsNoGraph();
	$('#placeWhereQueriesAreDisplayed').hide();
	$('#tableResultsTab').empty();
	$('#tableResultsTab').append('<div class="table-responsive" id="imsiTblResponseDiv">'
			+'		<table id="imsisPerFailureClassTable" class="table table-striped table-bordered">'
			+'			<thead>'
			+'				<tr>'
			+'					<th>IMSI</th>'
			+'				</tr>'
			+'			</thead>'
			+'			<tbody id="dataTableBody14">'
			+'			</tbody>'
			+'		</table>'
			+'	</div>');

	$('#imsisPerFailureClassTable').hide();
}

/**
 * This method is used to create the query input fields.
 */
var makeIMSISPerFailureClassFields=function() {
	$('#queryPanel').empty();
	$('#queryPanel').append('<br>'+
			'<label>Failure Class: </label>'
			+'<select id="failureClassDropDownList" name="failureClass"'
			+'style="height: 32px;">'
			+'</select>'
			+"<span class=\"glyphicon glyphicon-info-sign\" id=\"dateInfo\" data-toggle=\"tooltip\" "
			+"data-placement=\"bottom\""
			+"title=\"This query returns all the IMSI's with a failure for the provided failure class.\">"
			+"</span>"
			+'<button type="button" class="btn btn-info" id="searchIMSIPerFailureClassBtn">Search</button>'
			+'<br><br><br>')

};

/**
 * This method is used to get all the IMSI's in the database that have had
 * error associated with the user provided failure class and then to populate the data table 
 * with this retrieved data.
 */
var findAllIMSISPerFailureClass = function(failureClass){

	console.log('renderFailuresPerIMSITable()');

	$('#imsisPerFailureClassTable').show();

	$('#imsisPerFailureClassTable').dataTable( {
		"bFilter" : true,
		"sPaginationType" : "full_numbers",
		"aoColumns" : [
		               {"mRender" : function(imsi){
		            	   return imsi;
		               }}
		               ],
		               "bProcessing" :true,
		               "bServerSide" : true,
		               "sAjaxSource" : "rest/basedata/imsisPerFailureClass/"+failureClass
	});
	window.scrollTo(0, 0);
	$('#placeWhereQueriesAreDisplayed').show();
}

/**
 * This method is used to get all the failure classes that are present in the database.
 */
var findAllFailureClasses = function(){
	console.log('findAllFailureClasses()');
	$.ajax({
		type: 'GET',
		url: 'http://localhost:8080/Jarheads/rest/basedata/failureClasses',
		dataType: "json",
		success: renderFailureClassDropDownList
	});
};

/**
 * This method populates the drop down list with the failure classes retrieved from 
 * the database.
 */
var renderFailureClassDropDownList = function(data) {
	var failureClassList;
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$.each(list,function(index, failureClass) {	
		failureClassList+='<option value='+failureClass[0]+'>'
		+failureClass[0]+' : '+failureClass[1]+'</option>';
	});
	$('#failureClassDropDownList').append(failureClassList);
};