var importURL = 'http://localhost:8080/Jarheads/rest/basedata/import'
	var currentFileName = ''; 
setInterval(function() {
	console.log("In interval");
	callAutoImport();
}, 30 * 1000); // 60 * 1000 milliseconds
/**
 * This method has many internal functions that are carried out when the DOM is loaded.
 */

$(document).ready(function() {

	$(document).on("submit", 'form#data', importData);
	checkIfFileIsSelected();
	$('#invalidData').dataTable();
	$("#success-alert").hide();
	$("#confirmationClear").hide();
	$("#success-AutoImport").hide();
	$.fn.dataTableExt.sErrMode = 'throw';
	$("#btnErrorLog").click(function() {
		openErrorLogs();
	});
	$("#btnResetDatabase").click(function() {
		$("#confirmationClear").modal('show');
	}); 
	$("#yesClear").click(function() {
		clearDatabase();
		$("#confirmationClear").modal('hide');
	});
	$("#cancelClear").click(function() {
		$("#confirmationClear").modal('hide');
	});
	$('#myTab').click( function() {
		var note = document.getElementById("tableResultsTab").getAttribute("class");
		if(note=="tab-pane")
		alert(note);
	} );
	$("#denis").click(function() {
		$('#placeWhereQueriesAreDisplayed').empty();
		currentQuerySelected = "story9"
			
		if($('#queryPanel').is(':visible')&&(!$(this).next().is('div'))){
			$('#queryPanel').hide();
			makeStory9();
			var elem = $("#queryPanel");
			elem.insertAfter($(this));
			$('#queryPanel').slideDown("slow");
		}
		else if($('#queryPanel').is(':visible')&&$(this).next().is('div')){
			$('#queryPanel').slideUp();
		}
		else{
			makeStory9();
			var elem = $("#queryPanel");
			elem.insertAfter($(this));
			$('#queryPanel').slideDown("slow");
		}
	}); 

	
	/************************************************
	 * ******************************************************************************
	 * START STORY 6
	 * ******************************************************************************
	 ***************************************************/
	
	$("#st6_CauseCodesPerImsiBtn").click(function() {
		$('#placeWhereQueriesAreDisplayed').empty();
	
		if($('#queryPanel').is(':visible')&&(!$(this).next().is('div'))){
			$('#queryPanel').hide();
			makeStory6();
			story6Tbl();
			populateIMSIListForDropdownBoxSearch();
			var elem = $("#queryPanel");
			elem.insertAfter($(this));
			$('#queryPanel').slideDown("slow");
		}
		else if($('#queryPanel').is(':visible')&&$(this).next().is('div')){
			$('#queryPanel').slideUp();
		}
		else{
			makeStory6();
			story6Tbl();
			populateIMSIListForDropdownBoxSearch();
			var elem = $("#queryPanel");
			elem.insertAfter($(this));
			$('#queryPanel').slideDown("slow");
		}

		$("#searchButton6").click(function() {
			search6($('#searchKey6').val());
			return false;
		});

		$('#searchKey6').keypress(function(e) {
			if (e.which == 13) {
				search6($('#searchKey6').val());
				e.preventDefault();
				return false;
			}
		});
	});
	/*************************************************
	 * ******************************************************************************
	 * END STORY 6
	 * ******************************************************************************
	 * ************************************************/
	$("#causeAndEventForImsiFailures4").click(function() {
		$('#placeWhereQueriesAreDisplayed').empty();
	
		if($('#queryPanel').is(':visible')&&(!$(this).next().is('div'))){
			$('#queryPanel').hide();
			makeStory4();
			story4Tbl();
			findDistinctIMSIList();
			var elem = $("#queryPanel");
			elem.insertAfter($(this));
			$('#queryPanel').slideDown("slow");
		}
		else if($('#queryPanel').is(':visible')&&$(this).next().is('div')){
			$('#queryPanel').slideUp();
		}
		else{
			makeStory4();
			story4Tbl();
			findDistinctIMSIList();
			var elem = $("#queryPanel");
			elem.insertAfter($(this));
			$('#queryPanel').slideDown("slow");
		}

		$("#searchButton").click(function() {
			search($('#searchKey').val());
			return false;
		});

		$('#searchKey').keypress(function(e) {
			if (e.which == 13) {
				search($('#searchKey').val());
				e.preventDefault();
				return false;
			}
		});
	});

	$("#colin").click(function() {
		$('#placeWhereQueriesAreDisplayed').empty();
		currentQuerySelected = "story8"
		
		if($('#queryPanel').is(':visible')&&(!$(this).next().is('div'))){
			$('#queryPanel').hide();
			makeStory8();
			var elem = $("#queryPanel");
			elem.insertAfter($(this));
			$('#queryPanel').slideDown("slow");
		}
		else if($('#queryPanel').is(':visible')&&$(this).next().is('div')){
			$('#queryPanel').slideUp();
		}
		else{
			makeStory8();
			var elem = $("#queryPanel");
			elem.insertAfter($(this));
			$('#queryPanel').slideDown("slow");
		}

	});

	$("#imsiFailuresForTimePeriod7").click(function() {
		$('#placeWhereQueriesAreDisplayed').empty();
		currentQuerySelected = "story7"
	
		if($('#queryPanel').is(':visible')&&(!$(this).next().is('div'))){
			$('#queryPanel').hide();
			makeStory7();
			var elem = $("#queryPanel");
			elem.insertAfter($(this));
			$('#queryPanel').slideDown("slow");
		}
		else if($('#queryPanel').is(':visible')&&$(this).next().is('div')){
			$('#queryPanel').slideUp();
		}
		else{
			makeStory7();
			var elem = $("#queryPanel");
			elem.insertAfter($(this));
			$('#queryPanel').slideDown("slow");
		}
		
	});
	$('.checkTheresData').click(function() {
		findAmountOfRecordsInDatabase();
	});

	$("#failuresPerDeviceBtn").click(function() {
		$('#placeWhereQueriesAreDisplayed').empty();
		currentQuerySelected = "story10";
		
		if($('#queryPanel').is(':visible')&&(!$(this).next().is('div'))){
			$('#queryPanel').hide();
			makeStory10();
			makeStory10Tbl();
			findAllUETypes();
			var elem = $("#queryPanel");
			elem.insertAfter($(this));
			$('#queryPanel').slideDown("slow");
		}
		else if($('#queryPanel').is(':visible')&&$(this).next().is('div')){
			$('#queryPanel').slideUp();
		}
		else{
			makeStory10();
			makeStory10Tbl();
			findAllUETypes();
			var elem = $("#queryPanel");
			elem.insertAfter($(this));
			$('#queryPanel').slideDown("slow");
		}	

		$("#failuresPerDeviceSearchBtn").click(function() {
			getfailuresPerDeviceList($('#searchKey').val());
			return false;
		});

	});

	
	/* Story 13 - top 10 worst cells ever */
	$("#top10WorstCellsBtn").click(function() {
		console.log('clicked: top10WorstCellBtn');
		$('#queryPanel').hide();
		$('#placeWhereQueriesAreDisplayed').empty();
		$('#placeWhereQueriesAreDisplayed').show();
		renderStory13Graph();
		

	});
	/* Story 11 - top 10  Market/Operator/Cell ID failures  */
	$("#top10FailuresBtn").click(function() {
		$('#placeWhereQueriesAreDisplayed').empty();
		currentQuerySelected = "story11"
		
		if($('#queryPanel').is(':visible')&&(!$(this).next().is('div'))){
			$('#queryPanel').hide();
			maketop10Failures();
			var elem = $("#queryPanel");
			elem.insertAfter($(this));
			$('#queryPanel').slideDown("slow");
		}
		else if($('#queryPanel').is(':visible')&&$(this).next().is('div')){
			$('#queryPanel').slideUp();
		}
		else{
			maketop10Failures();
			var elem = $("#queryPanel");
			elem.insertAfter($(this));
			$('#queryPanel').slideDown("slow");
		}

	});


	/*** Story 12 - top 10 Imsi's with failures */
	$("#top10ImsiCallFailsTimePeriod").click(function() {
		console.log('clicked: top10ImsiCallFailsTimePeriod');
		currentQuerySelected = "story12"
		$('#placeWhereQueriesAreDisplayed').empty();
		
		if($('#queryPanel').is(':visible')&&(!$(this).next().is('div'))){
			$('#queryPanel').hide();
			makeStory12();
			var elem = $("#queryPanel");
			elem.insertAfter($(this));
			$('#queryPanel').slideDown("slow");
		}
		else if($('#queryPanel').is(':visible')&&$(this).next().is('div')){
			$('#queryPanel').slideUp();
		}
		else{
			makeStory12();
			var elem = $("#queryPanel");
			elem.insertAfter($(this));
			$('#queryPanel').slideDown("slow");
		}
//		document.getElementById('#queryPanel').scrollIntoView();
//
//	    $("#querySelection").scrollTop($("#querySelection").children().height());
	});

	/**
	 * This function is used to display the query input fields for 
	 * the failures per IMSI query. 
	 */
	$('#failuresPerImsiBtn').click(function(e) {
		$('#placeWhereQueriesAreDisplayed').empty();
		currentQuerySelected = "failuresPerImsi";
		console.log('failuresPerImsiBtn clicked');
		
		if($('#queryPanel').is(':visible')&&(!$(this).next().is('div'))){
			$('#queryPanel').hide();
			makeFailurePerImsiFields();
			makeFailurePerImsiTbl();
			findAllIMSIs();
			var elem = $("#queryPanel");
			elem.insertAfter($(this));
			$('#queryPanel').slideDown("slow");
		}
		else if($('#queryPanel').is(':visible')&&$(this).next().is('div')){
			$('#queryPanel').slideUp();
		}
		else{
			makeFailurePerImsiFields();
			makeFailurePerImsiTbl();
			findAllIMSIs();
			var elem = $("#queryPanel");
			elem.insertAfter($(this));
			$('#queryPanel').slideDown("slow");
		}
	});

	/**
	 * This function is used to display the query input fields for 
	 * the IMSI's per Failure Class query. 
	 */
	$('#imsisPerFailureClassBtn').click(function() {
		$('#placeWhereQueriesAreDisplayed').empty();
		currentQuerySelected = "imsisPerFailureClass";
		console.log('imsisPerFailureClassBtn clicked');
		
		if($('#queryPanel').is(':visible')&&(!$(this).next().is('div'))){
			$('#queryPanel').hide();
			makeIMSISPerFailureClassFields();
			findAllFailureClasses();
			var elem = $("#queryPanel");
			elem.insertAfter($(this));
			$('#queryPanel').slideDown("slow");
		}
		else if($('#queryPanel').is(':visible')&&$(this).next().is('div')){
			$('#queryPanel').slideUp();
		}
		else{
			makeIMSISPerFailureClassFields();
			findAllFailureClasses();
			var elem = $("#queryPanel");
			elem.insertAfter($(this));
			$('#queryPanel').slideDown("slow");
		}
	});

});

var importData = function(event) {
	event.preventDefault();
	$("#success-alert").hide();
	var formData = new FormData($(this)[0]);
	$.ajax({
		url : importURL,
		type : 'POST',
		data : formData,
		async : false,
		cache : false,
		contentType : false,
		processData : false,
		success : [ renderMessage, clearFileSelectionField,
		            checkIfFileIsSelected ],
		            error : function(returndata) {
		            	disableImportButton(true);
		            	showPopUp('fail', 'File import failed - Invalid File');
		            	clearFileSelectionField();
		            }

	});

	return false;

}

var checkIfFileIsSelected = function() {
	disableImportButton(true);

	$('input:file').on('change', function() {

		if ($(this).val()) {
			checkFileExtension($(this));
			disableImportButton(false);
			currentFileName = $(this).val().split('\\').pop();
			$('#tableTitle').text('Invalid data from ' + currentFileName);
		} else {
			disableImportButton(true);
			currentFileName = '';
		}
	});

}

var checkFileExtension = function(ref) {
	var fileExtension = [ 'xls', 'xlsx' ];
	if ($.inArray($(ref).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
		showPopUp('fail',
		"Invalid format: Only 'xls' and 'xlsx' formats are allowed.");
		clearFileSelectionField();
	}
}
var clearDatabase = function() {
	$.ajax({
		type : 'GET',
		url : "rest/basedata/clearDatabase",
		success : displayConsoleMessage
	});
}
var clearFileSelectionField = function() {
	$('input:file').val('');
}

var disableImportButton = function(bool) {
	$('#btnImport').prop('disabled', bool);
	return this;
}

var renderMessage = function(data) {

	clearFileSelectionField();
	console.log('data' + data);
	if (data == "The file selected has already been imported") {
		console.log("IN IF");
		showPopUp('fail', "Import Failed: " + data);
	} else if (data == "The file you have selected does not contain base data records") {
		showPopUp('fail', "Import Failed: " + data);
	} else if (data == "The file you have selected is missing the required fields") {
		showPopUp('fail', "Import Failed: " + data);
	} else if (data.startsWith('Import Succesful')) {
		showPopUp('success', data);
	}
};

var showPopUp = function(status, text) {
	$("#success-alert").text('');
	if (status == 'success') {
		$("#success-alert").attr('class', 'alert alert-success');

	} else if (status == 'fail') {
		$("#success-alert").attr('class', 'alert alert-danger');
	}

	$("#success-alert").append(text);
	$("#success-alert").alert();
	$("#success-alert").fadeTo(5000, 500).slideUp(500, function() {
		$("#success-alert").hide();
	});
}
var callAutoImport = function() {
	$.ajax({
		type : 'GET',
		url : "rest/basedata/callAutoImport",
		success : displayConsoleMessage
	});
}
var displayConsoleMessage = function(data){
	if(data == "Database has been cleared !"){
		$("#success-alert").attr('class', 'alert alert-success')
		$("#success-alert").empty();
		$("#success-alert").append(data);
		$("#success-alert").alert();
		$("#success-alert").fadeTo(5000, 500).slideUp(500, function() {
			$("#success-alert").hide();
		});
	}
	else if(data!="empty"){
		$("#success-alert").attr('class', 'alert alert-success')
		$("#success-alert").empty();
		$("#success-alert").append(data);
		$("#success-alert").alert();
		$("#success-alert").fadeTo(5000, 500).slideUp(500, function() {
			$("#success-alert").hide();
		});
	}
	else{
		console.log(data);
	}
};
var findAmountOfRecordsInDatabase = function() {
	$.ajax({
		type : 'GET',
		url : "rest/basedata/sizeOfDatabase",
		dataType : "json",
		contentType : "application/json",
		success : areQuerysAvailable
	});
}
var openErrorLogs = function() {
	$.ajax({
		type : 'GET',
		url : "rest/basedata/openErrorLogs",
		dataType : "json",
		contentType : "application/json",
	});
}
var areQuerysAvailable = function(data) {
	console.log("In database count " + data);
	if (data == 0) {
		$('.messageToDisplay').empty();
		$('.messageToDisplay').append('There is no data in the'
					+ 'database. A System Administrator needs to import some data to use'
					+ 'the query functionality');
		
		$("#denis").prop('disabled', true);
		$("#causeAndEventForImsiFailures4").prop('disabled', true);
		$("#colin").prop('disabled', true);
		$("#imsiFailuresForTimePeriod7").prop('disabled', true);
		$("#failuresPerDeviceBtn").prop('disabled', true);
		$("#failuresPerImsiBtn").prop('disabled', true);
		$("#top10FailuresBtn").prop('disabled', true);
		$("#top10WorstCellsBtn").prop('disabled', true);
		$("#st6_CauseCodesPerImsiBtn").prop('disabled', true);
		$("#imsisPerFailureClassBtn").prop('disabled', true);
		$("#top10ImsiCallFailsTimePeriod").prop('disabled', true);
		$('#theresNoData').modal('show');
	} else {
		$("#denis").prop('disabled', false);
		$("#causeAndEventForImsiFailures4").prop('disabled', false);
		$("#colin").prop('disabled', false);
		$("#imsiFailuresForTimePeriod7").prop('disabled', false);
		$("#failuresPerDeviceBtn").prop('disabled', false);
		$("#failuresPerImsiBtn").prop('disabled', false);
		$("#top10FailuresBtn").prop('disabled', false);
		$("#top10WorstCellsBtn").prop('disabled', false);
		$("#st6_CauseCodesPerImsiBtn").prop('disabled', false);
		$("#imsisPerFailureClassBtn").prop('disabled', false);
		$("#top10ImsiCallFailsTimePeriod").prop('disabled', false);

	}
}

