/** Render graphs/ graphics/ charts */

var findTop10WorstCellsEver = function() {
	console.log('find top 10 worst cells ever');
	$.ajax({
		type : 'GET',
		url : 'http://localhost:8080/Jarheads/rest/basedata/top10FailCells',
		dataType : "json",
		success : buildDataForGraph
	});
};

var buildDataForGraph = function(data) {
	console.log('fucntion: buildDataForGraph')
	console.log(data);
	var myDataList = [];
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$.each(list, function(index, myData) {
		myDataList.push([ myData[0] + ' ' + myData[1] + '- Cell ' + myData[2],
		                  myData[3], myData[1],myData[0],myData[2],myData[4]]);
	});

	console.log(myDataList);
	renderTop10WorstCellsGraph(myDataList,
			'Top 10 Worst Cells (based on failure count)', 'Cells',
	'Total Failures')
}

//TODO: change this ti renderGraph method that takes different parameters based
//on data to be rendered
var renderTop10WorstCellsGraph = function(myDataList, title, labelX, labelY) {
	console.log('fucntion: renderTop10WorstCellsGraph')
	$('#placeWhereQueriesAreDisplayed').show();
	$('#placeWhereQueriesAreDisplayed').highcharts(
			{
				exporting : {
					enabled : false
				},
				chart : {
					type : 'column'
				},
				title : {
					text : title
				},
				subtitle : {
					text : '  <a href="http://localhost:8080/Jarheads"> </a>'
				},

				xAxis : {
					max : 10,
					type : 'category',
					title : {
						text : labelX
					},
					labels : {
						useHTML : true,
						rotation : -45,
						style : {

							fontSize : '10px',
							fontFamily : 'Verdana, sans-serif'
						}
					}

				},
				scrollbar : {
					enabled : false
				},

				scrollbar : {
					enabled : false,
					barBackgroundColor : 'gray',
					barBorderRadius : 7,
					barBorderWidth : 0,
					buttonBackgroundColor : 'gray',
					buttonBorderWidth : 0,
					buttonArrowColor : 'yellow',
					buttonBorderRadius : 7,
					rifleColor : 'yellow',
					trackBackgroundColor : 'white',
					trackBorderWidth : 1,
					trackBorderColor : 'silver',
					trackBorderRadius : 7
				},
				yAxis : {
					min : 0,
					title : {
						text : labelY
					}
				},
				legend : {
					enabled : false
				},
				tooltip : {
					//followPointer:true, // if you want tooltip to follow the mouse pointer
					formatter : function() {
						return 'Operator: <b>' + myDataList[this.x][2] + '</b><br/>' + 
						'Market: <b>' + myDataList[this.x][3] + '</b><br/>' +
						'Cell Id: <b>' + myDataList[this.x][4] + '</b><br/>' +
						'Failure % for this Operator: <b>' + myDataList[this.x][5] + '%</b><br/>' ;
					}
				// pointFormat : labelY + ':<b>{point.y}</b>', //
				// <b>{point.y:.1f}

				},

				series : [ {
					name : title,
					data : myDataList,
					colorByPoint: true, // if you want different colors per bar
					dataLabels : {
						enabled : true,
						rotation : -90,
						color : '#FFFFFF',
						align : 'right',
						// format: '{point.y:.1f}', // one decimal
						y : 10, // 10 pixels down from the top
						style : {
							fontSize : '13px',
							fontFamily : 'Verdana, sans-serif'
						}
					}
				} ]
			});
};
