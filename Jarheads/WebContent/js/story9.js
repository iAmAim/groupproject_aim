/** Network Management  - Query: Count Imsi Fails and Calculate total Duration of an Imsis failures/downtime */

var makeStory9=function(){
	$('#placeWhereQueriesAreDisplayed').hide();
	$('#queryPanel').empty();			
	buildResultsTabs();
	buildGenericCalendarHTML();
	$('#queryPanel').append("<span class=\"glyphicon glyphicon-info-sign\" id=\"dateInfo\" data-toggle=\"tooltip\" "
			+"data-placement=\"bottom\""
			+"title=\"This query returns the number of failures for each IMSI and their total downtime " +
					"during a defined time period.\">"
			+"</span>");
	$('#tableResultsTab').append("<div class=\"table-responsive\">"
			+"<!--<table id=\"myTable\" class=\"display table\" width=\"100%\">-->"
			+"		<table id=\"imsiFailCountAndDurationCalculation\" class=\"table table-striped table-bordered\">"
			+"			<thead>"
			+"				<tr>"
			+"					<th>IMSI</th>"
			+"					<th>Total Failures</th>"
			+"					<th>Total Downtime (seconds)</th>"
			+"				</tr>"
			+"			</thead>"
			+"			<tbody id=\"dataTableBody9\">"
			+""
			+"			</tbody>"
			+"			</table>"
			+"			</div>"
	)	
	buildCalendar();
};


var findImsiFailCountAndCalculateDurationOfDowntime=function(datesInfo){

	renderStory9Graph(datesInfo);
	$("#graphResultsTab").hide();
	console.log('findImsiFailCountAndCalculateDurationOfDowntime');
	$.ajax({
		type:'GET',
		contentType:"application/json",
		url:"rest/basedata/ImsiFailureCountAndDuration/"+datesInfo,
		dataType:"json",
		success: renderImsiFailCountAndDuration
	});
}


var renderImsiFailCountAndDuration = function(data) {

	console.log('renderImsiFailCountAndDuration');
	var dataTable = $('#imsiFailCountAndDurationCalculation').dataTable();
	dataTable.fnClearTable();
	dataTable.fnDraw();
	dataTable.fnDestroy();
	list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	
	if(list.length==0){
		showNoDataPopUp();
		$('#noDataModalLabel').text("No IMSI's with failures during this time period");
	}
	else{
		$.each(list, function(index, baseData) {
			$('#imsiFailCountAndDurationCalculation').append(
					'<tr>'
					+ '<td>' + baseData[0] + '</td>' + '<td>' + baseData[1]
					+ '</td>' + '<td>' + parseInt(baseData[2])/1000 + ' </td>');
			});
			
		$('#imsiFailCountAndDurationCalculation').dataTable();
		$('#placeWhereQueriesAreDisplayed').show();	
	}
};