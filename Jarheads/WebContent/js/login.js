var rootURL = 'http://localhost:8080/Jarheads/rest/users/login/'

$(document).ready(function() {
	$('#logout-btn').hide();
	$('.hide').not('#login').hide();

	initializeLoginFields();

	$(document).on('submit', '#login form', function(event) {
		var username = $("#inputName").val();
		var password = $("#inputPassword").val();
		isUserValid(username, password)
		return false;
	});
	
	
	$(document).on('click', '#logout-confirmed', function(event) {
		$('#logoutModal').on('hidden.bs.modal', function () {		
			location.reload();
		 })
	});
	
});

var showPageAfterLogout = function(){
	
	$('#logoutModal').on('hidden.bs.modal', function () {		
		localStorage.clear();
		$("#home").addClass("hide");
		$('#login').show();
		$('.hide').not('#login').hide();
	 })
	 
}

var initializeLoginFields = function() {
	if (!$.isEmptyObject(localStorage.currentUser)) {
		var user = $.parseJSON(localStorage.currentUser);
		$("#inputName").val(user.userName);
		$("#inputPassword").val(user.password);
	}
}

var isUserValid = function(username, password) {

	if (username == "" || password == "") {
		$("#userValidation").text(
				"Username and password fields cannot be blank.").show();
		return;
	}

	console.log("function: is user valid");
	var promise = $.ajax({
		type : 'POST',
		url : rootURL + 'username=' + username + '&password=' + password,
	});

	promise.done(renderSuccessLogin);
	promise.fail(renderLoginError);

	return false;
};

var renderSuccessLogin = function(data, textStatus, errorThrown) {
	console.log('Successfully Logged in. Current user group is:'
			+ data.userType.name);
	localStorage.currentUser = JSON.stringify(data);
	$("#userValidation").text("").show();
	$('#logout-btn').show();
	renderCurrentUserScreens(data.userType.name);
}

var renderCurrentUserScreens = function(userType) {
	$('#login').hide();
	$('.hide').not('#login').show();
	$("#home").removeClass("hide");
	$('#home').show();

	if (userType == "System Administrator") {
		$('#tablist').children().not('.admin').hide();
		$('.tab-content').children().not('.admin').hide();
		$('#queries section #queryButtons').children().not('.admin').hide();
		
		$( "#tablist, .tab-content").children().find(".admin").show();
		$( "*" ).find(".admin.default").addClass("active");
		$( "*" ).find(".admin.default").show();
		
	}
	
	if (userType == "Customer Service Representative") {
		$('#tablist').children().not('.cust-rep').hide();
		$('.tab-content').children().not('.cust-rep').hide();
		$('#queries section #queryButtons').children().not('.cust-rep').hide();
		
		$( "#tablist, .tab-content").children().find(".cust-rep").show();
		$( "*" ).find(".cust-rep.default").addClass("active");
		$( "*" ).find(".cust-rep.default").show();
		findAmountOfRecordsInDatabase();
	}
		
	if (userType == "Network Management Engineer") {
		$('#tablist').children().not('.net-man').hide();
		$('.tab-content').children().not('.net-man').hide();
		$('#queries section #queryButtons').children().not('.net-man').hide();
		
		$( "#tablist, .tab-content").children().find(".net-man").show();
		$( "*" ).find(".net-man.default").addClass("active");
		$( "*" ).find(".net-man.default").show();
		findAmountOfRecordsInDatabase();
	}
	
	if (userType == "Support Engineer") {
		$('#tablist').children().not('.supp-eng').hide();
		$('.tab-content').children().not('.supp-eng').hide();
		$('#queries section #queryButtons').children().not('.supp-eng').hide();
		
		$( "#tablist, .tab-content").children().find(".supp-eng").show();
		$( "*" ).find(".supp-eng.default").addClass("active");
		$( "*" ).find(".supp-eng.default").show();
		findAmountOfRecordsInDatabase();
	}	
}

var renderLoginError = function(data, textStatus, errorThrown) {
	console.log("Error: " + data.status + " =" + textStatus + " =  "
			+ errorThrown);
	console.log((data.status == 404));
	if (data.status == 404) {
		$("#userValidation")	
				.text("Please enter valid credentials to continue.").show();
	} else {
		$("#userValidation").text(
				"Username and password fields cannot be blank.").show();
	}
}