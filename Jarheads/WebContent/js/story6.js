var distinctList;
var currentIMSINumber;
var makeStory6 = function() {
	$('#queryPanel').empty();
	$('#queryPanel')
	.append(
			"<p id=\"story7Description\"><h3 id=\"headerForQuery\">"
			+ "</h3></p>"
			+ "<!-- ====================== Story6 IMSI input ====================== -->"
			+ "<p id=\"query6Validation\" class=\"text-danger\"></p>"
			+ "<label for=\"searchKey6\">Enter IMSI:</label> "
			+ "<input type=\"text\" id=\"searchKey6\" list=\"s6dropdown\" name=\"searchKey6\">"
			+ "<datalist id=s6dropdown></datalist>"
			+ "<span class=\"glyphicon glyphicon-info-sign\" id=\"dateInfo\" data-toggle=\"tooltip\" "
			+ "data-placement=\"bottom\""
			+ "title=\"This query returns the cause code and description for all failures affecting the given IMSI.\">"
			+ "</span>"
			+ "     "
			+ "<button type=\"button\" class=\"btn btn-info\" id=\"searchButton6\">"
			+ "<span class=\"glyphicon glyphicon-search\"></span> Search</button>"
			+ "<br><br><br>"
			+ "<!-- ====================== End of S6 IMSI Input   ====================== -->")
};

var story6Tbl = function() {
	buildTabsNoGraph();
	$('#placeWhereQueriesAreDisplayed').hide();
	$('#tableResultsTab').empty();
	$('#tableResultsTab')
	.append(
			"<div class=\"table-responsive\">"
			+ "		<table id=\"query6\" class=\"table table-striped table-bordered\">"
			+ "			<thead>" + "				<tr>" 
			+ "					<th>Event Id</th>"
			+ "					<th>Cause Code</th>"
			+ "					<th>Description</th>" + "				</tr>"
			+ "			</thead>" + "			<tbody id=\"dataTableBody\">"
			+ "" + "			</tbody>" + "			</table>" + "	</div>")

			$('#query6').hide();
	$('#placeWhereQueriesAreDisplayed').prepend("<b><h3><div id=\"displayQueryTitle\"></div></h3></b>");
	$('#displayQueryTitle').hide();
}

var search6 = function(searchKey) {
	if (searchKey == '') {
		$("#query6Validation").text("Search fields cannot be blank.").show();
		return;
	} else if (searchKey.length != 15) {
		$("#query6Validation").text("IMSI Numbers MUST be a 15 digit number")
		.show();
		return;
	} else if (searchKey.match(/[^\d]/)) {
		$("#query6Validation").text("IMSI Numbers MUST contain digits ONLY")
		.show();
		return;
	}
	else {
		currentIMSINumber=searchKey;
		$('#displayQueryTitle').empty();
		$('#displayQueryTitle').append("Call Failure Codes for IMSI #"+currentIMSINumber);
		$("#query6Validation").hide();
		findByImsiFor6(searchKey);


	}
};

var findByImsiFor6 = function(searchKey) {
	console.log('findByImsi: ' + searchKey);
	$.ajax({
		type : 'GET',
		url : 'http://localhost:8080/Jarheads/rest/basedata/findCauseFailuresByImsi/'
			+ searchKey,
			dataType : "json",
			success : renderImsiFor6,
	});
};

var renderImsiFor6 = function(data) {
	console.log('renderImsi In story 6');

	var dataTable = $('#query6').dataTable();
	dataTable.fnClearTable();
	dataTable.fnDraw();
	dataTable.fnDestroy();
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	if(list.length==0){
		showNoDataPopUp();
		$('#noDataModalLabel').text("No failures for this IMSI");
	}
	else{
		$.each(list, function(index, baseData) {
			console.log('baseData' + baseData)
			var t = $('#query6').DataTable();
			t.row.add([ baseData[1], baseData[2], baseData[3] ])
			.draw();
			$('#query6').show();
		});
		clearFileSelectionField();
		$('#displayQueryTitle').show();
		console.log('displayQueryTitle ShOWN');
		window.scrollTo(0, 0);
		$('#placeWhereQueriesAreDisplayed').show();	
	}
};

var populateIMSIListForDropdownBoxSearch = function() {
	console.log('findDistinctImsi');
	$.ajax({
		type : 'GET',
		url : 'http://localhost:8080/Jarheads/rest/basedata/findListOfDistinctImsisForDropdown/',
		dataType : "json",
		success : renderDropDownBoxFromIMSIList
	});
};

var renderDropDownBoxFromIMSIList = function(data) {
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$.each(list, function(index, errorData) {
		distinctList += '<option value=' + errorData + '>';
	});
	$('#s6dropdown').append(distinctList);
};
