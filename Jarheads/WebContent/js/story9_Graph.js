var renderStory9Graph = function(date) {

	var displayDate = date.split('&'); 
	var startDateAndTime = displayDate[0]+'/'+displayDate[1]+'/'+displayDate[2]+' '+
		displayDate[3]+':'+displayDate[4];
	var endDateAndTime = displayDate[5]+'/'+displayDate[6]+'/'+displayDate[7]+' '+
		displayDate[8]+':'+displayDate[9];
	
	var myTitle = "Failures Per IMSI and Total Duration";
	var mySubtitle = 'Date between: ' + startDateAndTime+' and '+endDateAndTime;
	var yAxisLabel = "Total Downtime (seconds)";
	var xAxisLabel = "IMSI"
	var level1URL = 'http://localhost:8080/Jarheads/rest/basedata/ImsiFailureCountAndDuration/'+date;
	var level2URL = 'http://localhost:8080/Jarheads/rest/basedata/failuresBreakDownPerIMSI/';
	
	getBaseData(function(myDataList) {
		baseData = processData(myDataList);

		$('#graphResultsTab').empty();
		var myGraph = $('#graphResultsTab')
		.highcharts(
				{
					chart : {
						type : 'column'
					},
					title : {
						text : myTitle
					},

					subtitle : {
						text : mySubtitle
					},
					xAxis : {
						max: 10,
						title : {
							text : xAxisLabel
						},
						type : 'category',
						labels : {
							rotation : -45,
							style : {
								fontSize : '10px',
								fontFamily : 'Verdana, sans-serif'
							},
						}
					},
					yAxis : {
						min : 0,
						title : {
							text : yAxisLabel
						}
					},

					legend : {
						enabled : false
					},
					scrollbar : {
						enabled : true
					},
					plotOptions : {
						series : {
							borderWidth : 0,
							dataLabels : {
								enabled : true,
							},
							turboThreshold : 0,
							point : {
								events : {
									click : function(event) {
										var chart = this.series.chart;
										console.log(this.series.chart.title.textStr)
										var imsi = this.name;
										$
										.ajax({
											url : level2URL+date+'&'+imsi,
											success : function(data) {
												console.log('url is: '+ this.url)

												var myDataList = [];
												var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
												$.each(list, function(index, myData) {
													myDataList.push([myData[0], parseFloat(myData[1])/1000]);
												});

												swapSeries(chart,"Downtime per Failure",myDataList);
											},
											dataType : "json"
										});
									}
								}
							}
						}
					},

					series : [ {
						name : 'Downtime per IMSI',
						colorByPoint : true,
						data : baseData
					} ],
					exporting: {
						buttons: {
							customButton: {
								text: 'Go back',
								onclick: function () {
									$('#graphResultsTab').empty();
									renderStory9Graph(date)
								}
							}
						}
					}
				});
	});

	function swapSeries(chart, name, data) {
		chart.series[0].remove();
		chart.addSeries({
			data : data,
			name : name,
			colorByPoint : true,
		});
//		console.log('Length of drill down: '+data.length);
		if(name=='Downtime per Failure'){
			console.log('TRUE');
			chart.xAxis[0].setExtremes(0,data.length-1);
		}
		else{
			chart.xAxis[0].setExtremes(null,null);
		}
	}

	function getBaseData(callback) {
		$
		.ajax({
			url : level1URL,
			success : function(data) {
				var myDataList = []
				var list = data == null ? []
				: (data instanceof Array ? data : [ data ]);
				$.each(list, function(index, myData) {
					myDataList
					.push([myData[0], myData[2]]);
				});
				console.log(myDataList);

				if (typeof callback === "function")
					callback(myDataList);
			},
			dataType : "json"
		});

	}

	function processData(myDataList) {
		console.log(myDataList);
		var myLevel1Data = myDataList
		var drilldownsData = {};
		var convertedLevel1DataArrayToObject = [];
		myLevel1Data.forEach(function(entry) {
			convertedLevel1DataArrayToObject.push({
				name : entry[0],
				y : (parseFloat(entry[1])/1000),
				drilldown : true

			});
		});

		return convertedLevel1DataArrayToObject;

	}
}