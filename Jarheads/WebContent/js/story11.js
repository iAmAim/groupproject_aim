/**
 * Story 11 - As a Network Management Engineer I want to see the Top 10 Market/Operator/Cell ID
 *  combinations that had call failures during a time period 
 */

var maketop10Failures=function(){
	$('#placeWhereQueriesAreDisplayed').hide();
	$('#tableResultsTab').empty();
	$('#queryPanel').empty();
	buildResultsTabs();
	buildGenericCalendarHTML();
	$('#queryPanel').append("<span class=\"glyphicon glyphicon-info-sign\" id=\"dateInfo\" data-toggle=\"tooltip\" "
			+"data-placement=\"bottom\""
			+"title=\"This query returns the top 10 worst market/operator/cell id combinations during a defined time period.\">"
			+"</span>");

	$('#tableResultsTab').append("<div class=\"table-responsive\">"
			+"<!--<table id=\"myTable\" class=\"display table\" width=\"100%\">-->"
			+"<table id=\"top10FailuresTable\" class=\"table table-striped table-bordered\">"
			+"<thead>"
			+"<tr>"
			+"<th>Market</th>"
			+"<th>Operator</th>"
			+"<th>Cell ID</th>"
			+"<th>Number Of Failures</th>"
			+"</tr>"
			+"</thead>"
			+"<tbody id=\"dataTableBody7\">"
			+""
			+"</tbody>"
			+"</table>"
			+"</div>"
	)	
	$('#top10FailuresTable').hide();
	buildCalendar();
}

var getTop10FailuresWithDate = function(dateInfo) {
	$("#graphResultsTab").hide();
	console.log('getTop10FailuresWithDate')
	$.ajax({
		type : 'GET',
		contentType : "application/json",
		url : "rest/basedata/top10FailuresWithDate/"+dateInfo,
		dataType:"json",
		success : function(data){
			console.log('DATA LENGTH---->'+data.length)
			if(data.length==0){
				showNoDataPopUp();
				$('#noDataModalLabel').text("No failures during this time period");
			}
			else{
				renderStory11Graph(dateInfo);
				renderTop10List(data);	
			}
		}
	});
};



var renderTop10List = function(data) {

	console.log('function: renderTop10List')
	console.log(data);
	var dataTable = $('#top10FailuresTable').dataTable();


	dataTable.fnClearTable();
	dataTable.fnDraw();
	dataTable.fnDestroy();
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$.each(list,function(index, myData) {	
//		var t = $('#top10FailuresTable').DataTable();
//		t.fnSort([[3,'desc']]);
//		t.row.add(
//		[ myData[0],myData[1], myData[2], myData[3]]).draw();		
		$('#top10FailuresTable').append(
				'<tr>'
				+ '<td>' + myData[0] + '</td>' + '<td>' + myData[1]
				+ '</td>'+ '<td>' + myData[2]
				+ '</td>'+ '<td>' + myData[3]
				+ '</td>');

	});
	oTable=$('#top10FailuresTable').dataTable({
		"bPaginate": false,
		"sPaginationType": "full_numbers",
		"bFilter": false,
		"bSearchable":false,
		"bSort":false,
		"bInfo":false,});
	oTable.fnSort([[3,'desc']]);


	console.log('rendering top 10 list finished.');
	$('#top10FailuresTable').show();
	$('#placeWhereQueriesAreDisplayed').show();


};


