/** 
 * This JavaScript file provides method for the query that retrieves the number of
 * failures associated with the provides IMSI over a defined period of time.
 * 
 * @version 1.0
 */

$(document).ready(function () {
	/**
	 * This function is used to close the no data pop up modal when
	 * the OK button is clicked
	 */
	$(document).on('click', '#ok', function() {
		console.log('ok button clicked');
		$('#noDataModal').modal('hide');
	});
});

/**
 * This method is used to retrieve the number of failures for the provided IMSI
 * over the defined period. This method is called once the find all results button is 
 * clicked. The date and time parameters selected by the user are passed into this method.
 * 
 * @param dateInfo the date and time period selected for the query.
 */
var failuresPerIMSI = function(dateInfo) {
	$('#failuresPerIMSITable').hide();
	var imsi = $('#usersImsiInput').val();
	console.log('IMSI enetered: '+usersImsiInput);
	console.log('failuresPerIMSI(): ' +imsi);
	
	if (imsi == '') {
		$("#invalidUserInput").text("Search fields cannot be blank.").show();
	}
	else if(isNaN(imsi)){
		$("#invalidUserInput").text("IMSI's contain only numbers").show();
	}
	else if(imsi.length!=15){
		$("#invalidUserInput").text("Input must be a valid IMSI number (15 digits)").show();
	}
	else{
		$("#invalidUserInput").hide();
		$.ajax({
			type: 'GET',
			url: 'http://localhost:8080/Jarheads/rest/basedata/failuresPerIMSI/'+dateInfo+'&'+imsi,
			dataType: "json",
			success: renderFailuresPerIMSITable,
		});
	}
};

/**
 * This method is used to display a pop up message to the user informing them 
 * that there is no data available in the database for the valid IMSI and time period
 * that was defined by them for this query.
 */
var showNoDataPopUp = function(){
	console.log('showNoDataPopUp()');
	
	$('#noDataModal').remove();

	$('#queryPanel').append('<div class="modal fade bs-example-modal-sm" id="noDataModal"'+
			'tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"'+
			'aria-hidden="true">'+
			'<div class="modal-dialog modal-sm">'+
			'<div class="modal-content">'+
			'<div class="modal-header">'+
			'<h4 class="modal-title">No data returned</h4>'+
			'</div>'+
			'<div class="modal-body">'+
			'<label id="noDataModalLabel">No failures for this IMSI during this time period</label><br>'+
			'<button type="submit" class="btn btn-success btn-sm" id="ok">OK</button>'+
			'</div>'+
			'</div>'+
			'</div>'+
	'</div>');
	
	$('#noDataModal').modal('show');
}

/**
 * This method is used to create the data table which will display the 
 * results of the query.
 * 
 * @param data the data retrieved from the query
 */
var renderFailuresPerIMSITable = function(data) {
	
	console.log('renderFailuresPerIMSITable()');

	var dataTable = $('#failuresPerIMSITable').dataTable();
	
	dataTable.fnClearTable();
	dataTable.fnDraw();
	dataTable.fnDestroy();
	
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$.each(list,function(index, imsiData) {
		var t = $('#failuresPerIMSITable').DataTable({
			bFilter: false,
			bInfo: false,
			bPaginate: false,
			bSort: false,
			aoColumns: [
			             { sWidth: '50%' },
			             { sWidth: '50%' }
			           ]
		});
		if(imsiData[1]==0){
			showNoDataPopUp();
		}
		else{
			t.row.add(
					[imsiData[0],imsiData[1]]).draw();
			$('#failuresPerIMSITable').show();
			window.scrollTo(0, 0);
			$('#placeWhereQueriesAreDisplayed').show();
		}
	});	
};

/**
 * This method is used to create the IMSI input field and the data table
 * that will hold the results of the query.
 */
var makeFailurePerImsiFields=function() {
	$('#placeWhereQueriesAreDisplayed').empty();
	$('#queryPanel').empty();			
	buildGenericCalendarHTML();
	$('#queryPanel').append("<span class=\"glyphicon glyphicon-info-sign\" id=\"dateInfo\" data-toggle=\"tooltip\" "
	+"data-placement=\"bottom\""
	+"title=\"The query returns all the IMSI's that have failed during the defined time period.\">"
	+"</span>");
	$('#moreUserInputDiv').append('<p id="invalidUserInput" class="text-danger"></p>'
			+'<label>IMSI:</label>'
			+'<input type="text" id="usersImsiInput" list="imsiDropDownList" name="imsiDropDownList">'
			+'<datalist id=imsiDropDownList></datalist>'
		);
	buildCalendar();
};

var makeFailurePerImsiTbl = function(){
	buildTabsNoGraph();
	$('#placeWhereQueriesAreDisplayed').hide();
	$('#tableResultsTab').empty();
	$('#tableResultsTab').append('<div class="table-responsive">'
			+'<!--<table id="failuresPerIMSITable" class="display table" width="100%">-->'
			+'		<table id="failuresPerIMSITable" class="table table-striped table-bordered">'
			+'			<thead>'
			+'				<tr>'
			+'										<th>IMSI</th>'
			+'										<th>Total Failures</th>'
								+'				</tr>'
								+'			</thead>'
								+'			<tbody id="dataTableBody5">'
								+'			</tbody>'
			+'			</table>'
			+'			</div>'
		);
	$('#failuresPerIMSITable').hide();
}

/**
 * This method is used to get all the current IMSI's that are present in the database.
 */
var findAllIMSIs = function(){
	console.log('findAllIMSIs()');
	$.ajax({
		type: 'GET',
		url: 'http://localhost:8080/Jarheads/rest/basedata/findDistinctImsi',
		dataType: "json",
		success: renderIMSIDropDownList
	});
};
/**
 * This method populate the drop down list associated with the IMSI input field
 * with the retrieved IMSI's to allow the user to select from the list of IMSIs.
 */
var renderIMSIDropDownList = function(data) {
	var imsiList;
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$.each(list,function(index, imsi) {	
		imsiList+='<option value='+imsi+'>'+imsi+'</option>';
	});
	$('#imsiDropDownList').append(imsiList);
};
