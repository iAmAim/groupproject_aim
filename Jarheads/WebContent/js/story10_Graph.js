var renderStory10Graph = function(myDeviceModel) {

	var myTitle = "Unique Failures per Device Model"
	var mySubtitle = 'Device: ' + myDeviceModel
	var yAxisLabel = "Number of Failures"
	getBaseData(function(myDataList) {
		baseData = processData(myDataList);
	
		$('#graphResultsTab').empty();
		var myGraph = $('#graphResultsTab')
				.highcharts(
						{
							chart : {
								type : 'column'
							},
							title : {
								text : myTitle
							},

							subtitle : {
								text : mySubtitle
							},
							xAxis : {
								max: 20,
								type : 'category',
								labels : {
									rotation : -45,
									style : {
										fontSize : '10px',
										fontFamily : 'Verdana, sans-serif'
									}
								}

							},
							yAxis : {
								min : 0,
								title : {
									text : yAxisLabel
								}
							},
				             scrollbar: {
				 	            enabled:true,
				 				barBackgroundColor: 'gray',
				 				barBorderRadius: 7,
				 				barBorderWidth: 0,
				 				buttonBackgroundColor: 'gray',
				 				buttonBorderWidth: 0,
				 				buttonArrowColor: 'yellow',
				 				buttonBorderRadius: 7,
				 				rifleColor: 'yellow',
				 				trackBackgroundColor: 'white',
				 				trackBorderWidth: 1,
				 				trackBorderColor: 'silver',
				 				trackBorderRadius: 7
				 		    },

							legend : {
								enabled : false
							},

							plotOptions : {
								series : {
									borderWidth : 0,
									dataLabels : {
										enabled : true
									},
									point : {
										events : {
											click : function(event) {
												var chart = this.series.chart;
												console
														.log(this.series.chart.title.textStr)
												var eventDescription = this.name;
												var seriesName = this.phoneModel;
												if(!seriesName==""){
													$
													.ajax({
														url : 'http://localhost:8080/Jarheads/rest/basedata/failuresPerDeviceModelAndEvent/model='
																+ this.phoneModel
																+ '&imsi='
																+ eventDescription,
														success : function(
																data) {
															console
																	.log('url is: '
																			+ this.url)
															swapSeries(
																	chart,
																	"Failures",
																	data);
														},
														dataType : "json"
													});
												}
											}
										}
									}
								}
							},

							series : [ {
								name : 'Unique Failures per Device',
								colorByPoint : true,
								data : baseData
							} ],
							 exporting: {
							        buttons: {
							            customButton: {
							                text: 'Go back',
							                onclick: function () {
							                	$('#graphResultsTab').empty();
							                	renderStory10Graph(myDeviceModel);
							                }
							            }
							        }
							    }
						});
		
			

	});

	function swapSeries(chart, name, data) {
		chart.series[0].remove();
		chart.addSeries({
			data : data,
			name : name,
			colorByPoint : true
		});
	}

	$('#iniChart').click(function() {
		swapSeries(Highcharts.charts[0], 'Failures', baseData);
	});

	function getBaseData(callback) {
		$
				.ajax({
					url : 'http://localhost:8080/Jarheads/rest/basedata/failuresPerDeviceModel/'
							+ myDeviceModel,
					success : function(data) {
						var myDataList = []
						var list = data == null ? []
								: (data instanceof Array ? data : [ data ]);
						$.each(list, function(index, myData) {
							myDataList
									.push([ myData[3], myData[4], myData[0] ]); // Description,
							// failures,
							// Devicemodel,
						});
						console.log(myDataList);

						if (typeof callback === "function")
							callback(myDataList);
					},
					dataType : "json"
				});

	}

	function processData(myDataList) {
		var myLevel1Data = myDataList// [['INITIAL CTXT SETUP-CSFB LICENSE
		// MISSING',5,'GX-28'],['UE CTXT
		// RELEASE-AUTHENTICATION
		// FAILURE',3,'GX-28']] // sample lvl1
		// data coming from WS
		// http://localhost:8080/Jarheads/rest/basedata/failuresPerDeviceModel/GX-28
		// loop through level 1 data and build the drildowns data object
		var drilldownsData = {};
		var convertedLevel1DataArrayToObject = [];
		myLevel1Data.forEach(function(entry) {
			// for building initial data series
			convertedLevel1DataArrayToObject.push({
				name : entry[0],
				y : entry[1],
				phoneModel : entry[2],
				drilldown : true

			});
		});

		return convertedLevel1DataArrayToObject;

	}

}