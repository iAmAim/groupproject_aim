var renderStory13Graph = function(date) {
	
	date = typeof date == "undefined" ? "_" : date;
	var myTitle = "Top 10 Worst Market/Operator/Cell ID"
	var mySubtitle = 'Market/Operator/Cells combo with the highest number of failures'
	var yAxisLabel = "Number of Failures"
	var level1URL = 'http://localhost:8080/Jarheads/rest/basedata/Top10MarketByDate/'+date
	var level2URL = 'http://localhost:8080/Jarheads/rest/basedata/Top10OperatorsPerMarketByDate/'
	var level3URL = 'http://localhost:8080/Jarheads/rest/basedata/Top10CellsPerOperatorByDate/' //TODO refactor this to show Cells. Then move to level 4!
	getBaseData(function(myDataList) {
		baseData = processData(myDataList);	
		
		var myGraph = $('#placeWhereQueriesAreDisplayed')
				.highcharts(
						{
							chart : {
								type : 'pie'
							},
							title : {
								text : myTitle
							},

							subtitle : {
								text : mySubtitle
							},
							xAxis : {
								type : 'category',
								labels : {
									rotation : -45,
									style : {
										fontSize : '10px',
										fontFamily : 'Verdana, sans-serif'
									}
								}

							},
							yAxis : {
								min : 0,
								title : {
									text : yAxisLabel
								}
							},

							legend : {
								enabled : false
							},
				

							plotOptions : {
								series : {
									borderWidth : 0,
									dataLabels : {
										enabled : true
									},
									point : {
										events : {
											click : function(event) {
							                    console.log(this);
							                    console.log(this.name);
							                    console.log(this.y);
							                    console.log(this.drilldown);
												
												var chart = this.series.chart;
												console.log(this.series.chart.title.textStr)
												var market = this.market;
												var operator = this.operator;
												var drillDownURL = "";
												var currentLevel = this.level;
												if(this.level == 1){
													console.log('Current level is 1. Going to  Level 2..')
													 drillDownURL = level2URL+date+'&'+market
												}
												if(this.level == 2){
													console.log('Current level is 2. Going to Level 3..')
													 drillDownURL = level3URL+date+'&'+operator
												}
								
													$
													.ajax({
														url : drillDownURL,
														success : function(
																data) {
															console
																	.log('url is: '
																			+ this.url)
															swapSeries(
																	chart,
																	"Failures",
																	data,++currentLevel);
														},
														dataType : "json"
													});
											}
										}
									}
								}
							},

							series : [ {
								name : 'Failures',
								colorByPoint : true,
								data : baseData
							} ],
							 exporting: {
							        buttons: {
							            customButton: {
							                text: 'Go back',
							                onclick: function () {
							                	//swapSeries(Highcharts.charts[0], 'Failures', baseData);
							                	$('#placeWhereQueriesAreDisplayed').empty();
							                	renderStory13Graph(date);
							                }
							            }
							        }
							    }
						});
		
	});
	/**
	 * @param name = name of the Y axis on hover */
	function swapSeries(chart, name, data,currentLevel) {
		console.log('current level is now: ' + currentLevel)
		console.log('current chart is now: ' + chart)
		chart.series[0].remove();
		
		var currentLevelData = [];
		if(currentLevel ==2){
			data.forEach(function(entry) {
				currentLevelData.push({
					name : entry[0],
					y : entry[1],
					drilldown : true,
					operator: entry[0],
					level: currentLevel
			});
		  });
		}
		else if(currentLevel ==3){
			data.forEach(function(entry) {
				currentLevelData.push({
					name : 'Cell '+entry[0],
					y : entry[1],
					drilldown : true,
					operator: entry[0],
					level: currentLevel
			});
		  });
		}
		
		else{
			currentLevelData = data;
		}

		chart.addSeries({
			data : currentLevelData,
			name : name,
			colorByPoint : true
		});
	}

	$('#iniChart').click(function() {
		swapSeries(Highcharts.charts[0], 'Failures', baseData);
	});

	function getBaseData(callback) {
		$
				.ajax({
					url : level1URL,
					success : function(data) {
						var myDataList = []
						var list = data == null ? []
								: (data instanceof Array ? data : [ data ]);
						$.each(list, function(index, myData) {
							myDataList
									.push([ myData[0], myData[1] ]);
						});
						console.log(myDataList);

						if (typeof callback === "function")
							callback(myDataList);
					},
					dataType : "json"
				});

	}

	function processData(myDataList) {
		var myLevel1Data = myDataList
		var drilldownsData = {};
		var convertedLevel1DataArrayToObject = [];
		myLevel1Data.forEach(function(entry) {
			// for building initial data series
			convertedLevel1DataArrayToObject.push({
				name : entry[0],
				y : entry[1],
				drilldown : true,
				market: entry[0],
				level: 1

			});
		});

		return convertedLevel1DataArrayToObject;

	}

}


