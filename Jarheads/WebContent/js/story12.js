/** Network Management  - Query: Top 10 IMSI's with call failures over a time period */

var makeStory12=function(){
	$('#placeWhereQueriesAreDisplayed').hide();
	$('#tableResultsTab').empty();
	$('#queryPanel').empty();
	buildResultsTabs();
	buildGenericCalendarHTML();
	$('#story7Description').html();
	$('#queryPanel').append($('#queryPanel').append("<span class=\"glyphicon glyphicon-info-sign\" id=\"dateInfo\" data-toggle=\"tooltip\" "
			+"data-placement=\"bottom\""
			+"title=\"This query returns the top 10 IMSI's with call failures during the defined time period.\">"
			+"</span>"));
	$('#tableResultsTab').append("<div class=\"table-responsive\">"
			+"<!--<table id=\"myTable\" class=\"display table\" width=\"100%\">-->"
			+"		<table id=\"top10ImsiWithFailuresOverTimePeriod\" class=\"table table-striped table-bordered\">"
			+"			<thead>"
			+"				<tr>"
			+"					<th>IMSI</th>"
			+"					<th>Total Failures</th>"
			+"				</tr>"
			+"			</thead>"
			+"			<tbody id=\"dataTableBody12\">"
			+""
			+"			</tbody>"
			+"			</table>"
			+"			</div>"
	)	
	$('#top10ImsiWithFailuresOverTimePeriod').hide();
	buildCalendar();
};


/** Network Management  - Rest call*/
var findTop10ImsiWithFailures=function(datesInfo){
	$("#graphResultsTab").hide();
	console.log(datesInfo);
	$.ajax({
		type:'GET',
		contentType:"application/json",
		url:"rest/basedata/Top10ImsiWithFailureList/"+datesInfo,
		dataType:"json",
		success:  function(data){
			renderTop10ImsiWithFailures(data,datesInfo);
		},
	});
}

/** Network Management  - render the top 10 list*/
var renderTop10ImsiWithFailures = function(data,datesInfoAndImsi) {

	//renderGraph
	renderStory12Graph(datesInfoAndImsi);

	if (data == 0) {
		showNoDataPopUp();
		$('#noDataModalLabel').text("No failures during this time period");

	} else {
		var dataTable = $('#top10ImsiWithFailuresOverTimePeriod').dataTable();
		dataTable.fnClearTable();
		dataTable.fnDraw();
		dataTable.fnDestroy();
		list = data == null ? [] : (data instanceof Array ? data : [ data ]);

		$.each(list, function(index, baseData) {

			$('#top10ImsiWithFailuresOverTimePeriod').append(
					'<tr>'
					+ '<td>' + baseData[0] + '</td>' + '<td>' + baseData[1]
					+ '</td>');

		});
		var orderedTable = $('#top10ImsiWithFailuresOverTimePeriod').dataTable({
			"bPaginate": false,
			"sPaginationType": "full_numbers",
			"bFilter": false,
			"bSearchable":false,
			"bSort":false,
			"bInfo":false,	
		});

		orderedTable.fnSort( [ [1,'desc'], [0,'asc'] ] );
	}

	$('#placeWhereQueriesAreDisplayed').show();
};

