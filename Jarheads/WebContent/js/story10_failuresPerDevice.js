
var getfailuresPerDeviceList = function(searchKey) {

	$("#query10Validation").hide();

	console.log('getfailuresPerDeviceList: ' +searchKey);
	$("#graphResultsTab").hide();
	$.ajax({
		type: 'GET',
		url: 'http://localhost:8080/Jarheads/rest/basedata/failuresPerDeviceModel/'+searchKey,
		dataType: "json",
		success: function(data){
			renderFailuresPerDeviceTblAndGraph(data,searchKey);
		},
		error: function(){
			console.log('searchKey: ' + searchKey)
			if (searchKey == '') {
				$("#query10Validation").text(
				"Search fields cannot be blank.").show();
			}
			console.log('Error on renderFailuresPerDeviceTbl');
		}
	});
};


var renderFailuresPerDeviceTblAndGraph = function(data,searchKey) {

	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	if(list.length==0){
		showNoDataPopUp();
		$('#noDataModalLabel').text("No failures for this model of phone");
	}
	else{
		//Render Graph
		renderStory10Graph(searchKey);


		//Render Table
		console.log('renderFailuresPerDeviceTbl');

		var dataTable = $('#failuresPerDeviceTbl').dataTable();
		console.log("data: " + data);
		dataTable.fnClearTable();
		dataTable.fnDraw();
		dataTable.fnDestroy();

		console.log("data: " + data);
		$.each(list,function(index, baseData) {	
			console.log("data: "+baseData)
			var t = $('#failuresPerDeviceTbl').DataTable();
			t.row.add(
					[baseData[1],baseData[2], baseData[3],baseData[4]
					]).draw();
		});

		console.log('rendering failuresPerDeviceTbl finished.');
		$('#placeWhereQueriesAreDisplayed').show();
		$('#failuresPerDeviceTbl').show();	
	}
}


/* Story 10 */
var makeStory10=function(){
	$('#queryPanel').empty();
	$('#queryPanel').append("<p id=\"query10Validation\" class=\"text-danger\"></p>"
			+"<label for=\"searchKey\">Phone Model:</label> "
			+"<input type=\"text\" id=\"searchKey\" list=\"ueTypeDropDown\" name=\"ueTypeDropDown	 		\">" 
			+"<datalist id=ueTypeDropDown></datalist>"
			+"<span class=\"glyphicon glyphicon-info-sign\" id=\"dateInfo\" data-toggle=\"tooltip\" "
			+"data-placement=\"bottom\""
			+"title=\"This query returns the event id,cause code and the number of failures for " +
			"a given model of phone.\">"
			+"</span>"
			+"<button type=\"button\" class=\"btn btn-info\" id=\"failuresPerDeviceSearchBtn\">"
			+"<span class=\"glyphicon glyphicon-search\"></span> Search</button>"
			+"<br><br><br>"
	)	
}

var makeStory10Tbl = function(){
	$('#placeWhereQueriesAreDisplayed').hide();
	buildResultsTabs();
	$('#tableResultsTab').empty();
	$('#tableResultsTab').append("<div class=\"table-responsive\">"
			+"<!--<table id=\"failuresPerDeviceTbl\" class=\"display table\" width=\"100%\">-->"
			+"		<table id=\"failuresPerDeviceTbl\" class=\"table table-striped table-bordered\">"
			+"			<thead>"
			+"				<tr>"
			+"					<th>Event Id</th>"
			+"					<th>Cause Code</th>"
			+"					<th>Description</th>"
			+"					<th>Total Failures</th>"
			+"				</tr>"
			+"			</thead>"
			+"			<tbody id=\"dataTableBody10\">"
			+""
			+"			</tbody>"
			+"			</table>"
			+"			</div>"
	)
	$('#failuresPerDeviceTbl').hide();

}

var findAllUETypes = function(){
	console.log('find All UEtypes');
	$.ajax({
		type: 'GET',
		url: 'http://localhost:8080/Jarheads/rest/uetypes/',
		dataType: "json",
		success: getAllUeTypes
	});
};

var getAllUeTypes = function(data) {
	var ueTypeList;
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$.each(list,function(index, ueType) {	
		ueTypeList+='<option value='+ueType[1]+'>' +ueType[0]+' ' + ueType[1] +'</option>';
	});
	$('#ueTypeDropDown').append(ueTypeList);
};
