
var buildGenericCalendarHTML=function(){
	$('#queryPanel').empty();
	$('#queryPanel').append(
			"<p id=\"story7Description\"><br>"
			+"<!-- ====================== Calender   ====================== -->"
			+"<label for=\"from\" id=\"startDateLabel\">Start Date</label> "
			+"<input type=\"text\" id=\"from\" name=\"from\">" 
			+"<label for=\"to\" id=\"endDateLabel\" >End  Date</label>"
			+"<input type=\"text\" id=\"to\" name=\"to\">"
			+"<br>"
			+"<!-- ====================== End of Calender   ====================== -->"
			+""
			+"<!-- ====================== Drop downs for hours/mins   ====================== -->"
			+"<br>"
			+"<label id=\"startTimeLabel\">Start Time</label> "
			+"<select class=\"selectTimeDropDown\" id=\"hourBoxFrom\" >"
			+"	<option value=\"Hours From\">Hrs</option>"
			+"</select>"
			+" :"
			+"<select class=\"selectTimeDropDown\" id=\"minuteBoxFrom\" >"
			+"	<option value=\"Minutes From\">Mins</option>"
			+"</select>"
			+"</br>"
			+"<label id=\"endTimeLabel\">End Time</label> "
			+"<select class=\"selectTimeDropDown\" id=\"hourBoxTo\">"
			+"	<option value=\"Hours To\">Hrs</option>"
			+"</select>"
			+" :"
			+"<select class=\"selectTimeDropDown\" id=\"minuteBoxTo\">"
			+"	<option value=\"Minutes To\">Mins</option>"
			+"</select>"
			+"</br>"
			+"<div id=\"moreUserInputDiv\"></div>"
			+"</br>"
			+"<input class=\"btn btn-info\" id=\"reset\" type=\"submit\" value=\"Reset\" />"
			+"<input class=\"btn btn-info\" id=\"undoStep\" type=\"submit\" value=\"Back A Step\" />"
			+"<input class=\"btn btn-info\" id=\"buildResults\" type=\"submit\" value=\"Search\" />"
			+"<!-- ====================== End of drop downs   ====================== -->"
			+"<!-- ====================== Table for imsi's with failures over a given time period   ====================== -->"
	)
}

var buildResultsTabs = function(){

	$('#placeWhereQueriesAreDisplayed').append(
			'<div class="bs-example">'
			+'<ul class="nav nav-tabs" id="myTab">'
			+'	<li><a data-toggle="tab" id="tableResultsTabBtn" href="#tableResultsTab">Table'
			+'			Results</a></li>'
			+'	<li><a data-toggle="tab" id="graphResultsTabBtn" href="#graphResultsTab">Graph'
			+'		Results</a></li>'
			+'</ul>'
			+'<div class="tab-content2">'
			+'<div id="tableResultsTab" class="tab-pane fade in active">'
			+'</div>'
			+'<div id="graphResultsTab" class="tab-pane fade">'
			+'</div>'
			+'</div>'
			+'</div>'
	)

}



