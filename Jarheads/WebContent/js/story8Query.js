var currentDateValue;
var alreadyRun=0;
var makeStory8 = function() {
	$('#placeWhereQueriesAreDisplayed').empty();
	$('#queryPanel').empty();			
	buildGenericCalendarHTML();
	buildCalendar();
	$('#queryPanel').append("<span class=\"glyphicon glyphicon-info-sign\" id=\"dateInfo\" data-toggle=\"tooltip\" "
			+"data-placement=\"bottom\""
			+"title=\"This query returns the number of failures for a model of phone" +
					" that occurred during a defined time period.\">"
			+"</span>");
	$('#moreUserInputDiv')
			.append('</br>'
					+'<select id="selectManufacturer" class="selectNameDropDown" onChange="getListOfHandsets()"></select>'
					+'</br>');
	getListOfManufacturers();

	$('#selectManufacturer').hide();
	if (alreadyRun==0){
		alreadyRun=1;
		$('#queryPanel').append('<br><br>');

	}
	$('#countDisplay').empty();
	$( "#undoStep" ).click(function() {
		$('#selectManufacturer').hide();
		$('#selectHandset').hide();
	});
	$( "#minuteBoxTo" ).change(function() {
		$('#selectManufacturer').show();
		$('#selectHandset').hide();

	});


}
var getListOfManufacturers = function() {
	$.ajax({
		type : 'GET',
		contentType : "application/json",
		url : "rest/basedata/listOfManufacturersInBaseData",
		success : renderManufacturersComboBox
	});
};

var renderManufacturersComboBox = function(data) {
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$('#selectManufacturer')
			.prepend(
					'<option id="startValue" value="Select">Select Manufacturer</option>');
	$.each(list, function(index, myData) {
		var manufacturer = myData;
		$('#selectManufacturer').append(
				'<option value="' + myData.trim() + '">' + myData.trim()
						+ '</option>');
	});

};
var getListOfHandsets = function() {
	if (document.getElementById("selectManufacturer").value == "Select") {
		var selectHandsetElement = document.getElementById("selectHandset");
		selectHandsetElement.parentNode.removeChild(selectHandsetElement);
			getListOfManufacturers();
	} else {
		var manufacturer = document.getElementById('selectManufacturer').value;
		console.log("and now about to complete ajax");
		$.ajax({
			type : 'GET',
			contentType : "application/json",
			url : "rest/basedata/listOfHandsetsByManufacturer/" + manufacturer,
			success : renderHandsetsComboBox
		});
	}
};

var renderHandsetsComboBox = function(data) {
	if (document.contains(document.getElementById("selectHandset"))) {
		document.getElementById("selectHandset").remove();
		console.log("Existing selectHandset element removed")
	}
	
	$('#moreUserInputDiv')
			.append(
					'<select id="selectHandset" class="selectNameDropDown" onChange="renderButtonForQuery()"></select>');
	$('#selectHandset')
			.append(
					'<option id="startValueHandsets" value="selectHandset">Select Handset</option>');
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$.each(list, function(index, myData) {
		$('#selectHandset').append(
				'<option value="' + myData + '">' + myData + '</option>');
	});

};
var renderButtonForQuery = function() {
	if (document.getElementById("selectHandset").value == "selectHandset") {
		$("#buildResults").prop('disabled', true);

	} else{
		$("#buildResults").prop('disabled', false);
	}

	
}

var getCountOfErrors = function(dateInfo) {
	var datesToSearch = String(dateInfo);
	currentDate=datesToSearch;
	var manufacturer = document.getElementById('selectManufacturer').value;
	var handset = document.getElementById('selectHandset').value;
	$.ajax({
		type : 'GET',
		contentType : "application/json",
		url : "rest/basedata/getListOfErrorsByHandsetAndDate/" + manufacturer
				+ "&" + handset + "&" + currentDate,
		success : displayCallFailuresCount
	});
};

var displayCallFailuresCount = function(data) {
	if (document.contains(document.getElementById("countDisplay"))) {
		document.getElementById("countDisplay").remove();
		console.log("Existing CountDisplay Removed")
	}
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	var counter = 0;
	$('#placeWhereQueriesAreDisplayed').hide();
	$('#placeWhereQueriesAreDisplayed').empty();

	$('#placeWhereQueriesAreDisplayed')
			.append(
					'<br><br><table id="countDisplay" align="center"><tr id="row1"><td id=cell11>Manufacturer</td><td id=cell12>Handset</td><td id=cell13>Total Errors</td><td id=cell14></td></tr>'
							+ '<tr id="row2"><td id=cell21>Manufacturer</td><td id=cell22>Handset</td><td id=cell23>Errors</td><td id=cell24></td></tr></table>');
	$.each(list, function(index, myData) {
		counter++;
	});
	
	if(counter.valueOf()==0){
		showNoDataPopUp();
		$('#noDataModalLabel').text("No failures for this model of phone during this time period");
	}
	else{
		$('#cell21').html($("#selectManufacturer").val());
		$('#cell22').html($("#selectHandset").val());
		$('#cell23').html(counter.valueOf());
		$('#placeWhereQueriesAreDisplayed').show();	
	}
};