var buildCalendar=(function() {

	findMaxDates();
	findMinDates();


	$( "#hourBoxFrom" ).change(function() {
		$("#undoStep").prop('disabled', false);
		currentMenu="#minuteBoxFrom";
		$("#minuteBoxFrom").prop('disabled', false);
		selectedMinHoursDropDown=$('#hourBoxFrom').val();
		$('#PlaceholderHoursFrom').remove();
		$('#minuteBoxFrom').empty();
		$('#minuteBoxFrom').append("<option id=\"PlaceholderMinutesFrom\" value=\"Mins\">Mins</option>");
		populateMinutesFromDown('minuteBoxFrom')
		$("#hourBoxFrom").prop('disabled', true);
		$('#minuteBoxFrom').css("background","#428bca");
		$('#hourBoxFrom').css("background","#ABABB2");
	});
	$( "#minuteBoxFrom" ).change(function() {
		currentMenu="#hourBoxTo";
		$("#hourBoxTo").prop('disabled', false);
		selectedMinMinutesDropDown=$('#minuteBoxFrom').val();	
		$('#PlaceholderMinutesFrom').remove();
		$('#hourBoxTo').empty();
		$('#hourBoxTo').append("<option id=\"PlaceholderHoursTo\" value=\"Hrs\">Hrs</option>");
		populateHoursToDown('hourBoxTo');
		$("#minuteBoxFrom").prop('disabled', true);
		$('#hourBoxTo').css("background","#428bca");
		$('#minuteBoxFrom').css("background","#ABABB2");
	});
	$( "#hourBoxTo" ).change(function() {
		currentMenu="#minuteBoxTo";
		$("#minuteBoxTo").prop('disabled', false);
		selectedMaxHoursDropDown=$('#hourBoxTo').val();
		$('#PlaceholderHoursTo').remove();
		$('#minuteBoxTo').empty();
		$('#minuteBoxTo').append("<option id=\"PlaceholderMinutesTo\" value=\"Mins\">Mins</option>");
		populateMinutesToDown('minuteBoxTo');
		$("#hourBoxTo").prop('disabled', true);
		$('#minuteBoxTo').css("background","#428bca");
		$('#hourBoxTo').css("background","#ABABB2");
	});
	$( "#minuteBoxTo" ).change(function() {
		currentMenu="#minuteBoxTo";
		$('#PlaceholderMinutesTo').remove();
		$("#buildResults").prop('disabled', false);
		tableNameToFill='imsiFailuresOverGivenTimePeriod';
		selectedMaxMinutesDropDown=$('#minuteBoxTo').val();
		datesInfoToSend=selectedMinDateCalendar+"&"+selectedMinHoursDropDown+"&"+selectedMinMinutesDropDown+"&"+selectedMaxDateCalendar+"&"+selectedMaxHoursDropDown+"&"+selectedMaxMinutesDropDown;
		getRidOfForwardSlash=datesInfoToSend.split("/");
		datesInfoToSend=getRidOfForwardSlash[0]+"&"+getRidOfForwardSlash[1]+"&"+getRidOfForwardSlash[2]+"&"+getRidOfForwardSlash[3]+"&"+getRidOfForwardSlash[4]
		if(currentQuerySelected=="story8"){
			$('#selectManufacturer').show();
			$("#buildResults").prop('disabled', true);
			$('#minuteBoxTo').css("background","#ABABB2");
			$('#minuteBoxTo').prop('disabled',true);
		}

	});
	$( "#reset" ).click(function() {
		$('#placeWhereQueriesAreDisplayed').empty();
		whatFunctionToResetSelector();
		$("#buildResults").prop('disabled', true);

	});
	$( "#buildResults" ).click(function() {
		functionSelector(datesInfoToSend);
	});
	$( "#undoStep" ).click(function() {
		$("#ImsiFailuresOverGivenPeriodList").hide();
		if(currentMenu == "#minuteBoxFrom"){
			$("#undoStep").prop('disabled', true);
			$('#minuteBoxFrom').css("background","#ABABB2");
			$('#hourBoxFrom').css("background","#428bca");
			$('#minuteBoxFrom').empty();
			$('#minuteBoxFrom').append("<option id=\"PlaceholderMinutesFrom\" value=\"Mins\">Mins</option>");
			$("#minuteBoxFrom").prop('disabled', true);
			$("#hourBoxFrom").prop('disabled', false);
			$('#hourBoxFrom').empty();
			$('#hourBoxFrom').append("<option id=\"PlaceholderHoursFrom\" value=\"Hrs\">Hrs</option>");
			populateHoursFromDown('hourBoxFrom');
		}
		else if(currentMenu == "#hourBoxTo"){
			$('#hourBoxTo').css("background","#ABABB2");
			$('#minuteBoxFrom').css("background","#428bca");
			$('#hourBoxTo').empty();
			$('#hourBoxTo').append("<option id=\"PlaceholderHoursTo\" value=\"Hrs\">Hrs</option>");
			$("#hourBoxTo").prop('disabled', true);
			$("#minuteBoxFrom").prop('disabled', false);
			$('#minuteBoxFrom').empty();
			$('#minuteBoxFrom').append("<option id=\"PlaceholderMinutesFrom\" value=\"Mins\">Mins</option>");
			populateMinutesFromDown('minuteBoxFrom');
			currentMenu="#minuteBoxFrom";
		}
		else if(currentMenu == "#minuteBoxTo"){
			$('#minuteBoxTo').css("background","#ABABB2");
			$('#hourBoxTo').css("background","#428bca");
			$('#minuteBoxTo').empty();
			$('#minuteBoxTo').append("<option id=\"PlaceholderMinutesTo\" value=\"Mins\">Mins</option>");
			$("#minuteBoxTo").prop('disabled', true);
			$("#hourBoxTo").prop('disabled', false);
			$('#hourBoxTo').empty();
			$('#hourBoxTo').append("<option id=\"PlaceholderMinutesFrom\" value=\"Hrs\">Hrs</option>");
			populateHoursToDown('hourBoxTo');
			currentMenu="#hourBoxTo";
			$("#buildResults").prop('disabled', true);
		}
	});

});
var currentMenu
var datesInfoToSend

var currentQuerySelected
var generalReset=function(){
	$('#hourBoxFrom').empty();
	$('#hourBoxFrom').append("<option id=\"PlaceholderHoursFrom\" value=\"Hrs\">Hrs</option>");
	$("#hourBoxFrom").prop('disabled', true);

	$('#minuteBoxFrom').empty();
	$('#minuteBoxFrom').append("<option id=\"PlaceholderMinutesFrom\" value=\"Mins\">Mins</option>");
	$("#minuteBoxFrom").prop('disabled', true);

	$('#hourBoxTo').empty();
	$('#hourBoxTo').append("<option id=\"PlaceholderMinutesTo\" value=\"Hrs\">Hrs</option>");
	$("#hourBoxTo").prop('disabled', true);

	$('#minuteBoxTo').empty();
	$('#minuteBoxTo').append("<option id=\"PlaceholderMinutes\" value=\"Mins\">Mins</option>");
	$("#minuteBoxTo").prop('disabled', true);

	$('#hourBoxFrom').css("background","#ABABB2");
	$('#minuteBoxFrom').css("background","#ABABB2");
	$('#minuteBoxTo').css("background","#ABABB2");
	$('#hourBoxTo').css("background","#ABABB2");

}
var whatFunctionToResetSelector=function (){
	if(currentQuerySelected=="story7"){
		makeStory7();
	}
	if(currentQuerySelected=="story8"){
		makeStory8();
	}
	if(currentQuerySelected=="story9"){
		makeStory9();
	}
	if(currentQuerySelected=="story12"){
		makeStory12();
	}
	if(currentQuerySelected=="failuresPerImsi"){
		makeFailurePerImsiFields();
		findAllIMSIs();
	}
	if(currentQuerySelected=="story11"){
		maketop10Failures();
	}
}
var functionSelector=function(data){
	if(currentQuerySelected=="story7"){
		renderList7(data);
		$('#imsiFailuresOverGivenTimePeriod').show();
		window.scrollTo(0, 0);
		$('#placeWhereQueriesAreDisplayed').show();
	}
	if(currentQuerySelected=="story8"){
		getCountOfErrors(data);
	}

	if(currentQuerySelected=="story9"){
		$('#placeWhereQueriesAreDisplayed').hide();	
		findImsiFailCountAndCalculateDurationOfDowntime(data);
		$('#imsiFailCountAndDurationCalculation').show();
	}
	if(currentQuerySelected=="failuresPerImsi"){
		failuresPerIMSI(data);
	}
	if(currentQuerySelected=="story11"){
		getTop10FailuresWithDate(data);
	}

	if(currentQuerySelected=="story12"){

		findTop10ImsiWithFailures(data);
		$('#top10ImsiWithFailuresOverTimePeriod').show();
	}
}

/**
 * ------------DATE TIME PICKER  BELOW---------------------------
 * 
 * There are a large amount of different date variables floating around
 * 			(1). Dates coming from the database with date and time (Max and Min)
 * 			(2). Dates and times from the database split (Max and Min).
 * 			(3). Dates selected by the user from the Calendar. (Start and End)
 * 			(4). Hours and minutes selected by the user from the drop down menus (Max and Min)
 */

var minDateFromDatabase
var maxDateFromDatabase
var minTimeFromDatabase
var maxTimeFromDatabase

var minHoursFromDatabase
var maxHoursFromDatabase

var minMinutesFromDatabase
var maxMinutesFromDatabase

var selectedMinDateCalendar='1';
var selectedMaxDateCalendar

var selectedMinHoursDropDown
var selectedMaxHoursDropDown

var selectedMinMinutesDropDown
var selectedMaxMinutesDropDown
/**
 * Set up the Calendar and use the selected dates to inform the populate drop downs method
 * which hrs/minutes are permitted. Parsing at the beginning is to split hours  from minutes
 * and match the incoming date format to that of the calendar to allow comparisons of selected dates.
 */
var t
var setUpCalander=(function() {
	console.log("FIVE-");
	minTimeDateSplit = minDateFromDatabase.split(" ");
	minDateFromDatabase=minTimeDateSplit[0];
	minMinutesHoursFromDatabase=minTimeDateSplit[1];

	minTimeSplit=minTimeFromDatabase.split(":");
	minHoursFromDatabase=minTimeSplit[0];
	minMinutesFromDatabase=minTimeSplit[1];

	maxTimeSplit=maxTimeFromDatabase.split(":");
	maxHoursFromDatabase=maxTimeSplit[0];
	maxMinutesFromDatabase=maxTimeSplit[1];

	if(maxHoursFromDatabase.charAt(0) =="0"){
		maxHoursFromDatabase=maxHoursFromDatabase.charAt(1);
	}
	if(maxMinutesFromDatabase.charAt(0) =="0"){
		maxMinutesFromDatabase=maxMinutesFromDatabase.charAt(1);
	}
	if(minHoursFromDatabase.charAt(0) =="0"){
		minHoursFromDatabase=minHoursFromDatabase.charAt(1);
	}
	if(minMinutesFromDatabase.charAt(0) =="0"){
		minMinutesFromDatabase=minMinutesFromDatabase.charAt(1);
	}

	from = minDateFromDatabase.split("-");

	minDateFromDatabase=from[1]+"/"+from[2]+"/"+from[0]
	f = new Date(from[0], from[1] - 1, from[2]);

	TO = maxDateFromDatabase.split("-");
	maxDateFromDatabase=TO[1]+"/"+TO[2]+"/"+TO[0]
	t = new Date(TO[0], TO[1] - 1, TO[2]);

	console.log("SIX-");
	console.log('MAX '+maxDateFromDatabase+" "+maxTimeFromDatabase);
	console.log('MIN '+minDateFromDatabase+" "+minTimeFromDatabase);
	$( "#from" ).datepicker({
		minDate: f,
		maxDate:t,
		defaultDate: f,
		changeMonth: true,
		numberOfMonths: 1,
		onClose: function( selectedDate ) {
			if(selectedDate != ''){
				generalReset();
				$("#to").prop('disabled', false);
				selectedMinDateCalendar=selectedDate;
				if($('#to').val() != ''){
					$("#hourBoxFrom").prop('disabled', false);
					populateHoursFromDown('hourBoxFrom');
					$('#hourBoxFrom').css("background","#428bca");
				}
			}
			$( "#to" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	$( "#to" ).datepicker({
		maxDate:t,
		defaultDate: f,
		changeMonth: true,
		numberOfMonths: 1,
		onClose: function( selectedDate ) {
			selectedMaxDateCalendar=selectedDate;
			generalReset();
			if($('#to').val() != ''){
				$("#hourBoxFrom").prop('disabled', false);
				populateHoursFromDown('hourBoxFrom');
				$('#hourBoxFrom').css("background","#428bca");
			}
		}
	});
});
var findMaxDates=function(){
	console.log("ONE-");
	$.ajax({
		type:'GET',
		dataType: "json",
		contentType:"application/json",
		url:"http://localhost:8080/Jarheads/rest/basedata/maxDateInDatabase",
		success: displayMaxDate
	});

};
var displayMaxDate=function(data){
	console.log("THREE-");
	maxDateFromDatabase=data.date;
	var timeSplitDate=maxDateFromDatabase.split(" ");
	maxDateFromDatabase=timeSplitDate[0];
	maxTimeFromDatabase=timeSplitDate[1];
}
var findMinDates=function(){
	console.log("TWO-");
	$.ajax({
		type:'GET',
		dataType: "json",
		contentType:"application/json",
		url:"http://localhost:8080/Jarheads/rest/basedata/minDateInDatabase",
		success: displayMinDate
	});

};
var displayMinDate=function(data){
	console.log("FOUR-");
	minDateFromDatabase=data.date;
	console.log("DISPLAY MIN DATE "+minDateFromDatabase);
	var timeSplitDate=minDateFromDatabase.split(" ");
	minDateFromDatabase=timeSplitDate[0];
	minTimeFromDatabase=timeSplitDate[1];
	setUpCalander();
	$("#to").prop('disabled', true);
	$("#ImsiFailuresOverGivenPeriodList").hide();
	$("#minuteBoxFrom").prop('disabled', true);
	$("#hourBoxFrom").prop('disabled', true);
	$("#hourBoxTo").prop('disabled', true);
	$("#minuteBoxTo").prop('disabled', true);
	$("#undoStep").prop('disabled', true);
	$("#buildResults").prop('disabled', true);
}
/**
 * Populating the drop downs depends on the dates that have been picked previously for example
 * a user picks a start date of the 17th of June. The min date in the database is 3pm 17th June. The
 * hours box needs to be populated accordingly.
 */
var populateHoursFromDown=function(hourDropName){
	if(selectedMinDateCalendar == minDateFromDatabase && selectedMaxDateCalendar != minDateFromDatabase){
		console.log("1");
		for(var i = minHoursFromDatabase; i < 24; i++){
			$('#'+hourDropName+'').append('<option value="'+i+'">'+i+'</option>');
		}
	}
	else if(selectedMinDateCalendar == minDateFromDatabase && selectedMaxDateCalendar == minDateFromDatabase && maxDateFromDatabase != minDateFromDatabase){
		console.log("1.1");
		for(var i = minHoursFromDatabase; i < 24; i++){
			$('#'+hourDropName+'').append('<option value="'+i+'">'+i+'</option>');
		}
	}
	else if(selectedMinDateCalendar == minDateFromDatabase && maxDateFromDatabase == minDateFromDatabase){
		console.log("2");
		for(var i = minHoursFromDatabase; i <= maxHoursFromDatabase; i++){
			$('#'+hourDropName+'').append('<option value="'+i+'">'+i+'</option>');
		}
	}
	else if(selectedMinDateCalendar == maxDateFromDatabase){
		console.log("3");
		for(var i = 0; i <= maxHoursFromDatabase; i++){
			$('#'+hourDropName+'').append('<option value="'+i+'">'+i+'</option>');
		}
	}
	else{
		console.log("4");
		for(var i = 0; i < 24; i++){	
			$('#'+hourDropName+'').append('<option value="'+i+'">'+i+'</option>');
		}
	}

}
var populateHoursToDown=function(hourDropName){
	if(selectedMinDateCalendar == maxDateFromDatabase && selectedMinHoursDropDown == maxHoursFromDatabase){
		for(var i = selectedMinHoursDropDown; i <= maxHoursFromDatabase; i++){
			$('#'+hourDropName+'').append('<option value="'+i+'">'+i+'</option>');
		}
	}
	else if(selectedMaxDateCalendar != maxDateFromDatabase && selectedMinDateCalendar != selectedMaxDateCalendar){
		for(var i = 0; i < 24; i++){
			$('#'+hourDropName+'').append('<option value="'+i+'">'+i+'</option>');
		}
	}
	else if(selectedMaxDateCalendar == selectedMinDateCalendar && selectedMinDateCalendar != maxDateFromDatabase && selectedMinDateCalendar != minDateFromDatabase ){
		for(var i = selectedMinHoursDropDown; i < 24; i++){
			$('#'+hourDropName+'').append('<option value="'+i+'">'+i+'</option>');
		}
	}
	else if(selectedMinDateCalendar == minDateFromDatabase && selectedMaxDateCalendar == selectedMinDateCalendar){
		for(var i = selectedMinHoursDropDown; i < 24; i++){
			$('#'+hourDropName+'').append('<option value="'+i+'">'+i+'</option>');
		}
	}
	else if(selectedMaxDateCalendar == maxDateFromDatabase){
		for(var i = 0; i <= maxHoursFromDatabase; i++){
			$('#'+hourDropName+'').append('<option value="'+i+'">'+i+'</option>');
		}
	}

}
var populateMinutesToDown=function(minuteDropName){
	if(selectedMaxDateCalendar == maxDateFromDatabase && selectedMinDateCalendar != maxDateFromDatabase){
		for(var i = 0; i <= maxMinutesFromDatabase; i++){
			$('#'+minuteDropName+'').append('<option value="'+i+'">'+i+'</option>');
		}
	}
	else if(selectedMaxDateCalendar == maxDateFromDatabase && selectedMaxHoursDropDown == maxHoursFromDatabase){
		for(var i = selectedMinMinutesDropDown; i <= maxMinutesFromDatabase; i++){
			$('#'+minuteDropName+'').append('<option value="'+i+'">'+i+'</option>');
		}
	}
	else if(selectedMinDateCalendar == selectedMaxDateCalendar && selectedMaxHoursDropDown == selectedMinHoursDropDown){
		for(var i = selectedMinMinutesDropDown; i < 60; i++){
			$('#'+minuteDropName+'').append('<option value="'+i+'">'+i+'</option>');
		}
	}
	else{
		for(var i = 0; i < 60; i++){
			$('#'+minuteDropName+'').append('<option value="'+i+'">'+i+'</option>');
		}
	}

}
var populateMinutesFromDown=function(minDropName){
	if(selectedMinDateCalendar == minDateFromDatabase && selectedMinHoursDropDown == minHoursFromDatabase && minDateFromDatabase == maxDateFromDatabase && minHoursFromDatabase == maxHoursFromDatabase){
		for(var i = minMinutesFromDatabase; i <= maxHoursFromDatabase; i++){
			$('#'+minDropName+'').append('<option value="'+i+'">'+i+'</option>');
		}
	}
	else if(selectedMinDateCalendar == maxDateFromDatabase && selectedMinHoursDropDown == maxHoursFromDatabase){
		for(var i = 0; i <= maxMinutesFromDatabase; i++){
			$('#'+minDropName+'').append('<option value="'+i+'">'+i+'</option>');
		}
	}
	else if(selectedMinDateCalendar == minDateFromDatabase && selectedMinHoursDropDown == minHoursFromDatabase){
		for(var i = minMinutesFromDatabase; i < 60; i++){
			$('#'+minDropName+'').append('<option value="'+i+'">'+i+'</option>');
		}
	}
	else if(selectedMinDateCalendar == maxDateFromDatabase && selectedMinHoursDropDown == maxHoursFromDatabase){
		for(var i = 0; i <= maxMinutesFromDatabase; i++){
			$('#'+minDropName+'').append('<option value="'+i+'">'+i+'</option>');
		}
	}
	else{
		for(var i = 0; i < 60; i++){
			$('#'+minDropName+'').append('<option value="'+i+'">'+i+'</option>');
		}
	}


}
/**
 * ---------------------------END OF DATE TIME PICKER CODE----------------------------------------
 */
