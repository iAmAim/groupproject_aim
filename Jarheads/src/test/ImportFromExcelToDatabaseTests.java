package test;


import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.ait.importFile.ImportFromExcelToDatabase;
import com.ait.models.BaseData;
public class ImportFromExcelToDatabaseTests {

	private ImportFromExcelToDatabase importClass;
	@SuppressWarnings("unused")
	private ImportFromExcelToDatabase mockImportClass;
	private BaseData data;
	private Row mockRow;
	private Cell mockCell0,mockCell1,mockCell2,mockCell3,mockCell4,mockCell5,mockCell6,mockCell7, mockCell8;
	private Cell mockCell9, mockCell10, mockCell11, mockCell12, mockCell13;
	
	@Before
	public void setUp(){
		importClass=new ImportFromExcelToDatabase();
		mockImportClass=Mockito.mock(ImportFromExcelToDatabase.class);
		mockRow=Mockito.mock(Row.class);
		data=new BaseData();
		mockCell0=Mockito.mock(Cell.class);
		mockCell1=Mockito.mock(Cell.class);
		mockCell2=Mockito.mock(Cell.class);
		mockCell3=Mockito.mock(Cell.class);
		mockCell4=Mockito.mock(Cell.class);
		mockCell5=Mockito.mock(Cell.class);
		mockCell6=Mockito.mock(Cell.class);
		mockCell7=Mockito.mock(Cell.class);
		mockCell8=Mockito.mock(Cell.class);
		mockCell9=Mockito.mock(Cell.class);
		mockCell10=Mockito.mock(Cell.class);
		mockCell11=Mockito.mock(Cell.class);
		mockCell12=Mockito.mock(Cell.class);
		mockCell13=Mockito.mock(Cell.class);
	}
	@Test
	public void testFinalInstances(){
	   assertEquals(ImportFromExcelToDatabase.BASE_DATA,0);
	   assertEquals(ImportFromExcelToDatabase.EVENT_CAUSE,1);
	   assertEquals(ImportFromExcelToDatabase.FAILURE_CLASS,2);
	   assertEquals(ImportFromExcelToDatabase.UE_TABLE,3);
	   assertEquals(ImportFromExcelToDatabase.MCC_MCN,4);
	   assertEquals(ImportFromExcelToDatabase.AMOUNT_OF_SHEETS,5);
	}
	@Test 
	public void convertFileTest1(){
		importClass.setErrorLogPath(System.getProperty("user.dir")+"\\TestFiles\\TestErrorLogs\\");
		importClass.setRunCsvToData(false);
		final String testList=importClass.convertFile(System.getProperty("user.dir")+"\\TestFiles\\TestCsvFiles\\", System.getProperty("user.dir")+"\\TestFiles\\ExcelTestFiles\\AIT Group Project - Sample Dataset.xls",true);
		assertTrue(testList.contains("200")); 
	}
	@Test 
	public void convertFileAutoImport(){
		importClass.setBase_dir(System.getProperty("user.dir"));
		importClass.setErrorLogPath(System.getProperty("user.dir")+"\\TestFiles\\TestErrorLogs\\");
		importClass.setRunCsvToData(false);
		final String testList=importClass.autoImport();
		System.out.println("TESTLIST "+testList);
		assertTrue(testList.contains("59995")); 
	}
	@Test(timeout=120000) 
	public void convertFileTimed1(){
		importClass.setErrorLogPath(System.getProperty("user.dir")+"\\TestFiles\\TestErrorLogs\\");
		importClass.setRunCsvToData(false);
		final String testList=importClass.convertFile(System.getProperty("user.dir")+"\\TestFiles\\TestCsvFiles\\", System.getProperty("user.dir")+"\\TestFiles\\ExcelTestFiles\\AITGroup Project - Dataset 3A.xls",true);
		assertTrue(testList.contains("30000")); 
	}
	@Test(timeout=120000)
	public void convertFileTimed2(){
		importClass.setErrorLogPath(System.getProperty("user.dir")+"\\TestFiles\\TestErrorLogs\\");
		importClass.setRunCsvToData(false);
		final String testList=importClass.convertFile(System.getProperty("user.dir")+"\\TestFiles\\TestCsvFiles\\", System.getProperty("user.dir")+"\\TestFiles\\ExcelTestFiles\\AIT Group Project - Dataset 3B.xls",true);
		assertTrue(testList.contains("29995")); 
	}
	@Test(timeout=120000)
	public void convertFileTimed3(){
		importClass.setErrorLogPath(System.getProperty("user.dir")+"\\TestFiles\\TestErrorLogs\\");
		importClass.setRunCsvToData(false);
		final String testList=importClass.convertFile(System.getProperty("user.dir")+"\\TestFiles\\TestCsvFiles\\", System.getProperty("user.dir")+"\\TestFiles\\ExcelTestFiles\\AIT Group Project - Dataset 3C.xls",true);
		assertTrue(testList.contains("26347")); 
	}
	@Test(timeout=120000)
	public void convertFileTimed4(){
		importClass.setErrorLogPath(System.getProperty("user.dir")+"\\TestFiles\\TestErrorLogs\\");
		importClass.setRunCsvToData(false);
		final String testList=importClass.convertFile(System.getProperty("user.dir")+"\\TestFiles\\TestCsvFiles\\", System.getProperty("user.dir")+"\\TestFiles\\ExcelTestFiles\\AIT Group Project - Dataset 3D.xls",true);
		assertTrue(testList.contains("25054")); 
	}
	@Test
	public void testErrorLogCreated(){
		importClass.setErrorLogPath(System.getProperty("user.dir")+"\\TestFiles\\TestErrorLogs\\");
		importClass.setRunCsvToData(false);
		final File pathOfFile = new File(System.getProperty("user.dir")+"\\TestFiles\\TestErrorLogs\\") ;
		try{
			FileUtils.cleanDirectory(pathOfFile); 
		}
		catch(Exception e){
			
		}
		final int fileCount=new File(System.getProperty("user.dir")+"\\TestFiles\\TestErrorLogs\\").listFiles().length;
		importClass.convertFile(System.getProperty("user.dir")+"\\TestFiles\\TestCsvFiles\\", System.getProperty("user.dir")+"\\TestFiles\\ExcelTestFiles\\testErrorLogCreation.xls",true);
		final int fileCount2=new File(System.getProperty("user.dir")+"\\TestFiles\\TestErrorLogs\\").listFiles().length;
		assertTrue(fileCount==fileCount2-1);
	}
	@Test
	public void convertFileTestReturnType(){
		importClass.setErrorLogPath(System.getProperty("user.dir")+"\\TestFiles\\TestErrorLogs\\");
		importClass.setRunCsvToData(false);
		final String testList=importClass.convertFile(System.getProperty("user.dir")+"\\TestFiles\\TestCsvFiles\\", System.getProperty("user.dir")+"\\TestFiles\\ExcelTestFiles\\AIT Group Project - Sample Dataset.xls",true);
		assertTrue(testList instanceof String); 
	}
	@Test
	public void formatDate(){
		data.setDate("24/01/1988 00:00");
		importClass.formatRecordDate(data);
		assertEquals("1988-01-24 00:00",data.getDate());
	}
	@Test
	public void formatDateException(){
		data.setDate("2dfdshhwt");
		importClass.formatRecordDate(data);
	}
	@Test
	public void testStoreRowDataInRecordObjectBaseData(){
		Mockito.when(mockRow.getCell(0)).thenReturn(mockCell0);
		Mockito.when(mockRow.getCell(1)).thenReturn(mockCell1);
		Mockito.when(mockRow.getCell(2)).thenReturn(mockCell2);
		Mockito.when(mockRow.getCell(3)).thenReturn(mockCell3);
		Mockito.when(mockRow.getCell(4)).thenReturn(mockCell4);
		Mockito.when(mockRow.getCell(5)).thenReturn(mockCell5);
		Mockito.when(mockRow.getCell(6)).thenReturn(mockCell6);
		Mockito.when(mockRow.getCell(7)).thenReturn(mockCell7);
		Mockito.when(mockRow.getCell(8)).thenReturn(mockCell8);
		Mockito.when(mockRow.getCell(9)).thenReturn(mockCell9);
		Mockito.when(mockRow.getCell(10)).thenReturn(mockCell10);
		Mockito.when(mockRow.getCell(11)).thenReturn(mockCell11);
		Mockito.when(mockRow.getCell(12)).thenReturn(mockCell12);
		Mockito.when(mockRow.getCell(13)).thenReturn(mockCell13);
		Mockito.when(String.valueOf(mockCell0)).thenReturn("0");
		Mockito.when(String.valueOf(mockCell1)).thenReturn("1");
		Mockito.when(String.valueOf(mockCell2)).thenReturn("2");
		Mockito.when(String.valueOf(mockCell3)).thenReturn("3");
		Mockito.when(String.valueOf(mockCell4)).thenReturn("4");
		Mockito.when(String.valueOf(mockCell5)).thenReturn("5");
		Mockito.when(String.valueOf(mockCell6)).thenReturn("6");
		Mockito.when(String.valueOf(mockCell7)).thenReturn("7");
		Mockito.when(String.valueOf(mockCell8)).thenReturn("8");
		Mockito.when(String.valueOf(mockCell9)).thenReturn("9");
		Mockito.when(String.valueOf(mockCell10)).thenReturn("10");
		Mockito.when(String.valueOf(mockCell11)).thenReturn("11");
		Mockito.when(String.valueOf(mockCell12)).thenReturn("12");
		Mockito.when(String.valueOf(mockCell13)).thenReturn("13");
		final BaseData base=new BaseData();
		importClass.storeRowDataInRecordObjectBaseData(base, mockRow);
		base.setDate("a date");
		assertEquals("a date;1;2;3;4;5;6;7;8;9;10;11;12;13;",base.getDate()+";"+
													base.getEventId()+";"+
													base.getFailureClass()+";"+
													base.getUeType()+";"+
													base.getMarket()+";"+
													base.getOperator()+";"+
													base.getCellId()+";"+
													base.getDuration()+";"+
													base.getCauseCode()+";"+
													base.getNeVersion()+";"+
													base.getImsi()+";"+
													base.getHier3id()+";"+
													base.getHier32id()+";"+
													base.getHier321id()+";"
													);
		
	}
	@Test
	public void testGetTableName() {
		assertEquals(importClass.getTableName(0),"base_data"); 
		assertEquals(importClass.getTableName(1),"event_cause_data");
		assertEquals(importClass.getTableName(2),"failure_class_table");
		assertEquals(importClass.getTableName(3),"ue_table");
		assertEquals(importClass.getTableName(4),"mcc_mcn_table");
		assertEquals(importClass.getTableName(5),null);
	}
	
	@Test
	public void testExcelFilePath(){
		importClass.setCsvFilePath("my test excel path");
		assertEquals(importClass.getCsvFilePath(),"my test excel path");
	}
	@Test
	public void testCsvFilePath(){
		importClass.setCsvFilePath("my test csv path");
		assertEquals(importClass.getCsvFilePath(),"my test csv path");
	}
	@Test
	public void testErrorLogPath(){
		importClass.setErrorLogPath("123");
		assertEquals(importClass.getErrorLogPath(),"123");
	}
	@Test
	public void testRunCsvToData(){
		importClass.setRunCsvToData(false);
		assertFalse(importClass.isRunCsvToData());
	}
	@Test
	public void testErrouneousData(){
		final ImportFromExcelToDatabase data=new ImportFromExcelToDatabase();
		final List<BaseData> testList=data.getErroneousDataList();
		assertEquals(testList.size(),0);
		testList.add(new BaseData());
		data.setErroneousDataList(testList);
		assertEquals(data.getErroneousDataList().size(),1);
	}

}
