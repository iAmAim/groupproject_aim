package test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;




import org.apache.poi.ss.usermodel.Sheet;
import org.junit.Before;
import org.junit.Test;

import com.ait.importFile.ExcelFileSheetSetup;

public class ExcelFileSheetSetupTest {
	
	private ExcelFileSheetSetup testSheet;
	private Sheet sheet;
	
	@Before
	public void setUp(){
		testSheet=new ExcelFileSheetSetup();
	}
	
	@Test
	public void testXLSSheet(){
		try {
			final FileInputStream file = new FileInputStream(new File(System.getProperty("user.dir")+"\\TestFiles\\ExcelTestFiles\\AIT Group Project - Sample Dataset.xls"));
			sheet=testSheet.excelFileSetup(0, file, "\\AIT Group Project - Sample Dataset.xls");
		} catch (Exception e) {
		}
		assertEquals(sheet.getClass().getName(),"org.apache.poi.hssf.usermodel.HSSFSheet");
	}

}
