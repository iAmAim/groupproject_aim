package test;

import static org.junit.Assert.*;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import com.ait.consistencycheck.ValidData;
import com.ait.consistencycheck.ValidDataHashSets;
import com.ait.importFile.ExctractHashTableData;

public class ExtractHashTableDataTests {

	private Row mockRow;
	private Cell mockCell1;
	private Cell mockCell2;
	private ExctractHashTableData validation;
	private ValidDataHashSets set;
	
	@Before
	public void setUp(){
		mockRow=Mockito.mock(Row.class);
		validation=new ExctractHashTableData();
		set=new ValidDataHashSets();
		mockCell1=Mockito.mock(Cell.class);
		mockCell2=Mockito.mock(Cell.class);
	}
	@Test
	public void testDefaultAction() {
		Mockito.when(mockRow.getCell(0)).thenReturn(mockCell1);
		Mockito.when(mockRow.getCell(1)).thenReturn(mockCell2);
		Mockito.when(String.valueOf(mockCell1)).thenReturn("1");
		Mockito.when(String.valueOf(mockCell2)).thenReturn("2");
		set=validation.fillHashTables(mockRow,6,set);
		assertEquals(ValidData.eventId_CauseCodeHashSet.size(),0);
		assertEquals(ValidData.failureClassHashSet.size(),0);
	}
	@Test
	public void testEventCauseCombo() {
		assertEquals(ValidData.eventId_CauseCodeHashSet.size(),0);
		Mockito.when(mockRow.getCell(0)).thenReturn(mockCell1);
		Mockito.when(mockRow.getCell(1)).thenReturn(mockCell2);
		Mockito.when(String.valueOf(mockCell1)).thenReturn("1");
		Mockito.when(String.valueOf(mockCell2)).thenReturn("2");
		set=validation.fillHashTables(mockRow,1,set);
		assertEquals(ValidData.eventId_CauseCodeHashSet.size(),1);		
	}
	@Test
	public void testFailureClass() {
		assertEquals(ValidData.failureClassHashSet.size(),0);
		Mockito.when(mockRow.getCell(0)).thenReturn(mockCell1);
		Mockito.when(String.valueOf(mockCell1)).thenReturn("4099");
		set=validation.fillHashTables(mockRow,2,set);
		assertEquals(ValidData.failureClassHashSet.size(),1);		
	}
	@Test
	public void testTAC() {
		assertEquals(ValidData.ueTypeHashSet.size(),0);
		Mockito.when(mockRow.getCell(0)).thenReturn(mockCell1);
		Mockito.when(String.valueOf(mockCell1)).thenReturn("1234");
		set=validation.fillHashTables(mockRow,3,set);
		assertEquals(ValidData.ueTypeHashSet.size(),1);		
	}
	@Test
	public void testCellTAC() {
		assertEquals(ValidData.mccMNCHashSet.size(),0);
		Mockito.when(mockRow.getCell(0)).thenReturn(mockCell1);
		Mockito.when(String.valueOf(mockCell1)).thenReturn("1");
		Mockito.when(mockRow.getCell(1)).thenReturn(mockCell2);
		Mockito.when(String.valueOf(mockCell2)).thenReturn("2");
		set=validation.fillHashTables(mockRow,4,set);
		assertEquals(ValidData.mccMNCHashSet.size(),1);	
	}

}