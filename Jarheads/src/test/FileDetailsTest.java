package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.ait.importFile.FileDetails;

public class FileDetailsTest {
	
	private FileDetails testFile;
	
	@Before
	public void setUp(){
		testFile=new FileDetails();
		testFile=new FileDetails("David");
	}
	
	@Test
	public void fileDetailsTest1(){
		assertEquals(testFile.getName(),"David");
	}
	@Test
	public void fileDetailsTest2(){
		testFile.setName("New Name");
		assertEquals(testFile.getName(),"New Name");
	}
	@Test
	public void fileDetailsTest3(){
		testFile.setFileId(1);
		assertEquals(testFile.getFileId(),1);
	}

}
