package test;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.never;
import static org.mockito.Matchers.any;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import com.ait.dataAccessObject.UserDAO;
import com.ait.models.User;
import com.ait.services.UserWS;

/**  IMPORTS FOR TESTING THE ENTITIES PASSED AS PARAMETERS, IF IMPLEMENTED == BETTER QUALITY TEST CASES*/
import javax.persistence.Entity;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

public class UserWSTest {
	private static final int RESPONSE_CODE_200 = 200;
	private static final int RESPONSE_CODE_201 = 201;
	private static final int RESPONSE_CODE_204 = 204;
	private static final int RESPONSE_CODE_404 = 404;
	private static final int RESPONSE_CODE_500 = 500;
	private static final int USER_ID = 1;
	private static final String USER_NAME= "denis";
	private static final String PASSWORD= "password";
	private static final String USER_NAME_AND_PASSWORD_KEY_VALUE_PAIR= "name=denis&password=password";
	private static final String INVALID_USER_NAME_AND_PASSWORD_KEY_VALUE_PAIR_BLANK_PASSWORD= "name=denis&password=";
	private static final String INVALID_USER_NAME_AND_PASSWORD_KEY_VALUE_PAIR_NO_PASSWORD= "name=denis";
	

	UserDAO mockUserDAO;
	UserWS stubUserWSDaoMethods;
	User stubUser;
	@Before
	public void setUp() {
		mockUserDAO = mock(UserDAO.class);
		stubUserWSDaoMethods = new UserWS();
		stubUserWSDaoMethods.setUserDao(mockUserDAO);
		stubUser = new User();
	}

	@Test
	public void testFindAll() {
		Response response = null;
		assertNull(response);
		response = stubUserWSDaoMethods.findAll();
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockUserDAO, times(1)).getAllUsers();

	}

	@Test
	public void testFindUserById() {
		Response response = null;
		assertNull(response);
		stubUser.setUserId(USER_ID);
		when(mockUserDAO.getUserById(any(Integer.class))).thenReturn(any(User.class));

		response = stubUserWSDaoMethods.findUserById(USER_ID);
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockUserDAO, times(1)).getUserById(USER_ID);
	}

	@Test
	public void testAddUser() {
		Response response = null;
		assertNull(response);
		// How to implement doNothing successfully, note the parenthesis
		// position.
		Mockito.doNothing()
		.when(mockUserDAO)
		.save(any(User.class));

		response = stubUserWSDaoMethods.addUser(stubUser);
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_201, response.getStatus());
		verify(mockUserDAO, times(1)).save(any(User.class));
	}

	@Test
	public void testFindUserByUserName() {
		Response response = null;
		assertNull(response);
		stubUser.setUserName(USER_NAME);
		when(mockUserDAO.getUserByName(any(String.class))).thenReturn(any(User.class));

		response = stubUserWSDaoMethods.findUserByUserName(USER_NAME);
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockUserDAO, times(1)).getUserByName(USER_NAME);
	}
	
	@Test
	public void testFindUserByUserNameAndPassword() {
		Response response = null;
		assertNull(response);
		stubUser.setUserName(USER_NAME);
		stubUser.setPassword(PASSWORD);
		when(mockUserDAO.findUserByUserNameAndPassword(USER_NAME,PASSWORD)).thenReturn(stubUser);
		
		response = stubUserWSDaoMethods.findUserByUserNameAndPassword(USER_NAME_AND_PASSWORD_KEY_VALUE_PAIR);
		
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockUserDAO, times(1)).findUserByUserNameAndPassword(USER_NAME, PASSWORD);
	}
	
	
	@Test
	public void testFindUserByUserNameAndPasswordFailOnNoPasswordGiven() {
		Response response = null;
		assertNull(response);
		stubUser.setUserName(USER_NAME);
		stubUser.setPassword(PASSWORD);
		when(mockUserDAO.findUserByUserNameAndPassword(USER_NAME,PASSWORD)).thenReturn(stubUser);

		response = stubUserWSDaoMethods.findUserByUserNameAndPassword(INVALID_USER_NAME_AND_PASSWORD_KEY_VALUE_PAIR_NO_PASSWORD);
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_404, response.getStatus());
		verify(mockUserDAO, never()).findUserByUserNameAndPassword(USER_NAME, PASSWORD);
	}
	
	
	@Test
	public void testFindUserByUserNameAndPasswordFailOnBlankPasswordGiven() {
		Response response = null;
		assertNull(response);
		stubUser.setUserName(USER_NAME);
		stubUser.setPassword(PASSWORD);
		when(mockUserDAO.findUserByUserNameAndPassword(USER_NAME,PASSWORD)).thenReturn(stubUser);

		response = stubUserWSDaoMethods.findUserByUserNameAndPassword(
				INVALID_USER_NAME_AND_PASSWORD_KEY_VALUE_PAIR_BLANK_PASSWORD);
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_500, response.getStatus());
		verify(mockUserDAO, never()).findUserByUserNameAndPassword(USER_NAME, PASSWORD);
	}
	
	@Test
	public void testUpdateUser() {
		Response response = null;
		assertNull(response);
		Mockito.doNothing()
		.when(mockUserDAO)
		.update(any(User.class));

		response = stubUserWSDaoMethods.updateUser(stubUser);
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockUserDAO, times(1)).update(any(User.class));
	}
	
	@Test
	public void testDeleteUser() {
		Response response = null;
		assertNull(response);
		stubUser.setUserId(1);
		Mockito.doNothing()
		.when(mockUserDAO)
		.delete(any(Integer.class));

		response = stubUserWSDaoMethods.deleteUser(stubUser.getUserId());
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_204, response.getStatus());
		verify(mockUserDAO, times(1)).delete(any(Integer.class));
	}

}
