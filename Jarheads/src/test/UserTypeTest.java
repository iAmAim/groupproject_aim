package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.ait.models.UserType;

public class UserTypeTest {

	private int userTypeId;
	private String name;
	
	private UserType userT;
	
	@Before
	public void setUp(){
		userT=new UserType();
	}
	@Test
	public void testId() {
		userT.setUserTypeId(1);
		assertEquals(userT.getUserTypeId(),1);
		assertFalse(2==(userT.getUserTypeId()));
	}
	@Test
	public void testName() {
		userT.login();
		userT.logout();
		userT.setName("David");
		assertEquals(userT.getName(),"David");
		assertFalse("Tristan".equals(userT.getName()));
	}

}
