package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.ait.consistencycheck.EventCauseCombo;

public class EventCauseComboTest {
	
	private EventCauseCombo ecc;

	@Before
	public void setUp() throws Exception {
		ecc = new EventCauseCombo(1,4097);
	}

	@Test
	public void testGetters() {
		assertEquals(1,ecc.getCauseCode());
		assertEquals(4097,ecc.getEventId());
	}

}
