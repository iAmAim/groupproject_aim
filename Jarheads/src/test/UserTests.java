package test;

import static org.junit.Assert.*;

import javax.persistence.Column;

import org.junit.Before;
import org.junit.Test;

import com.ait.models.User;
import com.ait.models.UserType;

public class UserTests {
	
	private User user;
	
	@Before
	public void setUp(){
		user = new User();
	}
	
	@Test
	public void testId() {
		user.setUserId(1);
		assertEquals(user.getUserId(),1);
		assertFalse(user.getUserId()==2);
	}
	
	@Test
	public void testUsername() {
		user.setUserName("Fred");
		assertEquals(user.getUserName(),"Fred");
		assertFalse(user.getUserName().equals("Barney"));
	}
	@Test
	public void testUserType() {
		UserType Dino = new UserType();
		UserType Pebbles = new UserType();
		user.setUserType(Dino);
		assertEquals(user.getUserType(),Dino);
		assertFalse(user.getUserType()==Pebbles );
	}
	
	
	@Test
	public void testPassword() {
		user.setPassword("Unbreakable");
		assertEquals(user.getPassword(),"Unbreakable");
		assertFalse(user.getPassword().equals("Breakable"));
	}

}
