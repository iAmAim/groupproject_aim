package test;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
		BaseDataTest.class, 
		BaseDataWSTest.class,
		BaseDataDAOTest.class,
		UserWSTest.class,
		ConsistencyCheckTest.class,
		BasicSystemSetupTests.class,
		EventCauseComboTest.class, 
		ExcelFileSheetSetupTest.class, 
		ExtractHashTableDataTests.class, 
		ImportFromExcelToDatabaseTests.class,
		ValidDataHashSetsTest.class,
		HasFileAlreadyBeenImportedTests.class,
		FileDetailsTest.class,
		UeDataTest.class,
		MCC_MNCComboTest.class})
public class AllTests {

}

