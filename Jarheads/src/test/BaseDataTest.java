package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.ait.dataAccessObject.BaseDataDAO;
import com.ait.models.BaseData;

public class BaseDataTest {

	private BaseData baseData;
	
	@Before
	public void setUp(){
		baseData=new BaseData();
	}
	@Test
	public void getMaxgetMin(){
		final String[] date={"1","2","3","4","5","6","7","8","9","10"};
		final BaseDataDAO dao=new BaseDataDAO();
		final String min=dao.getMinParsed(date);
		final String max=dao.getMaxParsed(date);
		assertEquals("3-1-2 4:5:00",min);
		assertEquals("8-6-7 9:10:00",max);
	}
	@Test
	public void testGettersAndSetters(){
		baseData.setDate("13/02/2015 23:05");
		assertEquals("13/02/2015 23:05",baseData.getDate());
		baseData.setEventId("3199");
		assertEquals("3199",baseData.getEventId());
		baseData.setFailureClass("5");
		assertEquals("5",baseData.getFailureClass());
		baseData.setUeType("1569216");
		assertEquals("1569216",baseData.getUeType());
		baseData.setMarket("165");
		assertEquals("165",baseData.getMarket());
		baseData.setOperator("145");
		assertEquals("145",baseData.getOperator());
		baseData.setCellId("8");
		assertEquals("8",baseData.getCellId());
		baseData.setDuration("1538");
		assertEquals("1538",baseData.getDuration());
		baseData.setCauseCode("5");
		assertEquals("5",baseData.getCauseCode());
		baseData.setNeVersion("2P");
		assertEquals("2P",baseData.getNeVersion());
		baseData.setImsi("16618486915165");
		assertEquals("16618486915165",baseData.getImsi());
		baseData.setHier3id("16816168513156165");
		assertEquals("16816168513156165",baseData.getHier3id());
		baseData.setHier32id("58926316631313");
		assertEquals("58926316631313",baseData.getHier32id());
		baseData.setHier321id("18946512655419871");
		assertEquals("18946512655419871",baseData.getHier321id());
	}

	@Test
	public void testToString(){
			baseData.setDate("13");
			baseData.setEventId("0");
			baseData.setFailureClass("1");
			baseData.setUeType("2");
			baseData.setMarket("3");
			baseData.setOperator("4");
			baseData.setCellId("5");
			baseData.setDuration("6");
			baseData.setCauseCode("7");
			baseData.setNeVersion("8");
			baseData.setImsi("9");
			baseData.setHier3id("10");
			baseData.setHier32id("11");
			baseData.setHier321id("12");
			final String representation=baseData.toString();
			assertEquals("13;0;1;2;3;4;5;6;7;8;9;10;11;12;\n",representation);
		 
	}


}