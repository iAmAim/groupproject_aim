package test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.ait.dataAccessObject.BaseDataDAO;
import com.ait.importFile.HasFileAlreadyBeenImported;

public class HasFileAlreadyBeenImportedTests {
	private HasFileAlreadyBeenImported checker;
	private Set<String> testSet=new HashSet<String>();;
	
	@Before 
	public void setUp(){
		checker=Mockito.mock(HasFileAlreadyBeenImported.class);
		testSet=new HashSet<String>();
		
	}
	@Test
	public void testFileCheck() throws IOException{
		final String address=System.getProperty("user.dir")+"\\TestFiles\\ExcelTestFiles\\AIT Group Project - Sample Dataset.xls";
		checker.setBoolean(false);
		testSet.add("AIT Group Project - Sample Dataset.xls2015-06-24T11:51:37.890571Z");
		checker.setKeysInDatabase(testSet); 
		Mockito.when(checker.hasTheFileBeenImportedBefore(address)).thenReturn(false);
		assertFalse(checker.hasTheFileBeenImportedBefore(address));	
	}
	
	@Test
	public void getMaxgetMin(){
		final String[] date={"1","2","3","4","5","6","7","8","9","10"};
		final BaseDataDAO dao=new BaseDataDAO();
		final String min=dao.getMinParsed(date);
		final String max=dao.getMaxParsed(date);
		assertEquals("3-1-2 4:5:00",min);
		assertEquals("8-6-7 9:10:00",max);
	}

}