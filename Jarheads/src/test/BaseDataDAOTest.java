package test;


import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import com.ait.dataAccessObject.BaseDataDAO;
import com.ait.importFile.HasFileAlreadyBeenImported;
import com.ait.importFile.ImportEntityManagerFunctions;
import com.ait.importFile.ImportFromExcelToDatabase;
import com.ait.services.BaseDataWS;

/**  IMPORTS FOR TESTING IMPORT FILE, THESE REQUIRED FOR MOCKING MULTIPARTINPUTFORM*/









import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.ait.models.BaseData;









/**  IMPORTS FOR TESTING THE ENTITIES PASSED AS PARAMETERS, IF IMPLEMENTED == BETTER QUALITY TEST CASES*/
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;

public class BaseDataDAOTest {

	
	final static String DATE_STRING= "02&23&2013&0&0&02&24&2013&0&0";
	final static String ERRORS_BY_HANDSET_DATE_STRING_SPECIAL_CASE= "02&23&2013&0&0&0&02&24&2013&0&0&0";
	final static String IMSI_NUMBER= "1111111111111111111";
	private final static String IMSI_AND_DATE_STRING = IMSI_NUMBER + "&" + DATE_STRING;
	final static String PHONE_MODEL= "GX-80";
	final static String EVENT= "0";
	final static String PHONEMODEL_AND_EVENT_KEY_VALUE_PAIR = "model=GX-80&event=0";
	private static final String[] TEST_STRING_ARRAY = {"02", "23", "2013", "0", "0", "02", "24", "2013", "0", "0"};
	private static final String MANUFACTURER = "manufacturer";
	
	BaseDataDAO stubBaseDataDAO;
	BaseDataDAO mockBaseDataDAO;
	ImportFromExcelToDatabase mockImportFromExcelToDatabase;
	HasFileAlreadyBeenImported mockHasFileAlreadyBeenImported;
	EntityManager mockEntityManager;
	List<BaseData> mockReturnedList;  
	BaseData mockBaseDataObject;
	Query mockQuery;
	
	
	
	
	@Before
	public void setUp() {
		mockEntityManager = mock(EntityManager.class);
		mockImportFromExcelToDatabase = mock(ImportFromExcelToDatabase.class);
		mockHasFileAlreadyBeenImported = mock(HasFileAlreadyBeenImported.class);
		mockQuery = mock(Query.class);
		mockBaseDataDAO = mock(BaseDataDAO.class);
		stubBaseDataDAO = new BaseDataDAO(mockImportFromExcelToDatabase, mockHasFileAlreadyBeenImported, mockEntityManager);
		mockBaseDataObject = mock(BaseData.class);
		
		mockReturnedList = mock(List.class);
		mockReturnedList.add(mockBaseDataObject);
		
		
	}
	
	@Test
	public void testGetAllData() {
		
		when(mockEntityManager.createQuery(any(String.class))).thenReturn(mockQuery);
		when(mockQuery.getResultList()).thenReturn(mockReturnedList);
		assertEquals(mockReturnedList, stubBaseDataDAO.getAllData());
	}
	
	@Test
	public void testGetMaxDate() {
		
		when(mockEntityManager.createQuery(any(String.class))).thenReturn(mockQuery);
		when(mockQuery.getSingleResult()).thenReturn(mockBaseDataObject);
		assertEquals(mockBaseDataObject, stubBaseDataDAO.getMaxDate());
	}
	
	@Test
	public void testGetMinDate() {
		
		when(mockEntityManager.createQuery(any(String.class))).thenReturn(mockQuery);
		when(mockQuery.getSingleResult()).thenReturn(mockBaseDataObject);
		assertEquals(mockBaseDataObject, stubBaseDataDAO.getMinDate());
	}
	
	@Test
	public void testSizeOfDatabase() {
		when(mockEntityManager.createQuery(any(String.class))).thenReturn(mockQuery);
		when(mockQuery.getSingleResult()).thenReturn(4L);
		
//		verify((mock(Long.class))).toString();
		assertEquals("4", stubBaseDataDAO.getDatabaseSize());
	}
	@Test
	public void testGetImsiFailuresForGivenTimeRange() {
		
		String failureClass = "1";
		Integer sEcho = 1;
		Integer iDisplayLength = 10;
		Integer iDisplayStart = 1;
		String searchTerm = "";
		String iSortCol_0 = "0";
		String sSearch = "";
		String sSortDir_0 = "0";
		String columnName = "imsi";
		when(mockEntityManager.createQuery(any(String.class))).thenReturn(mockQuery);
		when(stubBaseDataDAO.getImsiFailuresForGivenTimeRange(
				DATE_STRING, iDisplayStart,iDisplayLength, 
				columnName, sSearch, sSortDir_0))
				.thenReturn(mockReturnedList);

		
		assertEquals(mockReturnedList, stubBaseDataDAO.getImsiFailuresForGivenTimeRange(
				DATE_STRING, iDisplayStart,iDisplayLength, 
				columnName, sSearch, sSortDir_0));
	}
	
	@Test 
	public void testGetIMSI() {
		when(mockBaseDataDAO.getIMSI(any(String.class))).thenReturn(mockReturnedList);
		when(mockEntityManager.createQuery(any(String.class))).thenReturn(mockQuery);
		when(mockQuery.getResultList()).thenReturn(mockReturnedList);
		assertEquals(mockReturnedList, stubBaseDataDAO.getIMSI(any(String.class)));
	}
	
	@Test
	public void testGetDistinctIMSIList() {
		
		when(mockEntityManager.createQuery(any(String.class))).thenReturn(mockQuery);
		when(mockQuery.getResultList()).thenReturn(mockReturnedList);
		assertEquals(mockReturnedList, stubBaseDataDAO.getIMSIListOfCauseCodes());
	}
	
	@Test
	public void testGetFailuresPerDeviceModel() {
		
		when(mockEntityManager.createQuery(any(String.class))).thenReturn(mockQuery);
		when(mockQuery.getResultList()).thenReturn(mockReturnedList);
		assertEquals(mockReturnedList, stubBaseDataDAO.getFailuresPerDeviceModel(any(String.class)));
	}
	

	@Test
	public void testGetListOfManufacturersFromUeTableViaBaseData() {
		
		when(mockEntityManager.createQuery(any(String.class))).thenReturn(mockQuery);
		when(mockQuery.getResultList()).thenReturn(mockReturnedList);
		assertEquals(mockReturnedList, stubBaseDataDAO.getListOfManufacturersFromUeTableViaBaseData());
	}
	
	@Test
	public void testGetListOfHandsetsByManufacturers() {
		
		when(mockEntityManager.createQuery(any(String.class))).thenReturn(mockQuery);
		when(mockQuery.getResultList()).thenReturn(mockReturnedList);
		assertEquals(mockReturnedList, stubBaseDataDAO.getListOfHandsetsByManufacturers(any(String.class)));
	}
	
	@Test
	public void testGetListOfErrorsByHandsetAndDate() {
		
		when(mockEntityManager.createQuery(any(String.class))).thenReturn(mockQuery);
		when(mockQuery.getResultList()).thenReturn(mockReturnedList);
		assertEquals(mockReturnedList, stubBaseDataDAO.getListOfErrorsByHandsetAndDate(
				MANUFACTURER + "&" + ERRORS_BY_HANDSET_DATE_STRING_SPECIAL_CASE));
	}
	
	
	@Test
	public void testGetFailuresPerImsi() {
	
		when(mockEntityManager.createQuery(any(String.class))).thenReturn(mockQuery);
		when(mockQuery.getResultList()).thenReturn(mockReturnedList);
		assertEquals(mockReturnedList, stubBaseDataDAO.getFailuresPerImsi(
				IMSI_AND_DATE_STRING));
	}
	
	@Test
	public void testGetTop10FailCells() {
	
		when(mockEntityManager.createNativeQuery(any(String.class))).thenReturn(mockQuery);
		when(mockQuery.getResultList()).thenReturn(mockReturnedList);
		assertEquals(mockReturnedList, stubBaseDataDAO.getTop10FailCells());
	}
	
	@Test 
	public void testGetTop10ImsisWithFailuresTimePeriod() {
		when(mockBaseDataDAO.getTop10ImsisWithFailuresTimePeriod(any(String.class))).thenReturn(mockReturnedList);
		when(mockEntityManager.createQuery(any(String.class))).thenReturn(mockQuery);
		when(mockQuery.getResultList()).thenReturn(mockReturnedList);
		assertEquals(mockReturnedList, stubBaseDataDAO.getTop10ImsisWithFailuresTimePeriod(DATE_STRING));
	}
	
	@Test 
	public void testGetCountStory7() {
		TypedQuery<Long> mockTypedQuery = mock(TypedQuery.class);
		when(mockBaseDataDAO.getCountStory7(DATE_STRING)).thenReturn(any(Long.class));
		when(mockEntityManager.createQuery(DATE_STRING)).thenReturn(mockTypedQuery);
		when(mockTypedQuery.getSingleResult()).thenReturn(1L);
		assertEquals(1L, stubBaseDataDAO.getCountStory7(DATE_STRING));
	}
	

	@Test 
	public void testGetfailureCountPerImsiPerDateGroupByEvent() {
		when(mockBaseDataDAO.getfailureCountPerImsiPerDateGroupByEvent(any(String.class)))
		.thenReturn(mockReturnedList);
		when(mockEntityManager.createQuery(any(String.class))).thenReturn(mockQuery);
		when(mockQuery.getResultList()).thenReturn(mockReturnedList);
		assertEquals(mockReturnedList, stubBaseDataDAO.getfailureCountPerImsiPerDateGroupByEvent(DATE_STRING + "&" + IMSI_NUMBER));
	}


//	@Test 
//	public void testGetCount() {
//		String failureClass= "CALL DAIL";
//		TypedQuery<Long> mockTypedQuery = mock(TypedQuery.class);
//		when(mockBaseDataDAO.getCount(failureClass)).thenReturn(any(Long.class));
//		when(mockEntityManager.createQuery(failureClass)).thenReturn(mockTypedQuery);
//		when(mockTypedQuery.getSingleResult()).thenReturn(1L);
//		assertEquals(1L, stubBaseDataDAO.getCount(failureClass));
//	}
	
	@Test
	public void testGetIMSIsPerFailureClass() {
		
		String failureClass = "1";
		Integer sEcho = 1;
		Integer iDisplayLength = 10;
		Integer iDisplayStart = 1;
		String searchTerm = "";
		String iSortCol_0 = "0";
		String sSearch = "";
		String sSortDir_0 = "0";
		String columnName = "imsi";
		when(mockEntityManager.createQuery(any(String.class))).thenReturn(mockQuery);
		when(stubBaseDataDAO.getIMSIsPerFailureClass(
				iDisplayStart, iDisplayLength, columnName, 
				sSearch, sSortDir_0, failureClass))
				.thenReturn(mockReturnedList);
		
		
		assertEquals(mockReturnedList, stubBaseDataDAO.getIMSIsPerFailureClass(
				iDisplayStart, iDisplayLength, columnName, 
				sSearch, sSortDir_0, failureClass));
	}
	
	
//	@SuppressWarnings("unchecked")
//	public List<BaseData> getIMSIsPerFailureClass(final int start,final  int amount,final  String colName,
//			final String searchTerm,final String sortDir,final String failureClass){
//		
//		String query="select distinct b.imsi,b.failureClass1.description from BaseData as b where b.failureClass = "+failureClass
//				+ " ORDER BY b."+colName+" "+sortDir;
//		
//		if (!("").equals(searchTerm)){
//			final String search=" WHERE (b.imsi LIKE '%"+searchTerm+"%') and b.failureClass = "+ failureClass;
//			query="SELECT distinct b.imsi,"
//					+ "b.failureClass1.description FROM BaseData b "
//					+search+" ORDER BY b."+colName+" "+sortDir;
//		}
//		System.out.println("QUERY: "+query);
//		final Query queryToExecute=entityManager.createQuery(query);
//		queryToExecute.setFirstResult(start);
//		queryToExecute.setMaxResults(amount);
//		System.out.println("LIST SIZE----------->"+queryToExecute.getResultList().size());
//		return queryToExecute.getResultList();
//	}
//	

	
	
}