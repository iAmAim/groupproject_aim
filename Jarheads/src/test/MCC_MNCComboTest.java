package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.ait.consistencycheck.MCC_MNCCombo;

public class MCC_MNCComboTest {
	
	private MCC_MNCCombo mnc_mcc;

	@Before
	public void setUp() throws Exception {
		mnc_mcc = new MCC_MNCCombo(344,930);
	}

	@Test
	public void testGetterAndSetters() {
		assertEquals(344,mnc_mcc.getMarket());
		assertEquals(930,mnc_mcc.getOperator());	
	}

}
