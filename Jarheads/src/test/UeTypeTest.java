package test;

import static org.junit.Assert.*;

import javax.persistence.Column;

import org.junit.Before;
import org.junit.Test;

import com.ait.models.UEType;

public class UeTypeTest {

	private int tac;
	@Column(name = "marketing_name")
	private String marketingName;

	private UEType user;
	
	@Before
	public void setUp(){
		user=new UEType();
	}
	@Test
	public void testTac() {
		user.setTac(1);
		assertEquals(user.getTac(),1);
		assertFalse(2==(user.getTac()));
	}
	@Test
	public void testMarketing() {
		user.setMarketingName("1");
		assertEquals(user.getMarketingName(),"1");
		assertFalse("2".equals(user.getMarketingName()));
	}
	@Test
	public void testManu() {
		user.setManufacturer("1");
		assertEquals(user.getManufacturer(),"1");
		assertFalse("2".equals(user.getManufacturer()));
	}
	@Test
	public void testAccess() {
		user.setAccessCapability("1");
		assertEquals(user.getAccessCapability(),"1");
		assertFalse("2".equals(user.getAccessCapability()));
	}
	@Test
	public void testModel() {
		user.setModel("1");
		assertEquals(user.getModel(),"1");
		assertFalse("2".equals(user.getModel()));
	}
	@Test
	public void testVendor() {
		user.setVendorName("1");
		assertEquals(user.getVendorName(),"1");
		assertFalse("2".equals(user.getVendorName()));
	}
	@Test
	public void testType() {
		user.setType("1");
		assertEquals(user.getType(),"1");
		assertFalse("2".equals(user.getType()));
	}
	@Test
	public void testInputMode() {
		user.setInputMode("input");
		assertEquals(user.getInputMode(),"input");
		assertFalse("output".equals(user.getInputMode()));
	}
	@Test
	public void testOP() {
		user.setOperatingSystem("windows");
		assertEquals(user.getOperatingSystem(),"windows");
		assertFalse("mac".equals(user.getOperatingSystem()));
	}

}
