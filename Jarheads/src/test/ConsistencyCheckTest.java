package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ait.consistencycheck.ConsistencyCheck;
import com.ait.consistencycheck.ValidData;
import com.ait.consistencycheck.ValidDataHashSets;
import com.ait.models.BaseData;

public class ConsistencyCheckTest {
	
	private ConsistencyCheck consistencyCheck;
	private ValidDataHashSets hashSets;
	private BaseData record;

	@Before
	public void setUp() throws Exception {
		consistencyCheck = new ConsistencyCheck();
		hashSets = new ValidDataHashSets();
		record = new BaseData();
		record.setDate("13/02/2015 23:05");
		record.setEventId("4098");
		record.setFailureClass("1");
		record.setUeType("21060800");
		record.setMarket("344");
		record.setOperator("930");
		record.setCellId("4");
		record.setDuration("1000");
		record.setCauseCode("0");
		record.setNeVersion("11B");
		record.setImsi("344930000000011");
		record.setHier3id("4809532081614990000");
		record.setHier32id("8226896360947470000");
		record.setHier321id("1150444940909480000");
	}
	
	@Test
	public void testValidHashSetsGettersAndSetters(){
		consistencyCheck.setHashSets(hashSets);
		assertEquals(hashSets,consistencyCheck.getHashSets());
	}
	
	@Test
	public void testValidRecord(){
		hashSets.populateEventIdCauseCodeHashSet(0, 4098);
		hashSets.populateFailureClassHashSet(1);
		hashSets.populateMCC_MNCHashSet(344, 930);
		hashSets.populateUETypeHashSet(21060800);
		assertTrue(consistencyCheck.checkRecordIsValid(record));
	}
	
	@Test
	public void testInvalidRecord_Date(){
		record.setDate("13-02-2015 23:05");
		assertFalse(consistencyCheck.checkRecordIsValid(record));
	}
	
	@Test
	public void testInvalidRecord_EventId(){
		assertFalse(consistencyCheck.checkRecordIsValid(record));
	}
	
	@Test
	public void testInvalidRecord_FailureClass(){
		hashSets.populateEventIdCauseCodeHashSet(0, 4098);
		assertFalse(consistencyCheck.checkRecordIsValid(record));
	}
	
	@Test
	public void testInvalidRecord_UEType(){
		hashSets.populateEventIdCauseCodeHashSet(0, 4098);
		hashSets.populateFailureClassHashSet(1);
		assertFalse(consistencyCheck.checkRecordIsValid(record));
	}
	
	@Test
	public void testInvalidRecord_Market(){
		hashSets.populateEventIdCauseCodeHashSet(0, 4098);
		hashSets.populateFailureClassHashSet(1);
		hashSets.populateUETypeHashSet(21060800);
		assertFalse(consistencyCheck.checkRecordIsValid(record));
	}
	
	@Test
	public void testInvalidRecord_Operator(){
		hashSets.populateEventIdCauseCodeHashSet(0, 4098);
		hashSets.populateFailureClassHashSet(1);
		hashSets.populateUETypeHashSet(21060800);
		assertFalse(consistencyCheck.checkRecordIsValid(record));
	}
	
	@Test
	public void testInvalidRecord_CellId(){
		hashSets.populateEventIdCauseCodeHashSet(0, 4098);
		hashSets.populateFailureClassHashSet(1);
		hashSets.populateUETypeHashSet(21060800);
		hashSets.populateMCC_MNCHashSet(344, 930);
		record.setCellId("4A");
		assertFalse(consistencyCheck.checkRecordIsValid(record));
	}
	
	@Test
	public void testInvalidRecord_Duration(){
		hashSets.populateEventIdCauseCodeHashSet(0, 4098);
		hashSets.populateFailureClassHashSet(1);
		hashSets.populateUETypeHashSet(21060800);
		hashSets.populateMCC_MNCHashSet(344, 930);
		record.setDuration("1000s");
		assertFalse(consistencyCheck.checkRecordIsValid(record));
	}
	
	@Test
	public void testInvalidRecord_CauseCode(){
		hashSets.populateEventIdCauseCodeHashSet(1, 4098);
		hashSets.populateFailureClassHashSet(1);
		hashSets.populateUETypeHashSet(21060800);
		hashSets.populateMCC_MNCHashSet(344, 930);
		assertFalse(consistencyCheck.checkRecordIsValid(record));
	}
	
	@Test
	public void testInvalidRecord_NEVersion(){
		hashSets.populateEventIdCauseCodeHashSet(1, 4098);
		hashSets.populateFailureClassHashSet(1);
		hashSets.populateUETypeHashSet(21060800);
		hashSets.populateMCC_MNCHashSet(344, 930);
		record.setNeVersion("111");
		assertFalse(consistencyCheck.checkRecordIsValid(record));
	}
	
	@Test
	public void testInvalidRecord_IMSI(){
		hashSets.populateEventIdCauseCodeHashSet(0, 4098);
		hashSets.populateFailureClassHashSet(1);
		hashSets.populateUETypeHashSet(21060800);
		hashSets.populateMCC_MNCHashSet(344, 930);
		record.setImsi("344930000000011PO");
		assertFalse(consistencyCheck.checkRecordIsValid(record));
	}
	
	@Test
	public void testInvalidRecord_HIER3_ID(){
		hashSets.populateEventIdCauseCodeHashSet(0, 4098);
		hashSets.populateFailureClassHashSet(1);
		hashSets.populateUETypeHashSet(21060800);
		hashSets.populateMCC_MNCHashSet(344, 930);
		record.setHier3id("4809532081614990000li");
		assertFalse(consistencyCheck.checkRecordIsValid(record));
	}
	
	@Test
	public void testInvalidRecord_HIER32_ID(){
		hashSets.populateEventIdCauseCodeHashSet(0, 4098);
		hashSets.populateFailureClassHashSet(1);
		hashSets.populateUETypeHashSet(21060800);
		hashSets.populateMCC_MNCHashSet(344, 930);
		record.setHier32id("8226896360947470000[E");
		assertFalse(consistencyCheck.checkRecordIsValid(record));
	}
	
	@Test
	public void testInvalidRecord_HIER321_ID(){
		hashSets.populateEventIdCauseCodeHashSet(0, 4098);
		hashSets.populateFailureClassHashSet(1);
		hashSets.populateUETypeHashSet(21060800);
		hashSets.populateMCC_MNCHashSet(344, 930);
		record.setHier321id("1150444940909480000WEF");
		assertFalse(consistencyCheck.checkRecordIsValid(record));
	}

	@Test
	public void testValidDateFormat() {
		assertTrue(consistencyCheck.isDateFormatValid("13/02/2015 23:05"));
	}
	
	@Test
	public void testInValidDateFormat() {
		assertFalse(consistencyCheck.isDateFormatValid("13-02-2015 23:05:12"));
		assertFalse(consistencyCheck.isDateFormatValid("13-02-2015 23:05"));
	}
	
	@Test
	public void testValidEventIdAndCauseCodeCombinations(){
		hashSets.populateEventIdCauseCodeHashSet(0,4098);
		assertTrue(consistencyCheck.isEventIdAndCauseCodeCombinationValid("0","4098"));
	}
	
	@Test
	public void testInvalidEventIdAndCauseCodeCombinations(){
		hashSets.populateEventIdCauseCodeHashSet(0,4098);
		assertFalse(consistencyCheck.isEventIdAndCauseCodeCombinationValid("AB1","A4098"));
		assertFalse(consistencyCheck.isEventIdAndCauseCodeCombinationValid("25","44098"));
		assertFalse(consistencyCheck.isEventIdAndCauseCodeCombinationValid("1","4098"));
		assertFalse(consistencyCheck.isEventIdAndCauseCodeCombinationValid("0","4099"));
	}
	
	@Test
	public void testValidFailureClass(){
		hashSets.populateFailureClassHashSet(1);
		assertTrue(consistencyCheck.isFailureClassValid("1",record));
	}
	
	@Test
	public void testValidFailureClassNullValue(){
		assertTrue(consistencyCheck.isFailureClassValid("(null)",record));
		assertTrue(consistencyCheck.isFailureClassValid("null",record));
	}
	
	@Test
	public void testInvalidFailureClass(){
		hashSets.populateFailureClassHashSet(1);
		assertFalse(consistencyCheck.isFailureClassValid("2",record));
		assertFalse(consistencyCheck.isFailureClassValid("UI2",record));
	}
	
	@Test
	public void testValidMarketAndOperatorCombinations(){
		hashSets.populateMCC_MNCHashSet(344, 930);
		assertTrue(consistencyCheck.isMarketIdAndOperatorIdCombinationValid("344", "930"));
	}
	
	@Test
	public void testInValidMarketAndOperatorCombinations(){
		hashSets.populateMCC_MNCHashSet(344, 930);
		assertFalse(consistencyCheck.isMarketIdAndOperatorIdCombinationValid("344","931"));
		assertFalse(consistencyCheck.isMarketIdAndOperatorIdCombinationValid("AB980","930"));
	}
	
	@Test
	public void testValidUEType(){
		hashSets.populateUETypeHashSet(21060800);;
		assertTrue(consistencyCheck.isUETypeValid("21060800"));
	}
	
	@Test
	public void testInvalidUEType(){
		hashSets.populateUETypeHashSet(21060800);
		assertFalse(consistencyCheck.isUETypeValid("980"));
		assertFalse(consistencyCheck.isUETypeValid("U 78"));
	}
	
	@Test
	public void testValidDurationAndCellId() {
		assertTrue(consistencyCheck.isDurationAndCellIdValid("1000", "45"));
	}
	
	@Test
	public void testInValidDurationAndCellId() {
		assertFalse(consistencyCheck.isDurationAndCellIdValid("1 4353", "A45"));
	}
	
	@Test
	public void testValidStringToBigIntegerConversion() {
		assertTrue(consistencyCheck.isIMSIAndHIER_IDsValid("344930000000011",
				"4809532081614990000", "8226896360947470000",
				"1150444940909480000"));
	}
	
	@Test
	public void testInValidStringToBigIntegerConversion() {
		assertFalse(consistencyCheck.isIMSIAndHIER_IDsValid("A457 90",
				"A243", "NIT",
				"MIT"));
	}
	
	@Test
	public void testValidNEVersion(){
		assertTrue(consistencyCheck.isNEVersionValid("13C"));
	}
	
	@Test
	public void testInValidNEVersion(){
		assertFalse(consistencyCheck.isNEVersionValid("133"));
	}
	
	@After
	public void teardown(){
		ValidData.eventId_CauseCodeHashSet.clear();
		ValidData.failureClassHashSet.clear();
		ValidData.mccMNCHashSet.clear();
		ValidData.ueTypeHashSet.clear();
	}
	
}
