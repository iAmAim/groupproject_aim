package test;


import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import com.ait.dataAccessObject.BaseDataDAO;
import com.ait.dataAccessObject.UserDAO;
import com.ait.importFile.HasFileAlreadyBeenImported;
import com.ait.importFile.ImportEntityManagerFunctions;
import com.ait.importFile.ImportFromExcelToDatabase;
import com.ait.services.BaseDataWS;

/**  IMPORTS FOR TESTING IMPORT FILE, THESE REQUIRED FOR MOCKING MULTIPARTINPUTFORM*/









import com.ait.services.UserWS;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.ait.models.BaseData;









import com.ait.models.User;



/**  IMPORTS FOR TESTING THE ENTITIES PASSED AS PARAMETERS, IF IMPLEMENTED == BETTER QUALITY TEST CASES*/
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
 
public class UserDAOTest {

	
	private static final int USER_ID = 1;
	private static final String USER_NAME= "denis";
	private static final String PASSWORD= "password";
	private static final String USER_NAME_AND_PASSWORD_KEY_VALUE_PAIR= "name=denis&password=password";
	private static final String INVALID_USER_NAME_AND_PASSWORD_KEY_VALUE_PAIR_BLANK_PASSWORD= "name=denis&password=";
	private static final String INVALID_USER_NAME_AND_PASSWORD_KEY_VALUE_PAIR_NO_PASSWORD= "name=denis";
	

	
	UserDAO stubUserDAO;
	UserDAO mockUserDAO;
	User mockUser;
	EntityManager mockEntityManager;
	List<User> mockReturnedList;  
	Query mockQuery;
	
	
	
	
	@Before
	public void setUp() {
		mockEntityManager = mock(EntityManager.class);
		mockQuery = mock(Query.class);
		mockUserDAO = mock(UserDAO.class);
		stubUserDAO = new UserDAO();
		stubUserDAO.setEntityManager(mockEntityManager);
		
		mockReturnedList = mock(List.class);
		mockReturnedList.add(mockUser);
		
	}
	
	@Test
	public void testGetAllUsers() {
		
		when(mockEntityManager.createQuery(any(String.class))).thenReturn(mockQuery);
		when(mockQuery.getResultList()).thenReturn(mockReturnedList);
		assertEquals(mockReturnedList, stubUserDAO.getAllUsers());
	}
	
	@Test
	public void testGetUserById() {
		
		when(mockEntityManager.createQuery(any(String.class))).thenReturn(mockQuery);
		when(mockEntityManager.find(User.class, 1)).thenReturn(mockUser);
		assertEquals(mockUser, stubUserDAO.getUserById(1));
	}
	
	@Test
	public void testFindUserByUserNameAndPassword() {
		
		when(mockEntityManager.createQuery(any(String.class))).thenReturn(mockQuery);
		when(mockQuery.getSingleResult()).thenReturn(mockUser);
		assertEquals(mockUser, stubUserDAO.findUserByUserNameAndPassword(USER_NAME, PASSWORD));
	}
	
	@Test (expected=NoResultException.class)
	public void testFindUserByUserNameAndPasswordException() {
		when(mockEntityManager.createQuery(any(String.class))).thenReturn(mockQuery);
		when(mockQuery.getSingleResult()).thenThrow(new NoResultException());
		stubUserDAO.findUserByUserNameAndPassword(USER_NAME, PASSWORD);
	} 
	

	@Test
	public void testSave() {
		Mockito.doNothing().when(mockEntityManager).persist(mockUser);
		stubUserDAO.save(mockUser);
		verify(mockEntityManager, times(1)).persist(mockUser);
	}
	
//	@Test
//	public void testUpdate() {
//		Mockito.doNothing().when(mockEntityManager).merge(mockUser);
//		stubUserDAO.update(mockUser);
//		verify(mockEntityManager, times(1)).merge(mockUser);
//	}
	 
	@Test
	public void testDelete() {
		final int userId = 1;
		Mockito.doNothing().when(mockEntityManager).remove(mockUser);
		stubUserDAO.delete(userId);
		verify(mockEntityManager, times(1)).remove(mockUser);
	}
	
	@Test
	public void testGetUserByName() {
		
		when(mockEntityManager.createQuery(any(String.class))).thenReturn(mockQuery);
		when(mockQuery.getSingleResult()).thenReturn(mockUser);
		assertEquals(mockUser, stubUserDAO.getUserByName(USER_NAME));
	}
//	public User getUserByName(final String username) {
//		final Query query = entityManager.createQuery("select u from User u where u.userName = :theUsername");
//		query.setParameter("theUsername", username);
//		return (User) query.getSingleResult();
//	}
	
}