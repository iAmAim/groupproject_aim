package test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import com.ait.importFile.BasicSystemSetup;

public class BasicSystemSetupTests {

	private BasicSystemSetup fileBuilder;
	
	@Before
	public void setUp(){
		fileBuilder=new BasicSystemSetup();
		fileBuilder.setBaseAddress(System.getProperty("user.dir"));
		fileBuilder.runChecks();
	}
	@Test
	public void testAllFolders() throws IOException {
		fileBuilder=new BasicSystemSetup();
		fileBuilder.setBaseAddress(System.getProperty("user.dir"));
		fileBuilder.runChecks();
		final File doesFileExist = new File(System.getProperty("user.dir")+"\\Temp");
		assertTrue(doesFileExist.isDirectory());
	}
	@Test
	public void testCSVFolder() throws IOException {
		fileBuilder=new BasicSystemSetup();
		fileBuilder.setBaseAddress(System.getProperty("user.dir"));
		fileBuilder.runChecks();
		final File doesFileExist = new File(System.getProperty("user.dir")+"\\Temp\\CSV");
		assertTrue(doesFileExist.isDirectory());
		FileUtils.deleteDirectory(doesFileExist);
		assertFalse(doesFileExist.isDirectory());
		fileBuilder.runChecks();
		assertTrue(doesFileExist.isDirectory());
	}
	@Test
	public void testErrorLogsFolder() throws IOException {
		fileBuilder=new BasicSystemSetup();
		fileBuilder.setBaseAddress(System.getProperty("user.dir"));
		fileBuilder.runChecks();
		final File doesFileExist = new File(System.getProperty("user.dir")+"\\Temp\\ErrorLogs");
		assertTrue(doesFileExist.isDirectory());
		FileUtils.deleteDirectory(doesFileExist);
		assertFalse(doesFileExist.isDirectory());
		fileBuilder.runChecks();
		assertTrue(doesFileExist.isDirectory());
	}
	@Test
	public void testImportFolder() throws IOException {
		fileBuilder=new BasicSystemSetup();
		fileBuilder.setBaseAddress(System.getProperty("user.dir"));
		fileBuilder.runChecks();
		final File doesFileExist = new File(System.getProperty("user.dir")+"\\Temp\\Import");
		assertTrue(doesFileExist.isDirectory());
		FileUtils.deleteDirectory(doesFileExist);
		assertFalse(doesFileExist.isDirectory());
		fileBuilder.runChecks();
		assertTrue(doesFileExist.isDirectory());
	}
	@Test
	public void testTestAutoCSV() throws IOException {
		fileBuilder=new BasicSystemSetup();
		fileBuilder.setBaseAddress(System.getProperty("user.dir"));
		fileBuilder.runChecks();
		System.out.println(System.getProperty("user.dir"));
		final File doesFileExist = new File(System.getProperty("user.dir")+"\\Temp\\AutoImport\\Temp\\CSV");
		assertTrue(doesFileExist.isDirectory());
		FileUtils.deleteDirectory(doesFileExist);
		assertFalse(doesFileExist.isDirectory());
		fileBuilder.runChecks();
		assertTrue(doesFileExist.isDirectory());
	}
	@Test
	public void testTestAutoCopies() throws IOException {
		fileBuilder=new BasicSystemSetup();
		fileBuilder.setBaseAddress(System.getProperty("user.dir"));
		fileBuilder.runChecks();
		final File doesFileExist = new File(System.getProperty("user.dir")+"\\Temp\\AutoImport\\Temp\\Copies");
		assertTrue(doesFileExist.isDirectory());
		FileUtils.deleteDirectory(doesFileExist);
		assertFalse(doesFileExist.isDirectory());
		fileBuilder.runChecks();
		assertTrue(doesFileExist.isDirectory());
	}
	@Test
	public void testTestAutoTemp() throws IOException {
		fileBuilder=new BasicSystemSetup();
		fileBuilder.setBaseAddress(System.getProperty("user.dir"));
		fileBuilder.runChecks();
		final File doesFileExist = new File(System.getProperty("user.dir")+"\\Temp\\AutoImport\\Temp");
		assertTrue(doesFileExist.isDirectory());
		FileUtils.deleteDirectory(doesFileExist);
		assertFalse(doesFileExist.isDirectory());
		fileBuilder.runChecks();
		assertTrue(doesFileExist.isDirectory());
	}
	@Test
	public void testTestAutoErrorLog() throws IOException {
		fileBuilder=new BasicSystemSetup();
		fileBuilder.setBaseAddress(System.getProperty("user.dir"));
		fileBuilder.runChecks();
		final File doesFileExist = new File(System.getProperty("user.dir")+"\\Temp\\AutoImport\\Temp\\ErrorLogs");
		assertTrue(doesFileExist.isDirectory());
		FileUtils.deleteDirectory(doesFileExist);
		assertFalse(doesFileExist.isDirectory());
		fileBuilder.runChecks();
		assertTrue(doesFileExist.isDirectory());
	}

}
