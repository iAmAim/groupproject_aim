package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.ait.models.UeData;

public class UeDataTest {
	private UeData uedata;
	
	@Before 
	public void setUp(){
	uedata = new UeData();
	}
	
	@Test
	public void testGettersAndSetters() {
		uedata.setTac(123456);
		assertEquals(123456,uedata.getTac());
		uedata.setManufacturer("Menhausen");
		assertEquals("Menhausen", uedata.getManufacturer());
		uedata.setAccessCapabilities("GSM500");
		assertEquals("GSM500", uedata.getAccessCapabilities());
		uedata.setModel("12/96");
		assertEquals("12/96", uedata.getModel());
		uedata.setVendorName("Murphy");
		assertEquals("Murphy", uedata.getVendorName());
		uedata.setMarketingName("Voodoo Spider");
		assertEquals("Voodoo Spider", uedata.getMarketingName());
	}
	@Test 
	public void testToString(){
		uedata.setTac(123);
		uedata.setMarketingName("aname");
		uedata.setManufacturer("whomadethis");
		uedata.setAccessCapabilities("GSM");
		uedata.setModel("5000");
		uedata.setVendorName("Voda");
		final String resultsOfToString = uedata.toString();
		assertEquals("UeData [tac=123, marketingName=aname, manufacturer=whomadethis, accessCapabilities=GSM"
			+", model=5000, vendorName=Voda]",resultsOfToString);
	}
}


