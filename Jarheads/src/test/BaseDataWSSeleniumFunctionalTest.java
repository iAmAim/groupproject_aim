//package test;
//
//import static org.junit.Assert.*;
//
//import java.awt.AWTException;
//import java.awt.Robot;
//import java.awt.Toolkit;
//import java.awt.datatransfer.StringSelection;
//import java.io.File;
//import java.util.concurrent.TimeUnit;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.firefox.FirefoxDriver;
//
//import com.ait.services.BaseDataWS;
//
//
//public class BaseDataWSSeleniumFunctionalTest {
//
//	BaseDataWS fileUpload = new BaseDataWS();
//
//	private WebDriver driver;
// 
//	@Before
//	public void setUp() {
//		driver = new FirefoxDriver();
//		driver.get("http://localhost:8080/Jarheads/");
//	}
//
//	@Test
//	public void testFileUploadSuccessful2() throws InterruptedException, AWTException {
//
//		
//		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		final WebElement fileInput = driver.findElement(By
//				.xpath("//input[@type='file']"));
//		final WebElement importButton = driver.findElement(By.id("btnImport"));
//		final String filename = "AIT Group Project - Sample Dataset.xls";
//		final File file = new File(
//				"ExcelTestFiles\\AIT Group Project - Sample Dataset.xls");
//		
//		fileInput.click(); 
//		
//		Thread.sleep(5000); 
//		//the file specified in the above, AIT Group Project - Sample Dataset.xls needs to 
//		//be on desktop.
//		final StringSelection filePath = new StringSelection(filename); 
//		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(filePath, null);
//		
//		final Robot robot = new Robot();
//		robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
//		robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
//		robot.keyPress(java.awt.event.KeyEvent.VK_CONTROL);
//		robot.keyPress(java.awt.event.KeyEvent.VK_V);
//		robot.keyRelease(java.awt.event.KeyEvent.VK_V);
//		robot.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);
//		robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
//		robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
//
//		Thread.sleep(6000);
//		
//		importButton.click();
//		
//		Thread.sleep(20000);
//		assertTrue(file.exists());
//		
//		
//	}
//
//	@After
//	public void tearDown() throws Exception {
//		driver.quit(); 
//	}
//}
