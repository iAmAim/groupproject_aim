package test;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;

import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import com.ait.dataAccessObject.BaseDataDAO;
import com.ait.importFile.ImportEntityManagerFunctions;
import com.ait.pagination.PaginationClassDataTable;
import com.ait.services.BaseDataWS;


/**  IMPORTS FOR TESTING IMPORT FILE, THESE REQUIRED FOR MOCKING MULTIPARTINPUTFORM*/



import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.ait.models.BaseData;



/**  IMPORTS FOR TESTING THE ENTITIES PASSED AS PARAMETERS, IF IMPLEMENTED == BETTER QUALITY TEST CASES*/
import javax.persistence.Entity;

import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

public class BaseDataWSTest {
	final static String DATE_SENT_VIA_URL= "02&23&2013&0&0&02&24&2013&0&0";
	final static String IMSI_NUMBER= "1111111111111111111";
	final static String PHONE_MODEL= "GX-80";
	final static String EVENT= "0";
	final static String PHONEMODEL_AND_EVENT_KEY_VALUE_PAIR = "model=GX-80&event=0";
	private final int RESPONSE_CODE_200 = 200;
	private final int RESPONSE_CODE_201 = 201;

	BaseDataDAO mockBaseDataDAO;
	ImportEntityManagerFunctions mockEntityFunctions;
	PaginationClassDataTable stubIMSIPerFailureClassDataTable;
	
	BaseDataWS stubBaseDataWSDaoMethods;
	BaseDataWS stubBaseDataWSEntityFunctionsMethods;
	BaseDataWS mockBaseDataWS;
	
	List<BaseData> mockReturnedList;
	BaseData mockBaseDataObject;
	
	@Before
	public void setUp() {
		mockBaseDataWS = mock(BaseDataWS.class);
		mockBaseDataDAO = mock(BaseDataDAO.class);
		mockEntityFunctions = mock(ImportEntityManagerFunctions.class);
		
		stubBaseDataWSDaoMethods = new BaseDataWS(mockBaseDataDAO);
		stubBaseDataWSEntityFunctionsMethods = new BaseDataWS(mockEntityFunctions);
		stubIMSIPerFailureClassDataTable = new PaginationClassDataTable();
		stubBaseDataWSDaoMethods.setImsiPerFailureClassTbl(stubIMSIPerFailureClassDataTable);
		mockReturnedList = mock(List.class);
		mockReturnedList.add(mockBaseDataObject);
	}
	
	@Test
	public void testFindAll() {
		Response response = null;
		assertNull(response);
		response = stubBaseDataWSDaoMethods.findAll();
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		// Denis check bookmarks for how to properly verify.
		// ArgumentCaptor<BaseDataDAO> argument =
		// ArgumentCaptor.forClass(BaseDataDAO.class);
		verify(mockBaseDataDAO, times(1)).getAllData();
	}
	 
	@Test
	public void testCallAutoImport() {
		Response response = null;
		assertNull(response);
		response = stubBaseDataWSDaoMethods.callAutoImport();
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockBaseDataDAO, times(1)).callAutoImport();
	}
	
	@Test
	public void testClearDatabase() {
		Response response = null;
		assertNull(response);
		response = stubBaseDataWSEntityFunctionsMethods.clearDatabse();
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockEntityFunctions, times(1)).clearDatabase();
	}
	
	@Test
	public void testSizeOfDatabase() {
		Response response = null;
		assertNull(response);
		response = stubBaseDataWSDaoMethods.sizeOfDatabase();
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockBaseDataDAO, times(1)).getDatabaseSize();
	}
	
	
	//Denis, just mock all of it, no need to create external classes for inputPart's and Forms etc.
	//just mock those components.
//	@Test
//	public void testImportFile() { 
//		//-======
//		//Denis: I can mock this, but it will be time consuming so leaving until later/weekend
//		//-======
//		
////		MultipartFormDataInput m = mock(MultipartFormDataInput.class);
////	
////		Map<String, List<InputPart>> formValues = mock(Map.class);
////        when(m.getFormDataMap()).thenReturn(formValues);
////
////        Set<Map.Entry<String, List<BaseData>>> entries = new HashSet<Map.Entry<String, List<BaseData>>>();
////        entries.add(new EntryImpl<String, List<InputPart>>("a",
////                input(new InputPartImpl("a", MediaType.TEXT_PLAIN_TYPE))));
////        entries.add(new EntryImpl<String, List<InputPart>>("b",
////                input(new InputPartImpl("b", MediaType.TEXT_PLAIN_TYPE),
////                      new InputPartImpl("c", MediaType.TEXT_PLAIN_TYPE))));
////        entries.add(new EntryImpl<String, List<InputPart>>(ViewHashGenerator.VIEWHASH,
////                input(new InputPartImpl("view-hash", MediaType.TEXT_PLAIN_TYPE))));
////		
////		
////		Response response = null;
////		assertNull(response);
////		response = mockBaseDataWSDaoMethods.importFile(m);
////		assertNotNull(response);
////		assertEquals(RESPONSE_CODE_200, response.getStatus());
////		verify(mockBaseDataDAO, times(1)).getDatabaseSize();
//	}
	
	@Test
	public void testGetMaxDate() {
		Response response = null;
		assertNull(response);
		response = stubBaseDataWSDaoMethods.getMaxDate();
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockBaseDataDAO, times(1)).getMaxDate();
	}

	
	@Test
	public void testGetMinDate() {
		Response response = null;
		assertNull(response);
		response = stubBaseDataWSDaoMethods.getMinDate();
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockBaseDataDAO, times(1)).getMinDate();
	}
	

	@Test
	public void testgetImsiFailuresForGivenTimeRange() {
		
		Integer iDisplayLength = 10;
		Integer iDisplayStart = 1;
		String sSearch = "";
		String sSortDir_0 = "1";
		
		Response response = null;
		assertNull(response);
		when(mockBaseDataDAO.getImsiFailuresForGivenTimeRange(
				DATE_SENT_VIA_URL, iDisplayStart,iDisplayLength,
				"imsi",sSearch,sSortDir_0)).thenReturn(mockReturnedList);
		
		response = stubBaseDataWSDaoMethods.getImsiFailuresForGivenTimeRange
				(DATE_SENT_VIA_URL, iDisplayStart, iDisplayStart, iDisplayStart, sSortDir_0, sSortDir_0, sSortDir_0, sSortDir_0);
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockBaseDataDAO, times(1)).getImsiFailuresForGivenTimeRange
				(DATE_SENT_VIA_URL, iDisplayStart, iDisplayStart, "imsi", sSortDir_0, sSortDir_0);
	}
	
	
	@Test
	public void testGetImsiFailCountAndDuration() {
		Response response = null;
		assertNull(response);
		response = stubBaseDataWSDaoMethods.getImsiFailCountAndDuration(any(String.class));
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockBaseDataDAO, times(1)).getImsiFailCountAndDuration(any(String.class));
	}
	
	
	@Test
	public void testFindByIMSI() {
		Response response = null;
		assertNull(response);
		response = stubBaseDataWSDaoMethods.findByIMSI(any(String.class));
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockBaseDataDAO, times(1)).getIMSI(any(String.class));
	}
	
	@Test
	public void testFindDistinctIMSI() {
		Response response = null;
		assertNull(response);
		response = stubBaseDataWSDaoMethods.findDistinctIMSI();
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_201, response.getStatus());
		verify(mockBaseDataDAO, times(1)).getIMSIListOfCauseCodes();
	}
	
	@Test
	public void testGetImsiFailuresPerDeviceModel() {
		Response response = null;
		assertNull(response);
		response = stubBaseDataWSDaoMethods.getImsiFailuresPerDeviceModel(any(String.class));
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockBaseDataDAO, times(1)).getFailuresPerDeviceModel(any(String.class));
	}
	
	@Test
	public void testGetImsiFailuresPerDeviceModelAndEvent() {
		Response response = null;
		assertNull(response);
		
		when(mockBaseDataDAO.getUniqueFailuresPerDeviceModelAndEvent(
				PHONE_MODEL, PHONE_MODEL))
				.thenReturn(mockReturnedList);
		
		response = stubBaseDataWSDaoMethods
				.getImsiFailuresPerDeviceModelAndEvent(
				PHONEMODEL_AND_EVENT_KEY_VALUE_PAIR);
		
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockBaseDataDAO, times(1)).getUniqueFailuresPerDeviceModelAndEvent(PHONE_MODEL, EVENT);
	}
	
	
	
//	public Response getImsiFailuresPerDeviceModelAndEvent(@PathParam("modelAndImsi") final String modelAndEvent) {
//		final String[] parseAllInfo = modelAndEvent.split("&");
//		final String[] model = parseAllInfo[0].split("=");
//		final String[] event = parseAllInfo[1].split("=");
//
////		System.out.println("model: " + model[1]);
////		System.out.println("event: " + event[1]);
//		
//		final List<BaseData> failuresPerDeviceList = baseDataDao.getUniqueFailuresPerDeviceModelAndEvent(model[1], event[1]);
//		return Response.status(200).entity(failuresPerDeviceList).build();

	
	@Test
	public void testGetTop10FailCells() {
		Response response = null;
		assertNull(response);
		response = stubBaseDataWSDaoMethods.getTop10FailCells();
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockBaseDataDAO, times(1)).getTop10FailCells();
	}
	
	@Test
	public void testGetFailureCountPerImsi() {
		Response response = null;
		assertNull(response);
		response = stubBaseDataWSDaoMethods.getFailureCountPerImsi(any(String.class));
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockBaseDataDAO, times(1)).getFailuresPerImsi(any(String.class));
	}
	
	@Test
	public void testGetListOfManufacturersInBaseData() {
		Response response = null;
		assertNull(response);
		response = stubBaseDataWSDaoMethods.getListOfManufacturersInBaseData();
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockBaseDataDAO, times(1)).getListOfManufacturersFromUeTableViaBaseData();
	}
	
	
	@Test
	public void testGetListOfErrorsByHandsetAndDate() {
		Response response = null;
		assertNull(response);
		response = stubBaseDataWSDaoMethods.getListOfErrorsByHandsetAndDate(any(String.class));
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockBaseDataDAO, times(1)).getListOfErrorsByHandsetAndDate(any(String.class));
	}
	
	@Test
	public void testGetTop10FailuresWithDate() {
		Response response = null;
		assertNull(response);
		response = stubBaseDataWSDaoMethods.getTop10FailuresWithDate(any(String.class));
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_201, response.getStatus());
		verify(mockBaseDataDAO, times(1)).getTop10FailuresWithDate(any(String.class));
	}
	

	@Test
	public void testGetListOfHandsetsByManufacturer() {
		Response response = null;
		assertNull(response);
		response = stubBaseDataWSDaoMethods.getListOfHandsetsByManufacturer(any(String.class));
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockBaseDataDAO, times(1)).getListOfHandsetsByManufacturers(any(String.class));
	}
	
	@Test
	public void testGetAllFailureClasses() {
		Response response = null;
		assertNull(response);
		response = stubBaseDataWSDaoMethods.getAllFailureClasses();
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_201, response.getStatus());
		verify(mockBaseDataDAO, times(1)).getAllFailureClasses();
	}
	
	@Test
	public void testGetTop10ImsisWithFailuresTimePeriod() {
		Response response = null;
		assertNull(response);
		response = stubBaseDataWSDaoMethods.getTop10ImsisWithFailuresTimePeriod(any(String.class));
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockBaseDataDAO, times(1)).getTop10ImsisWithFailuresTimePeriod(any(String.class));
	}
	
	
	@Test
	public void testGetIMSIsPerFailureClass() {
		
		String failureClass = "1";
		Integer sEcho = 1;
		Integer iDisplayLength = 10;
		Integer iDisplayStart = 1;
		String search = "";
		String iSortCol_0 = "0";
		String sSearch = "";
		String sSortDir_0 = "0";
		
		Response response = null;
		assertNull(response);
		when(mockBaseDataDAO.getIMSIsPerFailureClass(
				iDisplayStart,iDisplayLength,
				"failureClass",sSearch,sSortDir_0,failureClass))
				.thenReturn(mockReturnedList);
		
		response = stubBaseDataWSDaoMethods.getIMSIsPerFailureClass(failureClass, sEcho, iDisplayLength, iDisplayStart, search, iSortCol_0, sSearch, sSortDir_0 );
		assertNotNull(response);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockBaseDataDAO, times(1)).getIMSIsPerFailureClass(any(Integer.class), any(Integer.class), any(String.class), any(String.class), any(String.class), any(String.class));
	}
	

	@Test
	public void testFailureCountPerImsiPerDateGroupByEvent() {
		Response response = null;
		assertNull(response);
		
		
		when(mockBaseDataDAO.getfailureCountPerImsiPerDateGroupByEvent(
				DATE_SENT_VIA_URL + IMSI_NUMBER))
				.thenReturn(mockReturnedList);
		
		response = stubBaseDataWSDaoMethods.getFailureCountPerImsiPerDateGroupByEvent(
				DATE_SENT_VIA_URL + IMSI_NUMBER);
		assertEquals(RESPONSE_CODE_200, response.getStatus());
		verify(mockBaseDataDAO, times(1)).getfailureCountPerImsiPerDateGroupByEvent(any(String.class));
	}
	
	@Test
	public void testOpenErrorLogs() {
		
		stubBaseDataWSDaoMethods.openErrorLogs();
		mockBaseDataWS.openErrorLogs();
		verify(mockBaseDataWS, times(1)).openErrorLogs();
		
	}
	
	
}