package test;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ait.consistencycheck.EventCauseCombo;
import com.ait.consistencycheck.MCC_MNCCombo;
import com.ait.consistencycheck.ValidData;
import com.ait.consistencycheck.ValidDataHashSets;

public class ValidDataHashSetsTest {
	
	private ValidDataHashSets validDataHS;

	@Before
	public void setUp(){
		validDataHS = new ValidDataHashSets();
	}

	@Test
	public void testAddingToEventIdCauseCodeHashSet() {
		final EventCauseCombo eventCauseCombo  = new EventCauseCombo(0,4098);
		validDataHS.populateEventIdCauseCodeHashSet(0, 4098);
		assertTrue(validDataHS.isCauseCodeEventIdComboInValidHashSet(eventCauseCombo));
	}
	
	@Test
	public void testAddingToFailureClassHashSet() {
		validDataHS.populateFailureClassHashSet(2);
		assertTrue(validDataHS.isFailureClassInValidHashSet(2));
	}
	
	@Test
	public void testAddingToMCC_MNCComboHashSet() {
		final MCC_MNCCombo mccMNCCombo = new MCC_MNCCombo(344,930);
		validDataHS.populateMCC_MNCHashSet(344, 930);
		assertTrue(validDataHS.isMarketIdAndOperatorIdComboInValidHashSet(mccMNCCombo));
	}
	
	@Test
	public void testAddingToUETypeHashSet() {
		validDataHS.populateUETypeHashSet(26541465);
		assertTrue(validDataHS.isUETypeInValidHashSet(26541465));
	}
	
	@After
	public void tearDown(){
		ValidData.eventId_CauseCodeHashSet.clear();
		ValidData.failureClassHashSet.clear();
		ValidData.mccMNCHashSet.clear();
		ValidData.ueTypeHashSet.clear();
	}

}
