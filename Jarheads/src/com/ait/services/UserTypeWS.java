package com.ait.services;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ait.dataAccessObject.UserTypeDAO;
import com.ait.models.User;

@Path("/usertypes")
@Stateless
@LocalBean
public class UserTypeWS {

	@EJB
	private UserTypeDAO userTypeDao;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response findAllUserType() {
		System.out.println("Getting all registered users");
		final List<User> userTypes = userTypeDao.getAllUserTypes();
		return Response.status(200).entity(userTypes).build();
	}
} 

