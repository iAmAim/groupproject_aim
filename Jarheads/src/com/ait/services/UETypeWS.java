package com.ait.services;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ait.dataAccessObject.UETypeDAO;
import com.ait.models.UEType;

@Path("/uetypes")
@Stateless
@LocalBean
public class UETypeWS {

	@EJB
	private UETypeDAO ueTypeDao;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response findAllUETypes() {
		System.out.println("Getting all ue types");
		final List<UEType> uETypes = ueTypeDao.getAllUETypes();
		return Response.status(200).entity(uETypes).build();
	}
} 

