package com.ait.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.ait.dataAccessObject.BaseDataDAO;
import com.ait.importFile.BasicSystemSetup;
import com.ait.importFile.ImportEntityManagerFunctions;
import com.ait.models.BaseData;
import com.ait.pagination.PaginationClassDataTable;

@Path("/basedata")
@Stateless
@LocalBean
public class BaseDataWS {
	
	
	
	public BaseDataWS(){
	}
	//constructor for mockito unit tests. Justified comment.
	public BaseDataWS(final BaseDataDAO bdDAO){
		this.baseDataDao = bdDAO;
	}
	
	public BaseDataWS(final ImportEntityManagerFunctions entityFunctions){
		this.entityFunctions = entityFunctions;
	}
	 
	
	static {
		final BasicSystemSetup setupFoldersIfNotExisting = new BasicSystemSetup();
		setupFoldersIfNotExisting.runChecks();
		setupFoldersIfNotExisting.setBaseAddress(System.getProperty("user.dir").substring(0,
				System.getProperty("user.dir").length() - 4));
		setupFoldersIfNotExisting.runChecks();
	}
	private static final String BASE_DIRECTORY = System.getProperty("user.dir").substring(0,
			System.getProperty("user.dir").length() - 4);
	private static final String SERVER_UPLOAD_LOCATION_FOLDER = BASE_DIRECTORY + "\\Temp\\Import\\";
	@EJB
	private BaseDataDAO baseDataDao;
	
	@EJB
	ImportEntityManagerFunctions entityFunctions;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response findAll() {
		System.out.println("Getting all call failure data..");
		final List<BaseData> data = baseDataDao.getAllData();
		return Response.status(200).entity(data).build();
	}
	@GET
	@Path("/callAutoImport")
	@Produces({ MediaType.TEXT_PLAIN})
	@PermitAll
	public Response callAutoImport() {
		final String messageFromAuto = baseDataDao.callAutoImport();
		System.out.println("WS MESSAGE "+messageFromAuto);
		return Response.status(200).entity(messageFromAuto).build();
	}
	@GET
	@Path("/clearDatabase")
	@Produces({ MediaType.TEXT_PLAIN})
	@PermitAll
	public Response clearDatabse() {
		System.out.println("InWS");
		entityFunctions.clearDatabase();
		return Response.status(200).entity("Database has been cleared !").build();
	}
	@GET
	@Path("/sizeOfDatabase")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response sizeOfDatabase() {
		final String size = baseDataDao.getDatabaseSize();
		return Response.status(200).entity(size).build();
	}

	/*
	 * TODO: Put code inside importFile method that deals with uploading file to
	 * server
	 */
	@POST
	@Consumes("multipart/form-data")
	@Produces({ MediaType.TEXT_PLAIN })
	@Path("/import")
	@PermitAll
	public Response importFile(final MultipartFormDataInput input) {

		System.out.println("WS: Server file upload location is :" + SERVER_UPLOAD_LOCATION_FOLDER);
		String fileName = "";
		final Map<String, List<InputPart>> formParts = input.getFormDataMap();
		final List<InputPart> inPart = formParts.get("file");
		String importReturnMessage = null;
		System.out.println("inside importfile");
 
		for (final InputPart inputPart : inPart) {
			System.out.println("inside importfile: for loop");
			try {
				// Retrieve headers, read the Content-Disposition header to
				// obtain the original name of the file
				final MultivaluedMap<String, String> headers = inputPart.getHeaders();
				fileName = parseFileName(headers);

				// Check if file is an excel file
				if (fileName.endsWith(".xlsx") || fileName.endsWith(".xls")) {
					System.out.println("inside importfile: an excel file");
					// Handle the body of that part with an InputStream
					final InputStream istream = inputPart.getBody(InputStream.class, null);
					fileName = SERVER_UPLOAD_LOCATION_FOLDER + fileName;
					saveFile(istream, fileName);
					System.out.println();
					// final String output =
					// "Your file has been saved successfully";
					System.out.println("success response..");
					importReturnMessage = baseDataDao.saveImportedData(BASE_DIRECTORY, fileName, true);
					System.out.println("WS: File is :" + fileName);

				}
			}
			catch (IOException e) {
				e.printStackTrace();
			}

		}

		return Response.status(200).entity(importReturnMessage).build();

	}

	// Parse Content-Disposition header to get the original file name
	private String parseFileName(final MultivaluedMap<String, String> headers) {
		System.out.println("Parsing filenmae");
		final String[] contentDispositionHeader = headers.getFirst("Content-Disposition").split(";");
		for (final String name : contentDispositionHeader) {
			if ((name.trim().startsWith("filename"))) {
				final String[] tmp = name.split("=");
				final String fileName = tmp[1].trim().replaceAll("\"", "");
				return fileName;
			}
		}
		return "randomName";
	}

	// save uploaded file to a defined location on the server
	private void saveFile(final InputStream uploadedInputStream, final String serverLocation) {
		System.out.println("save file");
		try {
			OutputStream outputStream = new FileOutputStream(new File(serverLocation));
			int read = 0;
			final byte[] bytes = new byte[1024];
			outputStream = new FileOutputStream(new File(serverLocation));
			while ((read = uploadedInputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
			outputStream.flush();
			outputStream.close();

		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * -------------Date Functions--------------------
	 */
	@SuppressWarnings("unused")
	@GET
	@Path("/openErrorLogs")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public void openErrorLogs() {
		try {
			final String userDir = (System.getProperty("user.dir"));
			final String relative = userDir.substring(0, userDir.length() - 4);
			relative.replace("\\", "\\\\");
			final Process openErrorLogs = new ProcessBuilder("explorer.exe", "/root," + relative + "\\Temp\\ErrorLogs")
			.start();
		}
		catch (IOException e) {
			e.printStackTrace();
		}

	}

	@GET
	@Path("/maxDateInDatabase")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response getMaxDate() {
		final BaseData maxDate = baseDataDao.getMaxDate();
		return Response.status(200).entity(maxDate).build();

	}

	@GET
	@Path("/minDateInDatabase")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response getMinDate() {
		final BaseData minDate = baseDataDao.getMinDate();
		return Response.status(200).entity(minDate).build();

	}

	
	String getImsiFailuresForGivenTimeRange [] = {"date","imsi","failureClass","ueType","duration","causeCode"};
	/**
	 * Story 7
	 * @param dateInfo a date range, data seperated by &s
	 * @return
	 */
	@GET
	@Path("/ImsiFailuresForGivenTimeRange/{dateInfo}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response getImsiFailuresForGivenTimeRange(@PathParam("dateInfo")final String dateInfo ,
			@QueryParam("sEcho")final  Integer sEcho,
			@QueryParam("iDisplayLength")final  Integer iDisplayLength,
			@QueryParam("iDisplayStart")final  Integer iDisplayStart,
			@QueryParam("sSearch")final  String search,
			@QueryParam("iSortCol_0")final  String iSortCol_0,
			@QueryParam("sSearch")final  String sSearch,
			@QueryParam("sSortDir_0")final  String sSortDir_0) {

		final int col = Integer.parseInt(iSortCol_0);
		final String colName = getImsiFailuresForGivenTimeRange[col];
		System.out.println("WS COLUMN NAME "+colName);
		final List<BaseData> failures = baseDataDao.getImsiFailuresForGivenTimeRange(
				dateInfo,iDisplayStart,iDisplayLength,
			colName,sSearch,sSortDir_0);
		
		imsiPerFailureClassTbl.setAaData(failures);
		final long totalRecords = baseDataDao.getCountStory7(dateInfo);
		imsiPerFailureClassTbl.setiTotalRecords(String.valueOf(totalRecords));
		imsiPerFailureClassTbl.setiTotalDisplayRecords(String.valueOf(totalRecords));
		imsiPerFailureClassTbl.setsEcho(sEcho);
		return Response.status(200).entity(imsiPerFailureClassTbl).build();

	}

	/**
	 * @return Response
	 * @param Date
	 *            information from the date picker
	 * */
	@GET
	@Path("/ImsiFailureCountAndDuration/{dateInfo}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response getImsiFailCountAndDuration(@PathParam("dateInfo") final String dateInfo) {
		final List<BaseData> imsiFailCountAndDuration = baseDataDao.getImsiFailCountAndDuration(dateInfo);
		return Response.status(200).entity(imsiFailCountAndDuration).build();

	}

	/**
	 * -------------Story 4 functions --------------------
	 */
	@GET
	@Path("/findImsi/{query}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response findByIMSI(@PathParam("query") final String query) {
		System.out.println("findByIMSI" + query);
		final List<BaseData> data = baseDataDao.getIMSI(query);
		return Response.status(200).entity(data).build();
	}

	@GET
	@Path("/findDistinctImsi")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response findDistinctIMSI() {
		System.out.println("get list of IMSI's from base table [Changed to non distinct IN Sprint2]");
		final List<BaseData> imsiQuery4 = baseDataDao.getIMSIListOfCauseCodes();
		return Response.status(201).entity(imsiQuery4).build();
	}
	
	
	/**
	 * -------------Story 6 --------------------
	 */
	@GET
	@Path("/findListOfDistinctImsisForDropdown")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response findListOfDistinctIMSIsForDropDown() {
		System.out.println("get distinct list of IMSI's from base table to populate the dropdown box for search ");
		final List<BaseData> imsiQueryDistinct = baseDataDao.getDistinctIMSIListForDropdown();
		return Response.status(201).entity(imsiQueryDistinct).build();
	}
	
	@GET
	@Path("/findCauseFailuresByImsi/{query}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response findCauseCodesByIMSI(@PathParam("query") final String query) {
		System.out.println("findByIMSI" + query);
		final List<BaseData> data = baseDataDao.getCauseCodesByIMSI(query);
		return Response.status(200).entity(data).build();
	}
	
	
	

	/* Story 10 - Call Failures per phone model */
	@GET
	@Path("/failuresPerDeviceModel/{model}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response getImsiFailuresPerDeviceModel(@PathParam("model") final String model) {
		final List<BaseData> failuresPerDeviceList = baseDataDao.getFailuresPerDeviceModel(model);
		return Response.status(200).entity(failuresPerDeviceList).build();

	}
	
	/*** Story 10 Graph level 2 - Call Failures per phone model 
	 * Drill down level 2 of story 10 graph*/
	@GET
	@Path("/failuresPerDeviceModelAndEvent/{modelAndImsi}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response getImsiFailuresPerDeviceModelAndEvent(@PathParam("modelAndImsi") final String modelAndEvent) {
		final String[] parseAllInfo = modelAndEvent.split("&");
		final String[] model = parseAllInfo[0].split("=");
		final String[] event = parseAllInfo[1].split("=");
		final List<BaseData> failuresPerDeviceList = baseDataDao.getUniqueFailuresPerDeviceModelAndEvent(model[1], event[1]);
		return Response.status(200).entity(failuresPerDeviceList).build();

	}
	
	/**
	 * Story 11 - As a Network Management Engineer I want to see the Top 10 Market/Operator/Cell ID
	 *  combinations that had call failures during a time period
	 */
	@GET
	@Path("/imsiPerMarketOperatorPerCellPerDate/{dateAndCellAndOperator}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response getImsiPerMarketOperatorPerCellPerDate(@PathParam("dateAndCellAndOperator") final String dateAndCellAndOperator){
		final List<BaseData> top10FailuresList = baseDataDao.getImsiPerMarketOperatorPerCellPerDate(dateAndCellAndOperator);
		return Response.status(201).entity(top10FailuresList).build();
		
	}
	
	/**
	 * Story 11 - original level 2 drill  down
	 */
	@GET
	@Path("/top10FailuresWithDate/{dateInfo}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response getTop10FailuresWithDate(@PathParam("dateInfo") final String dateInfo){
		final List<BaseData> top10FailuresList = baseDataDao.getTop10FailuresWithDate(dateInfo);
		return Response.status(201).entity(top10FailuresList).build();
		
	}
	
	
	/**
	 * Story 11 Level 1 Drill down for graph
	 * Top 10 Worst Markets by date
	 */
	@GET
	@Path("/Top10MarketByDate/{dateInfo}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response getTop10Market(@PathParam("dateInfo") final String dateInfo){
		final List<BaseData> top10WorstMarketsList = baseDataDao.getTop10WorstMarketsList(dateInfo);
		System.out.println(top10WorstMarketsList);
		return Response.status(201).entity(top10WorstMarketsList).build();
		
	}
	
	/**
	 * Story 11 Level 2 Drill down for graph
	 * Top 10 Worst Operators Per Market  by date
	 */
	@GET
	@Path("/Top10OperatorsPerMarketByDate/{dateAndMarket}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response getTop10Operators(@PathParam("dateAndMarket") final String dateAndMarket){
		final List<BaseData> top10WorstOperatorPerMarketByDateList = baseDataDao.getTop10WorstOperatorsList(dateAndMarket);
		return Response.status(200).entity(top10WorstOperatorPerMarketByDateList).build();
		
	}
	
	/**
	 * Story 11 Level 3 Drill down for graph
	 * Top 10 Worst Cells Per Operator by date
	 */
	@GET
	@Path("/Top10CellsPerOperatorByDate/{dateAndOperator}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response getTop10Cells(@PathParam("dateAndOperator") final String dateAndOperator){
		final List<BaseData> top10WorstCellsPerOperatorByDateList = baseDataDao.getTop10WorstCellsList(dateAndOperator);
		return Response.status(200).entity(top10WorstCellsPerOperatorByDateList).build();
		
	}
	
	
	/**
	 * Story 13 - Top 10 worst cells that have call failures
	 */
	@GET
	@Path("/top10FailCells/")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response getTop10FailCells() {
		final List<BaseData> top10FailCells = baseDataDao.getTop10FailCells();
		return Response.status(200).entity(top10FailCells).build();

	}

	/**
	 * This method is used to get the number of failures associated with a
	 * provided IMSI and that occurred over a date and time period.
	 * 
	 * @param dateAndImsi
	 *            the date and time period for the query and the IMSI
	 * @return 200 if the GET is successful
	 */
	@GET
	@Path("/failuresPerIMSI/{dateAndImsi}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response getFailureCountPerImsi(@PathParam("dateAndImsi") final String dateAndImsi) {
		System.out.println("Date and IMSI: " + dateAndImsi);
		final List<BaseData> failuresPerIMSIList = baseDataDao.getFailuresPerImsi(dateAndImsi);
		return Response.status(200).entity(failuresPerIMSIList).build();
	}

	@GET
	@Path("/listOfManufacturersInBaseData")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response getListOfManufacturersInBaseData() {
		final List<String> listOfManufacturers = baseDataDao.getListOfManufacturersFromUeTableViaBaseData();
		return Response.status(200).entity(listOfManufacturers).build();
	}

	@GET
	@Path("/getListOfErrorsByHandsetAndDate/{manufacturerAndHandsetAndDate : .+}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response getListOfErrorsByHandsetAndDate(
			@PathParam("manufacturerAndHandsetAndDate") final String manufacturerAndHandsetAndDate) {
		final List<BaseData> listOfHandsets = baseDataDao
				.getListOfErrorsByHandsetAndDate(manufacturerAndHandsetAndDate);
		return Response.status(200).entity(listOfHandsets).build();
	}

	@GET
	@Path("/listOfHandsetsByManufacturer/{manufacturer}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response getListOfHandsetsByManufacturer(@PathParam("manufacturer") final String manufacturer) {
		final List<String> listOfHandsets = baseDataDao.getListOfHandsetsByManufacturers(manufacturer);
		return Response.status(200).entity(listOfHandsets).build();
	}


	@EJB
	private PaginationClassDataTable imsiPerFailureClassTbl;

	String imsiPerFailureClassCols [] = {"imsi","failureClass","description"};
	/**
	 * This method is used to get all the IMSI's that belonged to
	 * a user defined failure class.
	 * 
	 * @param failureClass the failure class defined by the user
	 * @return 200 if the GET is successful
	 */
	@GET
	@Path("/imsisPerFailureClass/{failureClass}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response getIMSIsPerFailureClass(@PathParam("failureClass") final String failureClass,
			@QueryParam("sEcho")final  Integer sEcho,
			@QueryParam("iDisplayLength")final  Integer iDisplayLength,
			@QueryParam("iDisplayStart")final  Integer iDisplayStart,
			@QueryParam("sSearch")final  String search,
			@QueryParam("iSortCol_0")final  String iSortCol_0,
			@QueryParam("sSearch")final  String sSearch,
			@QueryParam("sSortDir_0")final  String sSortDir_0) {

		final int col = Integer.parseInt(iSortCol_0);
		final String colName = imsiPerFailureClassCols[col];
//		System.out.println("getData()");
//		System.out.println("invoking get");
//		System.out.println("sEcho: "+sEcho);
//		System.out.println("iDisplayLength: "+iDisplayLength);
//		System.out.println("iDisplayStart: "+iDisplayStart);
//		System.out.println("sSearch: "+search);
//		System.out.println("sSortDir_0: "+sSortDir_0);
//		System.out.println("iSortCol_0: "+iSortCol_0);
//		System.out.println("sSearch: "+sSearch); 
		final List<BaseData> imsisPerFailureClassList = baseDataDao.getIMSIsPerFailureClass(iDisplayStart,iDisplayLength,
				colName,sSearch,sSortDir_0,failureClass);
		imsiPerFailureClassTbl.setAaData(imsisPerFailureClassList);
		final long totalRecords = baseDataDao.getCount(failureClass);
		System.out.println("Total Records WS---------------->"+totalRecords);
		imsiPerFailureClassTbl.setiTotalDisplayRecords(String.valueOf(totalRecords));
		imsiPerFailureClassTbl.setiTotalRecords(String.valueOf(totalRecords));
		imsiPerFailureClassTbl.setsEcho(sEcho);
		return Response.status(200).entity(imsiPerFailureClassTbl).build();
	}


	/**
	 * This method is used to get all the failure classes in the database.
	 * @return 200 if the GET is successful
	 */
	@GET
	@Path("/failureClasses")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response getAllFailureClasses() {
		final List<BaseData> failureClassesList = baseDataDao.getAllFailureClasses();
		return Response.status(201).entity(failureClassesList).build();
	}
	
	

	/**
	 * Story 12
	 * This method is for the top 10 imsis with failures in a given time period.
	 * @return Response
	 * @param Date
	 *            information from the date picker
	 * */
	@GET
	@Path("/Top10ImsiWithFailureList/{dateInfo}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response getTop10ImsisWithFailuresTimePeriod(@PathParam("dateInfo") final String dateInfo) {
		final List<BaseData> top10ImsisWithFailuresTimePeriod = baseDataDao.getTop10ImsisWithFailuresTimePeriod(dateInfo);
		return Response.status(200).entity(top10ImsisWithFailuresTimePeriod).build();

	}
	/**
	 * Story 12 LEVEL 2 Drilldown
	
	 * */
	
	@GET
	@Path("/failureCountPerImsiPerDateGroupByEvent/{dateAndImsi}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response getFailureCountPerImsiPerDateGroupByEvent(@PathParam("dateAndImsi") final String dateAndImsi) {
	
		final List<BaseData> failureCountPerImsiPerDateGroupByEvent = baseDataDao.getfailureCountPerImsiPerDateGroupByEvent(dateAndImsi);
		return Response.status(200).entity(failureCountPerImsiPerDateGroupByEvent).build();
	}
	
	//Setter for mockito tests.. justified comment.
	public void setImsiPerFailureClassTbl(
			PaginationClassDataTable imsiPerFailureClassTbl) {
		this.imsiPerFailureClassTbl = imsiPerFailureClassTbl;
	}
	
	
	
	
	/**
	 * This method is used to get the each failure for a given IMSI and date and time 
	 * period for the IMSI and failures duration drill down chart.
	 * @return Response
	 * @param Date
	 *            information from the date picker
	 */
	@GET
	@Path("/failuresBreakDownPerIMSI/{datesAndIMSI}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response getFailuresPerIMSIForDrilldown(@PathParam("datesAndIMSI") final String datesAndIMSI) {
		final List<BaseData> individualFailuresPerIMSI = baseDataDao.getFailuresPerIMSIForDrilldown(datesAndIMSI);
		return Response.status(200).entity(individualFailuresPerIMSI).build();

	}



}
