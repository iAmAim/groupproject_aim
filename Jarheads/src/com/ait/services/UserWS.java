package com.ait.services;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ait.dataAccessObject.UserDAO;
import com.ait.models.User;

@Path("/users")
@Stateless
@LocalBean
public class UserWS {

	@EJB
	private UserDAO userDao;

	@RolesAllowed("System Administrator")
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response findAll() {
		System.out.println("Getting all registered users");
		final List<User> users = userDao.getAllUsers();
		return Response.status(200).entity(users).build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/searchbyid/{id}")
	@PermitAll
	public Response findUserById(@PathParam("id")final  int userId) {
		System.out.println("Getting user by id: "+userId);
		final User user= userDao.getUserById(userId);
		return Response.status(200).entity(user).build();
	}
	
	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response addUser(final User user) {
		System.out.println("Adding new user to the DB");
		userDao.save(user);
		return Response.status(201).entity(user).build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{username}")
	@PermitAll
	public Response findUserByUserName(@PathParam("username")final  String username) {
		System.out.println("Checking if user is already registered");
		final User user = userDao.getUserByName(username);
		return Response.status(200).entity(user).build();

	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/login/{userNameAndPassword}")
	@PermitAll
	public Response findUserByUserNameAndPassword(@PathParam("userNameAndPassword")final  String userNameAndPassword) {
		final String[] params = userNameAndPassword.split("&");
		if (params.length < 2) {
			return Response.status(404).entity(null).build();
		}
		System.out.println(params.length);
		final String[] userNamePair = params[0].split("=");
		final String[] passWordPair = params[1].split("=");

		if (userNamePair.length < 2 || passWordPair.length < 2) {
			return Response.status(500).entity(null).build();
		}
		System.out.println(userNamePair[1]);
		System.out.println(passWordPair[1]);
		final User user = userDao.findUserByUserNameAndPassword(userNamePair[1], passWordPair[1]);
		return user != null ? Response.status(200).entity(user).build() : Response.status(404).build();
	}
	
	@PUT
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response updateUser(final User user) {
		System.out.println("Updating user in the DB");
		userDao.update(user);
		return Response.status(200).entity(user).build();
	}
	
	@DELETE
	@Path("/delete/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PermitAll
	public Response deleteUser(@PathParam("id")final  int userId ) {
		System.out.println("Deleting user in the DB:"+userId);
		userDao.delete(userId);
		return Response.status(204).build();
	}

	public void setUserDao(UserDAO userDao) {
		this.userDao = userDao;
	}

	
	
	
}
