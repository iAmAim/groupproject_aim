package com.ait.consistencycheck;
/**
 * This class is used to create an market id (MCC) and operator id (MNC)
 * combination object. This object is required for the validation process 
 * of an market id and operator id of a row object.
 * 
 * @version 1.0
 */
public class MCC_MNCCombo {
	
	private final int marketId;
	private final int operatorId;

	/**
	 * This constructs the market id and operator id combination object.
	 * @param marketId the MCC of the row object.
	 * @param operatorId the MNC of the row object.
	 */
	public MCC_MNCCombo(final int marketId,final int operatorId){
		this.marketId=marketId;
		this.operatorId=operatorId;
	}

	public int getMarket() {
		return marketId;
	}

	public int getOperator() {
		return operatorId;
	}
}
