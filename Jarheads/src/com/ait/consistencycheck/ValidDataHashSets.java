package com.ait.consistencycheck;

/**
 * This class implements the ValidData interface and provides the implementation
 * for the inherited methods.
 * 
 * @version 1.0
 */
public class ValidDataHashSets implements ValidData {

	@Override
	public void populateEventIdCauseCodeHashSet(final Integer causeCode,final Integer eventId) {
		eventId_CauseCodeHashSet.add(new EventCauseCombo(causeCode,eventId));
	}

	@Override
	public void populateFailureClassHashSet(final Integer failureClass) {
		failureClassHashSet.add(failureClass);
	}

	@Override
	public void populateMCC_MNCHashSet(final Integer marketId,final Integer operatorId) {
		mccMNCHashSet.add(new MCC_MNCCombo(marketId,operatorId));
	}

	@Override
	public void populateUETypeHashSet(final Integer ueType) {
		ueTypeHashSet.add(ueType);	
	}

	@Override
	public boolean isCauseCodeEventIdComboInValidHashSet(final EventCauseCombo eventCauseCombo) {
		for(final EventCauseCombo ecc: eventId_CauseCodeHashSet){
			if(ecc.getCauseCode()==eventCauseCombo.getCauseCode()
					&&ecc.getEventId()==eventCauseCombo.getEventId()){
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isFailureClassInValidHashSet(final Integer failureClass) {
		return failureClassHashSet.contains(failureClass);
	}

	@Override
	public boolean isMarketIdAndOperatorIdComboInValidHashSet(final MCC_MNCCombo mccMNCCombo) {
		for(final MCC_MNCCombo mcc_mnc: mccMNCHashSet){
			if(mcc_mnc.getMarket()==mccMNCCombo.getMarket()
					&&mcc_mnc.getOperator()==mccMNCCombo.getOperator()){
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isUETypeInValidHashSet(final Integer ueType) {
		return ueTypeHashSet.contains(ueType);
	}
}

