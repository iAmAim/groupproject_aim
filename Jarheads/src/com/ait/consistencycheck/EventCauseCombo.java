package com.ait.consistencycheck;
/**
 * This class is used to create an event id and cause code combination object.
 * This object is required for the validation process of an event id and cause code
 * of a row object.
 * 
 * @version 1.0
 */
public class EventCauseCombo {
	
	private final int causeCode;
	private final int eventId;

	/**
	 * This constructs the event id and cause code combination object.
	 * @param causeCode the cause code of the row object.
	 * @param eventId the event id of the row object.
	 */
	public EventCauseCombo(final int causeCode,final int eventId){
		this.causeCode=causeCode;
		this.eventId=eventId;
	}

	public int getCauseCode() {
		return causeCode;
	}

	public int getEventId() {
		return eventId;
	}
}
