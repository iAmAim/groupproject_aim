package com.ait.consistencycheck;

import java.util.HashSet;
import java.util.Set;

/**
 * This interface is used to provide HashSets to store valid data. It
 * also provides methods to populate the HashSets with valid data and check 
 * whether certain data is stored within these HashSets.
 * 
 * @version 1.0
 */
public interface ValidData {
	
	Set<EventCauseCombo> eventId_CauseCodeHashSet = new HashSet<>();
	Set<Integer> failureClassHashSet = new HashSet<>();
	Set<MCC_MNCCombo> mccMNCHashSet = new HashSet<>();
	Set<Integer> ueTypeHashSet = new HashSet<>();
	
	/**
	 * This method is used to populate the event id cause code combination HashSet
	 * with valid data. The valid data is based on the data stored in the Event-Cause table
	 * in the Excel file.
	 * 
	 * @param causeCode the valid cause code from the Event-Cause table in the Excel file.
	 * @param eventId the valid event id from the Event-Cause table in the Excel file.
	 */
	void populateEventIdCauseCodeHashSet(Integer causeCode,Integer eventId);
	
	/**
	 * This method is used to populate the failure class HashSet with valid data. 
	 * The valid data is based on the data stored in the Failure Class table
	 * in the Excel file.
	 * 
	 * @param failureClass the valid failure class from the Failure Class table in the Excel file.
	 */
	void populateFailureClassHashSet(Integer failureClass);
	
	/**
	 * This method is used to populate the MCC/MNC HashSet with valid data.
	 * The valid data is based on the mobile country code (MCC) and 
	 * mobile network code (MNC) data stored in the MCC-MNC table in the CSV file.
	 * This data translates to the market id (MCC) and operator id (MNC) in the
	 * base data table.
	 * 
	 * @param marketId the valid MCC from the MCC-MNC table in the Excel file.
	 * @param operatorId the valid MNC from the MCC-MNC table in the Excel file.
	 */
	void populateMCC_MNCHashSet(Integer marketId,Integer operatorId);
	
	/**
	 * This method is used to populate the user equipment type HashSet with valid data.
	 * The valid data is based on the type allocation code (TAC) 
	 * data stored in the UE table in the Excel file.
	 * 
	 * @param UEType the valid TAC from the UE table in the Excel file.
	 */
	void populateUETypeHashSet(Integer UEType);
	
	/**
	 * This method is used to check if the event id and cause code combination
	 * is located within the associated valid HashSet.
	 * 
	 * @param eventCauseCombo the event id and cause code combination object being checked
	 * for validity.
	 * @return true if the event id and cause code combination is located within 
	 * the valid event id and cause code combination HashSet or false otherwise.
	 */
	boolean isCauseCodeEventIdComboInValidHashSet(EventCauseCombo eventCauseCombo);
	
	/**
	 * This method is used to check if the failure class
	 * is located within the associated valid HashSet.
	 * 
	 * @param failureClass the failure class being check for validity.
	 * @return true if the failure class is located within 
	 * the valid failure class HashSet or false otherwise.
	 */
	boolean isFailureClassInValidHashSet(Integer failureClass);
	
	/**
	 * This method is used to check if the market id (MCC) and 
	 * operator id (MNC) combination is located within the associated valid HashSet.
	 * 
	 * @param mccMNCCombo the market id and operator id combination object being checked
	 * for validity.
	 * @return true if the market id and operator id combination is located within 
	 * the valid market id and operator id combination HashSet or false otherwise.
	 */
	boolean isMarketIdAndOperatorIdComboInValidHashSet(MCC_MNCCombo mccMNCCombo);
	
	/**
	 * This method is used to check if the user equipment type
	 * is located within the associated valid HashSet.
	 * 
	 * @param ueType the user equipment type being check for validity.
	 * @return true if the user equipment type is located within 
	 * the valid user equipment type HashSet or false otherwise.
	 */
	boolean isUETypeInValidHashSet(Integer ueType);
	
}
