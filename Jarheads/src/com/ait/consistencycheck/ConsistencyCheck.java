package com.ait.consistencycheck;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ait.models.BaseData;

/**
 * This class provides methods that are used to check the if each data within each row object 
 * is valid or invalid according to pre-defined validity standards. 
 * 
 * <p>The row object is passed to the method called checkRecordIsInvalid which then
 * calls other methods based on the type of the data field being checked.
 * 
 * @version 1.0
 */
public class ConsistencyCheck{
	
	private ValidDataHashSets hashSets;
	
	/**
	 * This constructs a ValidHashSets object that is used
	 * to reference the HashSets that consist of certain valid data
	 * in the ValidDataHashSets class.
	 */
	public ConsistencyCheck(){
		hashSets = new ValidDataHashSets();
	}

	public ValidDataHashSets getHashSets() {
		return hashSets;
	}

	public void setHashSets(final ValidDataHashSets hashSets) {
		this.hashSets = hashSets;
	}

	/**
	 * This method takes a BaseData object as parameter and checks each
	 * data field within that object to see if each data field adheres to the
	 * pre-defined valid data standards.
	 * 
	 * @param record the row object being checked.
	 * @return true if the row object is valid according to all the validity
	 * checks and false if the row object fails one of the checks
	 */
	public boolean checkRecordIsValid(final BaseData record){
		
		if(!isDateFormatValid(record.getDate())){
			return false;
		}
		if(!isNEVersionValid(record.getNeVersion())){
			return false;
		}
		if(!isEventIdAndCauseCodeCombinationValid(record.getCauseCode(),record.getEventId())){
			return false;
		}
		if(!isFailureClassValid(record.getFailureClass(),record)){
			return false;
		}
		if(!isUETypeValid(record.getUeType())){
			return false;
		}
		if(!isMarketIdAndOperatorIdCombinationValid(record.getMarket(),record.getOperator())){
			return false;
		}
		if(!isDurationAndCellIdValid(record.getDuration(),record.getCellId())){
			return false;
		}
		if(!isIMSIAndHIER_IDsValid(record.getImsi(),record.getHier3id(),
				record.getHier32id(),record.getHier321id())){
			return false;
		}
		return true;
	}

	/**
	 * This method is used to check whether the date/time data field of
	 * the row object is valid. A valid date is in European date and 
	 * time format e.g. 15/04/2015 16:15. This method throws a <tt>ParseException</tt>
	 * if the date being check is not in the correct format.
	 * 
	 * @param date_time the date/time of the row object.
	 * @return true if date is valid or false otherwise.
	 */
	public boolean isDateFormatValid(final String date_time) {
		try {
			if(date_time.length()>"dd/MM/yyyy HH:mm".length()){
				return false;
			}
			Date date = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(date_time);	
		} catch (ParseException ex) {
			return false; 
		}
		return true;
	}

	/**
	 * This method is used to check whether the network element data field of
	 * the row object is valid. A valid network element version is a String
	 * in the consists of two digits followed by a single capital letter e.g. 12A.
	 * 
	 * @param neVersion the network element version of the row object.
	 * @return true if the network element version is valid or false otherwise.
	 */
	public boolean isNEVersionValid(final String neVersion) {
		if(neVersion.length()==3){
			final Pattern pattern = Pattern.compile("\\d{2}[A-Z]{1}");
			final Matcher matcher = pattern.matcher(neVersion);
			if(matcher.find()){
				return true;
			}
		}
		return false;
	}

	/**
	 * This method is used to check whether the event id and cause code combination of 
	 * the row object are valid. This method throws a <tt>NumberFormatException</tt> if 
	 * the cause code and event id cannot be converted to an int when the convertStringToInteger 
	 * method is called.
	 *  
	 * <p>This method then checks to see if the cause code and event id combination are 
	 * valid by first creating an object which consists of these two parameters and second 
	 * passing this object to another method that checks to see if this combination object 
	 * is located in the event id and cause code valid data hash set. If it is located 
	 * in this hash set then it is deemed a valid event id and cause code combination.
	 * 
	 * @param causeCode the cause code of the row object.
	 * @param eventId the event id of the row object.
	 * @return true if the event id and cause code combination is valid or false otherwise
	 */
	public boolean isEventIdAndCauseCodeCombinationValid(final String causeCode,final String eventId) {
		try{
			final int causeCodeAsInt = convertStringToInteger(causeCode);
			final int eventIdAsInt = convertStringToInteger(eventId);
			final EventCauseCombo eventCauseCombo = new EventCauseCombo(causeCodeAsInt,eventIdAsInt);
			if(hashSets.isCauseCodeEventIdComboInValidHashSet(eventCauseCombo)){
				return true;
			}
		}catch(NumberFormatException ex){
			return false;
		}
		return false;
	}
	
	/**
	 * This method is used to parse Strings to Integers.
	 * @param numberAsString the String to be converted to an integer.
	 * @return the parsed Integer or <tt>NumberFormatException</tt> is thrown. 
	 */
	private int convertStringToInteger(final String numberAsString) {
		return Integer.parseInt(numberAsString);
	}

	/**
	 * This method is used to check whether the failure class data field of the 
	 * row object is valid. The failure class is deemed valid if it is null therefore a check
	 * is done to determine this. If the failure class is not null 
	 * this method throws a <tt>NumberFormatException</tt> if the failure 
	 * class cannot be converted to an int when the convertStringToInteger method is called.
	 * 
	 * <p>This method then checks to see if the failure class is valid by
	 * passing the failure class int to another method that checks to see if this failure class 
	 * is located in the failure class valid data hash set. If it is located 
	 * in this hash set then it is deemed a valid failure class.
	 * @param failureClass the failure class of the row object.
	 * @return true if the failure class is valid or false otherwise.
	 */
	public boolean isFailureClassValid(final String failureClass, BaseData record) {
		if("null".equals(failureClass)||"(null)".equals(failureClass)){
			record.setFailureClass("");
			return true;
		}
		try{
			final int failureCodeAsInt = convertStringToInteger(failureClass);
			if(hashSets.isFailureClassInValidHashSet(failureCodeAsInt)){
				return true;
			}
		}catch(NumberFormatException ex){
			return false;
		}
		return false;
	}
	
	/**
	 * This method is used to check whether the market id (MCC) and operator id (MNC)
	 * combination of the row object are valid. This method throws 
	 * a <tt>NumberFormatException</tt> if the market id and operator id 
	 * cannot be converted to an int when the convertStringToInteger method is called.
	 *  
	 * <p>This method then checks to see if the market id and operator id combination are 
	 * valid by first creating an object which consists of these two parameters and second 
	 * passing this object to another method that checks to see if this combination object 
	 * is located in the MCC MNC valid data hash set. If it is located 
	 * in this hash set then it is deemed a valid market id and operator id combination.
	 * 
	 * @param marketId the MCC of the row object.
	 * @param operatorId the MNC of the row object.
	 * @return true if the market id and operator combination is valid or false otherwise
	 */
	public boolean isMarketIdAndOperatorIdCombinationValid(final String marketId,
			final String operatorId) {
		try{
			final int marketIdAsInt = convertStringToInteger(marketId);
			final int operatorIdAsInt = convertStringToInteger(operatorId);
			final MCC_MNCCombo mccMNCCombo = new MCC_MNCCombo(marketIdAsInt,operatorIdAsInt);
			if(hashSets.isMarketIdAndOperatorIdComboInValidHashSet(mccMNCCombo)){
				return true;
			}
		}catch(NumberFormatException ex){
			return false;
		}
		return false;
	}

	/**
	 * This method is used to check whether the duration and cell id of the row
	 * object are valid. This method throws a <tt>NumberFormatException</tt> if the 
	 * duration or cell id cannot be converted to an int when the convertStringToInteger 
	 * method is called.
	 * 
	 * @param duration the duration (in milliseconds) of the row object.
	 * @param cellId the id of the cell of the row object.
	 * @return true if the duration and cell id are valid or false otherwise.
	 */
	public boolean isDurationAndCellIdValid(final String duration,final String cellId) {
		try{
			convertStringToInteger(duration);
			convertStringToInteger(cellId);
			return true;
		}catch(NumberFormatException ex){
			return false;
		}
	}

	/**
	 * This method is used to check whether the imsi, hier3_id, hier32_id and hier321_id 
	 * of the row object are valid. This method throws a <tt>NumberFormatException</tt> 
	 * if any of the parameters cannot be converted to a BigInteger when the convertStringToBigInteger 
	 * method is called.
	 * 
	 * @param imsi the international mobile subscriber identity of the row object.
	 * @param hier3_id the id relates to the multi-antenna configuration of the LTE RBS
	 * of the row object.
	 * @param hier32id the id relates to the multi-antenna configuration of the LTE RBS.
	 * of the row object.
	 * @param hier321id the id relates to the multi-antenna configuration of the LTE RBS.
	 * of the row object.
	 * @return true if the imsi, hier3_id, hier32_id and hier321_id are valid or false
	 * otherwise.
	 */
	public boolean isIMSIAndHIER_IDsValid(final String imsi,final String hier3_id,final String hier32_id,
			final String hier321_id) {
		try{
			convertStringToBigInteger(imsi);
			convertStringToBigInteger(hier3_id);
			convertStringToBigInteger(hier32_id);
			convertStringToBigInteger(hier321_id);
			return true;
		}catch(NumberFormatException ex){
			return false;
		}
	}

	/**
	 * 
	 * @param bigIntegerAsString the String to be converted to a BigInteger
	 * @return the BigInteger version of the String or <tt>NumberFormatException</tt> is thrown. 
	 */
	private BigInteger convertStringToBigInteger(final String bigIntegerAsString) {
		return new BigInteger(bigIntegerAsString);
	}
	
	/**
	 * This method is used to check whether the user equipment type of the row object is valid.
	 * This method throws a <tt>NumberFormatException</tt> if the user equipment type
	 * cannot be converted to an int when the convertStringToInteger method is called.
	 * 
	 * <p>This method then checks to see if the user equipment type is valid by
	 * passing the user equipment type int to another method that checks to see if this 
	 * user equipment type is located in the user equipment type valid data hash set. 
	 * If it is located in this hash set then it is deemed a user equipment type.
	 * 
	 * @param ueType the user equipment type of the row object.
	 * @return true if the user equipment type is valid or false otherwise.
	 */
	public boolean isUETypeValid(final String ueType) {
		
		try{
			final int ueTypeAsInt = convertStringToInteger(ueType);
			if(hashSets.isUETypeInValidHashSet(ueTypeAsInt)){
				return true;
			}
		}catch(NumberFormatException ex){
			return false;
		}
		return false;
	}

}

