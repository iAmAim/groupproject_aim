package com.ait.dataAccessObject;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.ait.models.User;

@Stateless
@LocalBean
public class UserTypeDAO {
	@PersistenceContext(name = "testpoi")
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<User> getAllUserTypes() {
		final Query query = entityManager.createQuery("Select u from UserType u order by u.userTypeId");
		return query.getResultList();
	}

}
