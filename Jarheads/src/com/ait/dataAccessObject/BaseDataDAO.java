package com.ait.dataAccessObject;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.ait.importFile.HasFileAlreadyBeenImported;
import com.ait.importFile.ImportFromExcelToDatabase;
import com.ait.models.BaseData;

/**
 * This class contains the DAO methods used to query information from the
 * base data table in the database.
 */
@Stateless
@LocalBean
public class BaseDataDAO {

	@PersistenceContext(name = "testpoi")
	private EntityManager entityManager;

	@EJB
	ImportFromExcelToDatabase excelToDB;

	@EJB
	HasFileAlreadyBeenImported checkFileNotDuplicate;

	public BaseDataDAO() {

	}

	public BaseDataDAO(final ImportFromExcelToDatabase iFETD, final HasFileAlreadyBeenImported hFABI,
			final EntityManager emBaseDAO) {
		this.entityManager = emBaseDAO;
		this.excelToDB = iFETD;
		this.checkFileNotDuplicate = hFABI;
	}


	/**
	 * This method is used to get all the data from the base data table in 
	 * the database
	 * @return list of all base data rows
	 */
	@SuppressWarnings("unchecked")
	public List<BaseData> getAllData() {
		final Query query = entityManager.createQuery("Select d from BaseData d");
		return query.getResultList();
	}

	/**
	 * This method is used to saved the Excel data into the data 
	 * @return string message about result of the import 
	 * 
	 */
	public String saveImportedData(final String address, final String fileName, final boolean manualmport) {
		try {
			System.out.println("FILEMANE TO CHECK " + fileName);
			if (!checkFileNotDuplicate.hasTheFileBeenImportedBefore(fileName)) {
				return "The file selected has already been imported";
			}
		}
		catch (IOException e1) {
			return "Error in file creation";
		}
		return excelToDB.convertFile(address, fileName, manualmport);
	}



	/**
	 * This method is used to call the auto import function
	 * @return string message about the result of the import
	 */
	public String callAutoImport() {
		return excelToDB.autoImport();
	}

	/**
	 * This method is used to get the maximum date from the base data table 
	 * in the database
	 * @return the maximum date in the base data table
	 */
	public BaseData getMaxDate() {
		final Query query = entityManager.createQuery("Select d from BaseData d  order by" + " d.date desc");
		query.setMaxResults(1);
		final BaseData maxDate = (BaseData) query.getSingleResult();
		return maxDate;
	}


	/**
	 * This method is used to get the minimum date from the base data table 
	 * in the database
	 * @return the minimum date in the base data table
	 */
	public BaseData getMinDate() {
		final Query query = entityManager.createQuery("Select d from BaseData d  order by" + " d.date asc");
		query.setMaxResults(1);
		final BaseData minDate = (BaseData) query.getSingleResult();
		return minDate;
	}

	/**
	 * This method is used to get the total number of rows in the base data table i
	 * in the database
	 * @return a string that contains the number of rows in base data table
	 */
	public String getDatabaseSize() {
		final Query query = entityManager.createQuery("Select Count(d.callFailure_ID) from BaseData d ");
		final Long sizeOfDatabaseL = (Long) query.getSingleResult();
		final String sizeOfDatabase = sizeOfDatabaseL.toString();
		return sizeOfDatabase;
	}

	/**
	 * This method gets all the IMSI's in the base data table with failures during
	 * the defined time period. Server side pagination is used by this query 
	 * as the result set can be quite large
	 * 
	 * @param dateInfo the date and time being searched on
	 * @param start the index of the first row of that page of the data table
	 * @param amount the amount of rows being shown by that page of the data table
	 * @param columnName the name of the column being sorted on in data table
	 * @param searchTerm the term being searched on in the search field of the data table
	 * @param sortDir the sorting applied to the data table
	 * @return list of all IMSI's with failures during the time period
	 */
	@SuppressWarnings("unchecked")
	public List<BaseData> getImsiFailuresForGivenTimeRange(final String dateInfo, final int start, final int amount,
			final String columnName, final String searchTerm, final String sortDir) {
		final String[] parseDateInfo = dateInfo.split("&");
		final String min = getMinParsed(parseDateInfo);
		final String max = getMaxParsed(parseDateInfo);
		String stringyQuery = "Select d from BaseData d where d.date between" + " '" + min + "' and '" + max + "'"
				+ " ORDER BY " + columnName + " " + sortDir;
		System.out.println("DAO COLUMN NAME " + columnName);
		if (!("").equals(searchTerm)) {
			final String search = " WHERE (d.date LIKE '%" + searchTerm + "%'" + " OR d.imsi LIKE '%" + searchTerm
					+ "%'" + " OR d.failureClass LIKE '%" + searchTerm + "%'" + " OR d.ueType LIKE '%" + searchTerm
					+ "%'" + " OR d.duration LIKE '%" + searchTerm + "%'" + " OR d.causeCode LIKE '%" + searchTerm
					+ "%')";
			stringyQuery = "SELECT d FROM BaseData d " + search + " ORDER BY " + columnName + " " + sortDir;
		}
		final Query query = entityManager.createQuery(stringyQuery);
		query.setFirstResult(start);
		query.setMaxResults(amount);
		return query.getResultList();
	}

	/**
	 * This method is used to convert the "to" search date into the date format
	 * used in SQL
	 * @param parseDateInfo the date being converted
	 * @return the converted date format
	 */
	public String getMaxParsed(final String[] parseDateInfo) {
		final String maxYear = parseDateInfo[7] + "-" + parseDateInfo[5] + "-" + parseDateInfo[6];
		final String maxTime = parseDateInfo[8] + ":" + parseDateInfo[9] + ":00";
		final String max = maxYear + " " + maxTime;
		return max;
	}

	/**
	 * This method is used to convert the "from" search date into the date format
	 * used in SQL
	 * @param parseDateInfo the date being converted
	 * @return the converted date format
	 */
	public String getMinParsed(final String[] parseDateInfo) {
		final String minYear = parseDateInfo[2] + "-" + parseDateInfo[0] + "-" + parseDateInfo[1];
		final String minTime = parseDateInfo[3] + ":" + parseDateInfo[4] + ":00";
		final String min = minYear + " " + minTime;
		return min;
	}



	/**
	 * This method is used to get all the IMSI's with failures, the number of times they
	 * have had failures and the total duration of that IMSI's failures during 
	 * a defined time period.
	 * @param dateInfo dateInfo the date and time being searched on
	 * @return list of all the IMSI failures in this time period
	 * */
	@SuppressWarnings("unchecked")
	public List<BaseData> getImsiFailCountAndDuration(final String dateInfo) {
		final String[] parseDateInfo = dateInfo.split("&");
		final String min = getMinParsed(parseDateInfo);
		final String max = getMaxParsed(parseDateInfo);
		final Query queryFailCountAndDuration = entityManager
				.createQuery("SELECT DISTINCT d.imsi, COUNT(d), SUM(d.duration) " + "FROM BaseData AS d "
						+ "WHERE d.date " + "BETWEEN '" + min + "' " + "AND '" + max + "' " + "GROUP BY d.imsi");
		return queryFailCountAndDuration.getResultList();
	}

	/**
	 * This method is used to get a failed IMSI's cause code, event id and description
	 * @param IMSI the IMSI being searched for
	 * @return list of the cause codes and event ids that have affected that IMSI
	 */
	@SuppressWarnings("unchecked")
	public List<BaseData> getIMSI(final String IMSI) {
		final Query query = entityManager
				.createQuery("Select i.imsi, i.causeCode, i.eventId, i.eventAndCause.description FROM BaseData AS i WHERE i.imsi LIKE ?1");
		query.setParameter(1, IMSI);
		return query.getResultList();
	}

	/**
	 * This method is used to get the distinct IMSI's from the database
	 * @return list of all distinct IMSI's
	 */
	@SuppressWarnings("unchecked")
	public List<BaseData> getIMSIListOfCauseCodes() {
		final Query query = entityManager.createQuery("Select DISTINCT e.imsi FROM BaseData e");
		return query.getResultList();
	}

	/**
	 * This method is used to get the distinct cause codes and event ids that have affected
	 * an IMSI.
	 * @param IMSI the IMSI being searched on
	 * @return a list of the cause codes and event id's affecting that IMSI
	 */
	@SuppressWarnings("unchecked")
	public List<BaseData> getCauseCodesByIMSI(final String IMSI) {
		final Query query = entityManager
				.createQuery("Select DISTINCT i.imsi, i.causeCode, i.eventId, i.eventAndCause.description FROM BaseData AS i WHERE i.imsi LIKE ?1");
		query.setParameter(1, IMSI);
		return query.getResultList();
	}

	/**
	 * This method is used to get all the distinct IMSI's to populate 
	 * a drop down box used by some of the queries
	 * @return list of all distinct IMSI's
	 */
	@SuppressWarnings("unchecked")
	public List<BaseData> getDistinctIMSIListForDropdown() {
		final Query query = entityManager.createQuery("Select DISTINCT e.imsi FROM BaseData e");
		return query.getResultList();
	}

	/**
	 * This method is used to get all the cause codes and event id's that
	 * have affected a defined model of phone
	 * @param model the model of phone being searched for
	 * @return list of all cause codes/event id's that have affected that model of phone
	 */
	@SuppressWarnings("unchecked")
	public List<BaseData> getFailuresPerDeviceModel(final String model) {
		final Query query = entityManager
				.createQuery("Select b.uEType.model as model, b.eventId, b.causeCode, b.eventAndCause.description, count(b.callFailure_ID) "
						+ "as total_failures from BaseData b where b.uEType.model LIKE ?1 "
						+ "group by b.eventId, b.causeCode order by total_failures desc");
		query.setParameter(1, model);
		System.out.println("RESULT OF FAILURE PER DEVICE : " + query.getResultList().size());
		return query.getResultList();
	}



	/**
	 * This method is used to get the unique IMSI's for a model of phone
	 * and event id/cause code combination for use in a 
	 * graph for drill down functionality 
	 * @param model the model of phone
	 * @param description the event id/cause code description
	 * @return list of all unique IMSI's with this model of phone and event id/cause code
	 */
	@SuppressWarnings("unchecked")
	public List<BaseData> getUniqueFailuresPerDeviceModelAndEvent(final String model, final String description) {
		final Query query = entityManager
				.createQuery("Select  b.imsi, count(b.callFailure_ID) "
						+ "as total_failures_per_imsi from BaseData b where b.uEType.model LIKE ?1 and  b.eventAndCause.description LIKE ?2 "
						+ "group by b.eventId, b.causeCode, b.imsi order by total_failures_per_imsi desc");
		query.setParameter(1, model);
		query.setParameter(2, description);
		return query.getResultList();
	}

	/**
	 * This method is used to get all the phone manufacturers from the user equipment type
	 * table in the database.
	 * @return list of all unique phone manufacturers
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<String> getListOfManufacturersFromUeTableViaBaseData() {
		final Query query = entityManager
				.createQuery("SELECT DISTINCT u.manufacturer from UeData u ORDER BY u.manufacturer ASC");
		final List resultList = query.getResultList();
		return resultList;
	}

	//	/**
	//	 * This method gets the number of distinct IMSI failures for a given failure
	//	 * class
	//	 * @param failureClass
	//	 * @return number of distinct failures for the failure class
	//	 */
	//	public Long getPhoneModelCount(final String model) {
	//		final TypedQuery<Long> query = entityManager.createQuery("select count(distinct b.imsi) from "
	//				+ "BaseData b where b.failureClass = " + model, Long.class);
	//		final long count = query.getSingleResult();
	//		return count;
	//	}

	/**
	 * This method is used to get all the models of phone that a defined manufacturer has.
	 * @param manufacturer the phone manufacturer being searched for
	 * @return list of all models of phone for that manufacturer
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<String> getListOfHandsetsByManufacturers(final String manufacturer) {
		final Query query = entityManager
				.createQuery("SELECT DISTINCT u.marketingName from UeData u where u.manufacturer='" + manufacturer
						+ "'");
		System.out.println(query.getFirstResult());
		final List resultList = query.getResultList();
		return resultList;
	}

	/**
	 * This method is used to get the number of failures that a phone has 
	 * has during a defined period of time
	 * @param manufacturerAndHandsetAndDate a string that is separated by ampersands that 
	 * includes the manufacturer,handset and date being searched on
	 * @return list of rows in database that are returned from this search
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<BaseData> getListOfErrorsByHandsetAndDate(final String manufacturerAndHandsetAndDate) {
		final String[] parseAllInfo = manufacturerAndHandsetAndDate.split("&");
		for (int i = 0; i < parseAllInfo.length; i++) {
			System.out.println(i + " " + parseAllInfo[i]);
		}
		final String minYear = parseAllInfo[4] + "-" + parseAllInfo[2] + "-" + parseAllInfo[3];
		final String minTime = parseAllInfo[5] + ":" + parseAllInfo[6] + ":00";
		final String min = minYear + " " + minTime;

		final String maxYear = parseAllInfo[9] + "-" + parseAllInfo[7] + "-" + parseAllInfo[8];
		final String maxTime = parseAllInfo[10] + ":" + parseAllInfo[11] + ":00";
		final String max = maxYear + " " + maxTime;
		final String manufacturer = parseAllInfo[0];
		final String handset = parseAllInfo[1];
		final Query query = entityManager.createQuery("SELECT b FROM BaseData b WHERE b.ueType IN "
				+ "(SELECT u.tac FROM UeData u WHERE u.manufacturer='" + manufacturer + "' AND u.marketingName='"
				+ handset + "' AND b.date between" + " '" + min + "' and '" + max + "')");

		final List countOfErrorsByHandset = query.getResultList();
		return countOfErrorsByHandset;
	}

	/**
	 * This method is used to query the number of failures associated with the
	 * provided IMSI over the specified date/time period. The date and time
	 * period and IMSI are passed into the method as one String and are
	 * seperated out to build the JPA query.
	 * 
	 * @param imsiAndDate the date and time period and IMSI String being searched for
	 * @return the number of failures for that IMSI during that time period
	 */
	@SuppressWarnings("unchecked")
	public List<BaseData> getFailuresPerImsi(final String imsiAndDate) {
		final String[] parseDateAndImsiInfo = imsiAndDate.split("&");
		final String[] parseDateInfo = new String[10];
		final String imsi = parseDateAndImsiInfo[10];
		for (int i = 0; i < parseDateAndImsiInfo.length - 1; i++) {
			parseDateInfo[i] = parseDateAndImsiInfo[i];
		}
		final String min = getMinParsed(parseDateInfo);
		final String max = getMaxParsed(parseDateInfo);
		final Query query = entityManager.createQuery("select b.imsi, count(b.callFailure_ID) "
				+ "from BaseData b where b.imsi=" + imsi + "and b.date between '" + min + "' and '" + max + "'");
		return query.getResultList();
	}

	/**
	 * This method is used to get the top 10 cells with the most failures 
	 * @return list of the top 10 worst cells in database
	 */
	@SuppressWarnings("unchecked")
	public List<BaseData> getTop10FailCells() {
		final Query query = entityManager
				.createNativeQuery("select op.country, op.operator, cell_id, count(cell_id) as total_per_cell, "
						+ "ROUND((count(cell_id)/b2.counter * 100),2 )as percentage_failure_per_operator  "
						+ "from base_data b "
						+ "join (select operator, count(*) as counter from base_data group by market, operator ) b2 "
						+ "on b2.operator = b.operator "
						+ "join mcc_mcn_table op on (b.market = op.mcc and b.operator = op.mnc) "
						+ "group by market, b.operator, cell_id order by total_per_cell desc");
		query.setMaxResults(10);
		return query.getResultList();
	}


	/**
	 * This method is used to get the top 10 cells with the most failures 
	 * @param dateInfo the date and time being searched on
	 * @return list of the top 10 worst cells in database
	 */
	@SuppressWarnings("unchecked")
	public List<BaseData> getTop10FailuresWithDate(final String dateInfo) {
		final String[] parseDateInfo = dateInfo.split("&");
		final String min = getMinParsed(parseDateInfo);
		System.out.println("FROM ---->" + min);
		final String max = getMaxParsed(parseDateInfo);
		System.out.println("TO-------->" + max);
		final Query query = entityManager
				.createQuery("Select b.marketOperator.country, b.marketOperator.operator, b.cellId, count(*) "
						+ "as total_failures from BaseData b " + "WHERE b.date " + "BETWEEN '" + min + "' " + "AND '"
						+ max + "' " + "group by b.marketOperator.country, b.marketOperator.operator, b.cellId "
						+ "order by count(*) desc");
		return query.setMaxResults(10).getResultList();
	}
	
	/* Story 11 level 2 drill down original query */
	@SuppressWarnings("unchecked")
	public List<BaseData> getImsiPerMarketOperatorPerCellPerDate(final String dateAndCellAndOperator) {
		
		String min = "";
		String max = "";
		String operator = dateAndCellAndOperator.substring(4);
		String cellId = dateAndCellAndOperator.substring(2);
		String updatedQuery = "";
		if (!dateAndCellAndOperator.substring(0, 1).equals("_")) {
			final String[] parseDateAndMarket = dateAndCellAndOperator.split("&");
			final String[] parseDateInfo = new String[12];
			cellId = parseDateAndMarket[10];
			operator = parseDateAndMarket[11];
			for (int i = 0; i < parseDateAndMarket.length - 1; i++) {
				parseDateInfo[i] = parseDateAndMarket[i];
			}
			min = getMinParsed(parseDateInfo);
			max = getMaxParsed(parseDateInfo);
			updatedQuery = " AND b.date" + " BETWEEN '" + min + "' " + "AND '" + max + "' ";
		}
		final Query query = entityManager.createQuery("Select  b.imsi,  count(*) "
				+ "as total_failures from BaseData b  where b.marketOperator.operator LIKE  ?1 and"
				+ " b.cellId = " + cellId
				+ updatedQuery + "Group by b.imsi order by count(*) desc");
		query.setParameter(1, "%"+operator+"%");
		return query.getResultList();
	}
	

	/**
	 * This method is used to query the IMSI's in the database that are
	 * associated with the failure class provided. Server side pagination is used by this query 
	 * as the result set can be quite large
	 *
	 * @param start the index of the first row of that page of the data table
	 * @param amount the amount of rows being shown by that page of the data table
	 * @param columnName the name of the column being sorted on in data table
	 * @param searchTerm the term being searched on in the search field of the data table
	 * @param sortDir the sorting applied to the data table
	 * @param failureClass the failure class being searched for
	 * @return list of the IMSI's affected by the provided failure class
	 */
	@SuppressWarnings("unchecked")
	public List<BaseData> getIMSIsPerFailureClass(final int start, final int amount, final String colName,
			final String searchTerm, final String sortDir, final String failureClass) {

		String query = "select distinct b.imsi,b.failureClass1.description from BaseData as b where b.failureClass = "
				+ failureClass + " ORDER BY b." + colName + " " + sortDir;

		if (!("").equals(searchTerm)) {
			final String search = " WHERE (b.imsi LIKE '%" + searchTerm + "%') and b.failureClass = " + failureClass;
			query = "SELECT distinct b.imsi,b.failureClass1.description FROM BaseData b " + search
					+ " ORDER BY b." + colName + " " + sortDir;
		}
		System.out.println("QUERY: " + query);
		final Query queryToExecute = entityManager.createQuery(query);
		queryToExecute.setFirstResult(start);
		queryToExecute.setMaxResults(amount);
		System.out.println("LIST SIZE----------->" + queryToExecute.getResultList().size());
		return queryToExecute.getResultList();
	}

	/**
	 * This method gets the number of distinct IMSI failures for a given failure
	 * class to be used for setting the size of the data table for server side 
	 * pagination
	 * 
	 * @param failureClass the failure class being searched on
	 * @return number of distinct failures for the failure class
	 */
	public Long getCount(final String failureClass) {
		final TypedQuery<Long> query = entityManager.createQuery("select count(distinct b.imsi) from "
				+ "BaseData b where b.failureClass = "+failureClass,Long.class);

		System.out.println("COUNT------------>" + query.getSingleResult());
		final long count = query.getSingleResult();
		return count;
	}

	/**
	 * This method gets the number of failures for a given time period
	 * to be used for setting the size of the data table for server side pagination
	 * @param dateInfo the date and time period being searched on
	 * @return number of failures that have occured during that time period
	 */
	@SuppressWarnings("unchecked")
	public Long getCountStory7(final String dateInfo) {
		final String[] parseDateInfo = dateInfo.split("&");
		final String min = getMinParsed(parseDateInfo);
		final String max = getMaxParsed(parseDateInfo);
		final TypedQuery<Long> query = (TypedQuery<Long>) entityManager.createQuery("Select count(d) from BaseData d where d.date between" + " '" + min
				+ "' and '" + max + "'");

		System.out.println("COUNT------------>" + query.getSingleResult());
		final long count = query.getSingleResult();
		return count;
	}

	/**
	 * This method is used to get all the failure classes from the database.
	 * 
	 * @return list of all failure classes
	 */
	@SuppressWarnings("unchecked")
	public List<BaseData> getAllFailureClasses() {
		final Query query = entityManager.createQuery("select f.failureClass,f.description from FailureClass f");
		return query.getResultList();
	}

	/**
	 * This method is used to get top 10 IMSI;s with failures in a given
	 * time period and sort by IMSI with most failures.
	 * 
	 * @param dateInfo the date and time period being searched on
	 * @return list of the top 10 IMSI's with failures.
	 */
	@SuppressWarnings("unchecked")
	public List<BaseData> getTop10ImsisWithFailuresTimePeriod(final String dateInfo) {
		final String[] parseDateInfo = dateInfo.split("&");
		final String min = getMinParsed(parseDateInfo);
		final String max = getMaxParsed(parseDateInfo);
		final Query queryTop10ImsisWithFailuresOverTimePeriod = entityManager
				.createQuery("SELECT DISTINCT d.imsi, COUNT(d.imsi) " + "FROM BaseData AS d " + "WHERE d.date "
						+ "BETWEEN '" + min + "' " + "AND '" + max + "' "
						+ "GROUP BY d.imsi ORDER BY COUNT(d.imsi) DESC, d.imsi ASC");
		queryTop10ImsisWithFailuresOverTimePeriod.setMaxResults(10);
		return queryTop10ImsisWithFailuresOverTimePeriod.getResultList();

	}

	/**
	 * This method is used to get the number of event id and cause code errors
	 * that have affected a given IMSI during a defined time period
	 * @param dateAndImsi the date and time period and IMSI being searched on
	 * @return list of all event id and cause code descriptions and their count for the IMSI
	 * and time period
	 */
	@SuppressWarnings("unchecked")
	public List<BaseData> getfailureCountPerImsiPerDateGroupByEvent(final String dateAndImsi) {
		final String[] parseDateAndImsiInfo = dateAndImsi.split("&");
		final String[] parseDateInfo = new String[10];
		final String imsi = parseDateAndImsiInfo[10];
		for (int i = 0; i < parseDateAndImsiInfo.length - 1; i++) {
			parseDateInfo[i] = parseDateAndImsiInfo[i];
		}
		final String min = getMinParsed(parseDateInfo);
		final String max = getMaxParsed(parseDateInfo);
		final Query query = entityManager.createQuery("select b.eventAndCause.description, count(b.callFailure_ID) "
				+ "from BaseData b where b.imsi=" + imsi + "and b.date between '" + min + "' and '" + max + "'"
				+ " Group by b.eventAndCause.description");
		return query.getResultList();
	}


	/**
	 * This method retrieves each failure for a given IMSI and the date and time 
	 * the failure occurred with its duration
	 * @param datesAndIMSI the date and time period and IMSI being searched on
	 * @return list of the individual failures for the IMSI
	 */
	@SuppressWarnings("unchecked")
	public List<BaseData> getFailuresPerIMSIForDrilldown(final String datesAndIMSI){
		final String[] parseDateAndIMSIInfo = datesAndIMSI.split("&");
		final String[] parseDateInfo = new String[10];
		final String imsi = parseDateAndIMSIInfo[10];
		for (int i = 0; i < parseDateAndIMSIInfo.length - 1; i++) {
			parseDateInfo[i] = parseDateAndIMSIInfo[i];
		}
		final String min = getMinParsed(parseDateInfo);
		final String max = getMaxParsed(parseDateInfo);
		final Query failuresPerIMSIForDrilldown = entityManager
				.createQuery("select b.date,b.duration from BaseData b where"
						+ " b.imsi = "+imsi+" and b.date between '" + min + "' " + " and '" + max + "'");
		return failuresPerIMSIForDrilldown.getResultList();
	}


	/***
	 * Story 11 and Story 13 Level 1(Initial level) Drill down - Top 10 worst Markets per given time period.
	 * Story 11 is with date parameters. Story 13 does not have date parameters
	 * */

	/**
	 * This method is used to get the top 10 worst markets in the database for
	 * a defined period of time
	 * @param dateInfo dateInfo the date and time period being searched on
	 * @return list of the top 10 worst markets during that time period
	 */
	@SuppressWarnings("unchecked")
	public List<BaseData> getTop10WorstMarketsList(String dateInfo) {
		String min = "";
		String max = "";
		String dateExpression = "";
		if (!dateInfo.substring(0, 1).equals("_")) {
			final String[] parseDateInfo = dateInfo.split("&");
			min = getMinParsed(parseDateInfo);
			System.out.println("FROM ---->" + min);
			max = getMaxParsed(parseDateInfo);
			System.out.println("TO-------->" + max);

			dateExpression = " WHERE b.date " + "BETWEEN '" + min + "' " + "AND '" + max + "' ";

		}
		final Query query = entityManager.createQuery("Select b.marketOperator.country,  count(*) "
				+ "as total_failures from BaseData b " + dateExpression + "group by b.marketOperator.country "
				+ "order by count(*) desc");
		return query.setMaxResults(10).getResultList();
	}


	/***
	 * Story 11 and Story 13 Level 2 Drill down - Top 10 worst Operators per Market per given
	 * time period. Story 11 is with date parameters. Story 13 does not have date parameters
	 * */

	/**
	 * This method is used to get the top 10 worst operators within a given
	 * market in the database for a defined period of time
	 * @param dateAndMarket the date and time period and market being searched on
	 * @return list of the top 10 worst operators during that time period and for that market
	 */
	@SuppressWarnings("unchecked")
	public List<BaseData> getTop10WorstOperatorsList(String dateAndMarket) {
		String min = "";
		String max = "";
		String market = dateAndMarket.substring(2);
		String updatedQuery = "";
		if (!dateAndMarket.substring(0, 1).equals("_")) {
			final String[] parseDateAndMarket = dateAndMarket.split("&");
			final String[] parseDateInfo = new String[10];
			market = parseDateAndMarket[10];
			for (int i = 0; i < parseDateAndMarket.length - 1; i++) {
				parseDateInfo[i] = parseDateAndMarket[i];
			}
			min = getMinParsed(parseDateInfo);
			max = getMaxParsed(parseDateInfo);
			updatedQuery = " AND b.date" + " BETWEEN '" + min + "' " + "AND '" + max + "' ";
		}
		final Query query = entityManager.createQuery("Select  b.marketOperator.operator,  count(*) "
				+ "as total_failures, b.cellId from BaseData b  where b.marketOperator.country LIKE  ?1  "
				+ updatedQuery + "Group by b.marketOperator order by count(*) desc");
		query.setParameter(1, market);
		return query.setMaxResults(10).getResultList();

	}

	/**
	 * Story 11  and Story 13 Level 3 Drill down for graph Top 10 Worst Cells Per Operator by
	 * Story 11 is with date parameters. Story 13 does not have date parameters
	 * 
	 * 
	 * This method is used to get the top 10 worst cells in the database 
	 * a defined period of time and operator
	 * @param dateAndOperator dateInfo the date and time period and operator being searched on
	 * @return list of the top 10 worst cells during that time period and for that operator
	 */
	@SuppressWarnings("unchecked")
	public List<BaseData> getTop10WorstCellsList(String dateAndOperator) {
		String min = "";
		String max = "";
		String operator = dateAndOperator.substring(2);
		String updatedQuery = "";
		if (!dateAndOperator.substring(0, 1).equals("_")) {
			final String[] parseDateAndMarket = dateAndOperator.split("&");
			final String[] parseDateInfo = new String[11];
			operator = parseDateAndMarket[10]+'&'+parseDateAndMarket[11];
			for (int i = 0; i < parseDateAndMarket.length - 1; i++) {
				parseDateInfo[i] = parseDateAndMarket[i];
			}
			min = getMinParsed(parseDateInfo);
			max = getMaxParsed(parseDateInfo);
			updatedQuery = " AND b.date" + " BETWEEN '" + min + "' " + "AND '" + max + "' ";
		}
		final Query query = entityManager.createQuery("Select  b.cellId,  count(*) "
				+ "as total_failures, b.cellId from BaseData b  where b.marketOperator.operator LIKE  ?1 "
				+ updatedQuery + "Group by b.cellId order by count(*) desc");
		query.setParameter(1, "%"+operator+"%");
		return query.setMaxResults(10).getResultList();
	}

}
