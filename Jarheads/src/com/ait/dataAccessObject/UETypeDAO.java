package com.ait.dataAccessObject;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.ait.models.UEType;


@Stateless
@LocalBean
public class UETypeDAO {
	@PersistenceContext(name = "testpoi")
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<UEType> getAllUETypes() {
		final Query query = entityManager.createQuery("Select u.manufacturer, u.model from UEType u");
		return query.getResultList();
	}

}
