package com.ait.dataAccessObject;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.ait.models.User;

@Stateless
@LocalBean
public class UserDAO {
	@PersistenceContext(name = "testpoi")
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<User> getAllUsers() {
		final Query query = entityManager.createQuery("Select u from User u");
		return query.getResultList();
	}
	
	public User getUserById(final int userId){
		System.out.println("GET USER BY ID: DAO");
		return entityManager.find(User.class,userId);
	}

	public User findUserByUserNameAndPassword(final String userName,final String password) 
	throws NoResultException {
		
			System.out.println("Inside findUser..");
			final Query query = entityManager
					.createQuery("Select u from User u where u.userName like ?1 and u.password like ?2");
			query.setParameter(1, userName);
			query.setParameter(2, password);
			return (User) query.getSingleResult();
	}

	public void save(final User user) {
		entityManager.persist(user);
	}

	public User getUserByName(final String username) {
		final Query query = entityManager.createQuery("select u from User u where u.userName = :theUsername");
		query.setParameter("theUsername", username);
		return (User) query.getSingleResult();
	}

	public void update(final User user) {
		entityManager.merge(user);
	}
	
	public void delete(final int userId){
		entityManager.remove(getUserById(userId));

	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	

}
