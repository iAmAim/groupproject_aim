package com.ait.pagination;


import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.ait.models.BaseData;

/**
 * This class is used for server side pagination
 */
@Stateless
@LocalBean
public class PaginationClassDataTable {
	
	private Integer sEcho;
	private String iTotalRecords;
	private String iTotalDisplayRecords;
	private List<BaseData> aaData;
	
	@PersistenceContext
	public EntityManager emPagination;

	public Integer getsEcho() {
		return sEcho;
	}

	public void setsEcho(final Integer sEcho) {
		this.sEcho = sEcho;
	}

	public String getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(final String iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public String getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(final String iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public List<BaseData> getAaData() {
		return aaData;
	}

	public void setAaData(final List<BaseData> aaData) {
		this.aaData = aaData;
	}

}

