
 package com.ait.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name ="ue_table")

public class UeData {
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TAC")
	private int tac;
	@Column(name="marketing_name")
	private String marketingName;
	@Column(name="manufacturer")
	private String manufacturer;
	@Column(name="access_capability")
	private String accessCapabilities;
	@Column(name="model")
	private String model;
	@Column(name="vendor_name")
	private String vendorName;
	public int getTac() {
		return tac;
	}
	public void setTac(final int tac) {
		this.tac = tac;
	}
	public String getMarketingName() {
		return marketingName;
	}
	public void setMarketingName(final String marketingName) {
		this.marketingName = marketingName;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(final String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getAccessCapabilities() {
		return accessCapabilities;
	}
	public void setAccessCapabilities(final String accessCapabilities) {
		this.accessCapabilities = accessCapabilities;
	}
	public String getModel() {
		return model;
	}
	public void setModel(final String model) {
		this.model = model;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(final String vendorName) {
		this.vendorName = vendorName;
	}
	@Override
	public String toString() {
		return "UeData [tac=" + tac + ", marketingName=" + marketingName
				+ ", manufacturer=" + manufacturer + ", accessCapabilities="
				+ accessCapabilities + ", model=" + model + ", vendorName="
				+ vendorName + "]";
	}
}