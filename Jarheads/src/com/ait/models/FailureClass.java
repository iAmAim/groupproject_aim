package com.ait.models;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name = "failure_class_table")
public class FailureClass { 

	@Id
	@Column(name = "failure_Class")
	private int failureClass;
	@Column(name = "description")
	private String description;
	
	public int getFailureClass() {
		return failureClass;
	}
	public void setFailureClass(final int failureClass) {
		this.failureClass = failureClass;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(final String description) {
		this.description = description;
	}
	
	@OneToMany(mappedBy = "failureClass1")
	private List<BaseData> baseData;
}
