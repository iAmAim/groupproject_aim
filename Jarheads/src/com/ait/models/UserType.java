package com.ait.models;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name = "user_type")
public class UserType {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@NotNull
	@Column(name = "id")
	private int userTypeId;
	private String name;

	public int getUserTypeId() {
		return userTypeId;
	}

	public void setUserTypeId(final int userTypeId) {
		this.userTypeId = userTypeId;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "userType")
	private Collection<User> userList;

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void login() {

	}

	public void logout() {

	}
	// public Collection<User> getUserList() {
	// return userList;
	// }
	// public void setUserList(final Collection<User> userList) {
	// this.userList = userList;
	// }

}
