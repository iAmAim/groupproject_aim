package com.ait.models;


import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@XmlRootElement
@Table(name = "ue_table")
public class UEType {

	@Id
	private int tac;
	@Column(name = "marketing_name")
	private String marketingName;
	private String manufacturer;
	@Column(name = "access_capability")
	private String accessCapability;
	private String model;
	@Column(name = "vendor_name")
	private String vendorName;
	@Column(name = "ue_type")
	private String type;
	@Column(name = "os")
	private String operatingSystem;
	@Column(name = "input_mode")
	private String inputMode;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "uEType")
	private Collection<BaseData> baseDataList;

	public int getTac() {
		return tac;
	}

	public void setTac(final int tac) {
		this.tac = tac;
	}

	public String getMarketingName() {
		return marketingName;
	}

	public void setMarketingName(final String marketingName) {
		this.marketingName = marketingName;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(final String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getAccessCapability() {
		return accessCapability;
	}

	public void setAccessCapability(final String accessCapability) {
		this.accessCapability = accessCapability;
	}

	public String getModel() {
		return model;
	}

	public void setModel(final String model) {
		this.model = model;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(final String vendorName) {
		this.vendorName = vendorName;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getOperatingSystem() {
		return operatingSystem;
	}

	public void setOperatingSystem(final String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}

	public String getInputMode() {
		return inputMode;
	}

	public void setInputMode(final String inputMode) {
		this.inputMode = inputMode;
	}

}
