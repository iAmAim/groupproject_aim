package com.ait.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@XmlRootElement
@Table(name = "event_cause_data")
public class EventAndCause implements Serializable{ 

	private static final long serialVersionUID = 1L;

	@EmbeddedId 
	private EventAndCausePK eventAndCausePK;
	
	private String description;

	public EventAndCausePK getEventAndCausePK() {
		return eventAndCausePK;
	}
	public void setEventAndCausePK(final EventAndCausePK eventAndCauseid) {
		this.eventAndCausePK = eventAndCauseid;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(final String description) {
		this.description = description;
	}

	
	@OneToMany(mappedBy = "eventAndCause")
    private List<BaseData> baseData;
}


@Embeddable
class EventAndCausePK implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name="cause_Code",insertable = false, updatable = false)
    private int causeCode;
	@Column(name="event_ID",insertable = false, updatable = false)
    private int eventId;
	
	public int getCauseCode() {
		return causeCode;
	}
	public void setCauseCode(final int causeCode) {
		this.causeCode = causeCode;
	}
	public int getEventId() {
		return eventId;
	}
	public void setEventId(final int eventId) {
		this.eventId = eventId;
	}
}