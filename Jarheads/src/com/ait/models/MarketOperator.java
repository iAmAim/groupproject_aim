package com.ait.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@XmlRootElement
@Table(name = "mcc_mcn_table")
public class MarketOperator implements Serializable{ 

	private static final long serialVersionUID = 1L;

	@EmbeddedId 
	private MarketOperatorPK marketOperatorPK;
	
	private String country;
	private String operator;

	@OneToMany(mappedBy = "marketOperator")
    private List<BaseData> baseData;

	public MarketOperatorPK getMarketOperatorPK() {
		return marketOperatorPK;
	}

	public void setMarketOperatorPK(MarketOperatorPK marketOperatorPK) {
		this.marketOperatorPK = marketOperatorPK;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}



	
	
	
	
}


@Embeddable
class MarketOperatorPK implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name="MCC",insertable = false, updatable = false)
    private int mcc;
	@Column(name="MNC",insertable = false, updatable = false)
    private int mnc;
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getMcc() {
		return mcc;
	}
	public int getMnc() {
		return mnc;
	}
	public void setMcc(int mcc) {
		this.mcc = mcc;
	}
	public void setMnc(int mnc) {
		this.mnc = mnc;
	}
	
	
}
