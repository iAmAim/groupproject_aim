package com.ait.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * This class creates an object for each row in the base data table.
 * 
 * 
 * @version 1.0
 */

@Entity
@XmlRootElement
@Table(name = "base_data")
public class BaseData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int callFailure_ID;

	@Column(name = "date_Time")
	private String date;
	@Column(name = "event_ID")
	private String eventId;
	@Column(name = "failure_Class")
	private String failureClass;
	@Column(name = "UE_Type")
	private String ueType;
	private String market;
	private String operator;
	@Column(name = "cell_ID")
	private String cellId;
	private String duration;
	@Column(name = "cause_Code")
	private String causeCode;
	@Column(name = "NE_Version")
	private String neVersion;
	@Column(name = "IMSI")
	private String imsi;
	@Column(name = "HIER3_ID")
	private String hier3id;
	@Column(name = "HIER32_ID")
	private String hier32id;
	@Column(name = "HIER321_ID")
	private String hier321id;
	
	
	@ManyToOne(optional = false, targetEntity = UEType.class)
	@JoinColumn(name = "ue_type", referencedColumnName = "tac")
	private UEType uEType;
	
	public void setuEType(final UEType uEType) {
		this.uEType = uEType;
	}
	
	public UEType getuEType() {
		return uEType;
	}
	
	@ManyToOne(optional = false, targetEntity = FailureClass.class)
	@JoinColumn(name = "failure_Class", referencedColumnName = "failure_Class",insertable=false, updatable=false)
	private FailureClass failureClass1;
	
	public void setFailureClass1(final FailureClass failureClass) {
		this.failureClass1 = failureClass;
	}
	
	public FailureClass getFailureClass1() {
		return failureClass1;
	}

	@MapsId(value = "eventAndCausePK")
	@ManyToOne( targetEntity = EventAndCause.class)
	 @JoinColumns(value = {
			 @JoinColumn(name = "event_ID", referencedColumnName = "event_ID"),
			 @JoinColumn(name = "cause_Code", referencedColumnName = "cause_Code") })
	private EventAndCause eventAndCause;
	
	public EventAndCause getEventAndCause() {
		return eventAndCause;
	}

	public void setEventAndCause(final EventAndCause eventAndCause) {
		this.eventAndCause = eventAndCause;
	}
	
	
	@MapsId(value = "marketOperatorPK")
	@ManyToOne( targetEntity = MarketOperator.class)
	 @JoinColumns(value = {
			 @JoinColumn(name = "market", referencedColumnName = "MCC"),
			 @JoinColumn(name = "operator", referencedColumnName = "MNC") })
	private MarketOperator marketOperator;
	
	public MarketOperator getMarketOperator() {
		return marketOperator;
	}

	public void setMarketOperator(final MarketOperator marketOperator) {
		this.marketOperator = marketOperator;
	}

	@Override
	public String toString() {
		String delimeter = ";";
		return date + delimeter + eventId + delimeter + failureClass + delimeter + ueType + delimeter + market
				+ delimeter + operator + delimeter + cellId + delimeter + duration + delimeter + causeCode + delimeter
				+ neVersion + delimeter + imsi + delimeter + hier3id + delimeter + hier32id + delimeter + hier321id
				+ delimeter + "\n";
	}

	public String toStringLog() {
		return date + "," + eventId + "," + failureClass + "," + ueType + "," + market + "," + operator + "," + cellId
				+ "," + duration + "," + causeCode + "," + neVersion + "," + imsi + "," + hier3id + "," + hier32id
				+ "," + hier321id + "," + "\n";
	}

	public String getDate() {
		return date;
	}

	public void setDate(final String date) {
		this.date = date;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(final String eventId) {
		this.eventId = eventId;
	}

	public String getFailureClass() {
		return failureClass;
	}

	public void setFailureClass(final String failureClass) {
		this.failureClass = failureClass;
	}

	public String getUeType() {
		return ueType;
	}

	public void setUeType(final String ueType) {
		this.ueType = ueType;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(final String market) {
		this.market = market;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(final String operator) {
		this.operator = operator;
	}

	public String getCellId() {
		return cellId;
	}

	public void setCellId(final String cellId) {
		this.cellId = cellId;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(final String duration) {
		this.duration = duration;
	}

	public String getCauseCode() {
		return causeCode;
	}

	public void setCauseCode(final String causeCode) {
		this.causeCode = causeCode;
	}

	public String getNeVersion() {
		return neVersion;
	}

	public void setNeVersion(final String neVersion) {
		this.neVersion = neVersion;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(final String imsi) {
		this.imsi = imsi;
	}

	public String getHier3id() {
		return hier3id;
	}

	public void setHier3id(final String hier3id) {
		this.hier3id = hier3id;
	}

	public String getHier32id() {
		return hier32id;
	}

	public void setHier32id(final String hier32id) {
		this.hier32id = hier32id;
	}

	public String getHier321id() {
		return hier321id;
	}

	public void setHier321id(final String hier321id) {
		this.hier321id = hier321id;
	}
}
