package com.ait.importFile;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import com.ait.consistencycheck.ConsistencyCheck;
import com.ait.consistencycheck.ValidDataHashSets;
import com.ait.models.BaseData;

@Stateless
@LocalBean
public class ImportFromExcelToDatabase {

	@PersistenceContext
	public  EntityManager emConvert;
	
	@EJB
	
	ImportEntityManagerFunctions entityFunctions;
	@EJB
	HasFileAlreadyBeenImported checkFileNotDuplicate;
	
	private String csvFilePath="\\Temp\\CSV\\";
	private String errorLogPath="..\\Temp\\ErrorLogs\\Error Log ";
	String base_dir=System.getProperty("user.dir").substring(0,System.getProperty("user.dir").length() - 4);
	private final ConsistencyCheck validation=new ConsistencyCheck();
	private ValidDataHashSets sets=new ValidDataHashSets();
	private boolean runCsvToData=true;
    public String tableName=null;
    public Sheet sheet;
	public final static int BASE_DATA=0;
    public final static int EVENT_CAUSE=1;
    public final static int FAILURE_CLASS=2;
    public final static int UE_TABLE=3;
    public final static int MCC_MCN=4;
    public final static int AMOUNT_OF_SHEETS=5;
    public List<BaseData> erroneousDataList=new ArrayList<>();
    public String returnMesg="";
    public int amountOfCorrectData=0;
    public int amountOfEroneousData=0;
    public boolean manualImport;
    /**
     * This method converts an excel file to a csv file and then inserts the data into a database.
     * @throws  
     */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public String convertFile(final String address,final String fileName,final boolean manualImport) {
    	this.manualImport=manualImport;
		csvFilePath=address + "\\Temp\\CSV\\";
    	amountOfCorrectData=0; amountOfEroneousData=0;
    	for(int sheetNumber=AMOUNT_OF_SHEETS-1;sheetNumber>=0;sheetNumber--){
    		if(sheetNumber==0){
    			erroneousDataList.clear();
    			validation.setHashSets(sets);			
    		}	
    		try {  

				final FileInputStream file = new FileInputStream(new File(fileName));
				final ExcelFileSheetSetup setupSheets = new ExcelFileSheetSetup();
				sheet = setupSheets.excelFileSetup(sheetNumber, file, fileName);
				final Iterator<Row> rowIterator = sheet.iterator();
				final StringBuffer buffer = new StringBuffer();
				final StringBuffer errorBuffer = new StringBuffer();
				errorBuffer.append("date,eventId,failureClass,ueType,market,operator,cellId,duration,causeCode,neVersion,imsi,hier3id,hier32id,hier321id,\n");
				rowIterator.next();
				while (rowIterator.hasNext()) {
					extractDataFromRowAndAssign(sheetNumber, rowIterator,buffer, errorBuffer);
				}
				file.close();
				tableName = getTableName(sheetNumber);
				adjustCsvPathPerThisLoop(address, sheetNumber);
				writeBufferToCSV(getCsvFilePath(), buffer,sheetNumber,errorBuffer);
				csvToDataBase(sheetNumber); 
			} 
    		catch(NullPointerException e){
    			e.printStackTrace(); 
    			return "The file you have selected is missing the required fields";
    		}
    		catch(IllegalArgumentException e){
    			e.printStackTrace();
    			return "The file you have selected does not contain base data records";
    		}
			catch (IOException e) {
				e.printStackTrace();
				return "Incorrect file type";
			} 
		}
    	returnMesg="Import Succesful: There was "+amountOfCorrectData+" "
    			+ "valid data rows and "+amountOfEroneousData+" "
    			+ "erroneous rows.";
		return returnMesg;
	}
	public String autoImport(){
		System.out.println("AUTO DIR TEST "+base_dir);
		File[] files;
		final File dir = new File(base_dir+"\\Temp\\AutoImport");
		files = dir.listFiles();
		System.out.println("FILES SIZE "+files.length);
		int[] numbersFromAuto=new int[2];
		String message = null;
		int importCounter=0;
		for (final File file : files) {
			if (!file.isDirectory()){
				String ext = FilenameUtils.getExtension(file.getAbsolutePath());
				System.out.println("EXT "+ext);
				if(!ext.equals("xls")){
					System.out.println("In Delete");
					file.delete();
					continue;
				}
				boolean checkFile=checkFileHasntAlreadyBeenImported(file);
				if(checkFile && runCsvToData){
					continue;
				}
				message=convertFile(dir.getPath(), file.getPath(),false);
				file.delete();
				String str = message.replaceAll("[^0-9]+", "&");
				str=str.substring(1, str.length()-1);
				final String[] numbersFromAutoIndividual=str.split("&");
				numbersFromAuto[0]+=Integer.parseInt(numbersFromAutoIndividual[0]);
				numbersFromAuto[1]+=Integer.parseInt(numbersFromAutoIndividual[1]);
				importCounter++;
				if(importCounter>3){
					return "Auto Import success. "+numbersFromAuto[0]+" succesfull records "+numbersFromAuto[1]+" erroneous records";
				}
//				try {
//					Files.copy(file.toPath(), dirCopy.toPath());
//					file.delete();
//				} catch (IOException e) {
//					file.delete();
//				}
			}
	    }
		message="Auto Import success. "+numbersFromAuto[0]+" succesfull records "+numbersFromAuto[1]+" erroneous records";
		if(numbersFromAuto[0] == 0 && numbersFromAuto[1] == 0){
			return "empty";
		}
		System.out.println(message);
		return message;
	}
	public boolean checkFileHasntAlreadyBeenImported(File file){
		try {
			 System.out.println("FILEMANE TO CHECK "+file.getAbsolutePath());
			 if(runCsvToData){
				 if (!checkFileNotDuplicate.hasTheFileBeenImportedBefore(file.getAbsolutePath())) {
						file.delete();
						return true;
				 }
			 }
		 } 
		 catch (IOException e1) {
			 return false;
		 }
		return false;
	}
	private void extractDataFromRowAndAssign(final int sheetNumber,
			final Iterator<Row> rowIterator, final StringBuffer buffer,
			final StringBuffer errorBuffer) {
		final Row row = rowIterator.next();
		final BaseData recordObject = new BaseData();
		decipherSheetNumberAndExtractData(sheetNumber, buffer, row, recordObject,errorBuffer);
	}

	private void csvToDataBase(final int sheetNumber) {
		if (sheetNumber != 0 && runCsvToData) {
			entityFunctions.setUpForImortFromExcelToDatabaseFunctions(tableName,csvFilePath);
			entityFunctions.fillDatabaseWithCSVusingPersitenceJPA();
		}
		else if(runCsvToData) {
			entityFunctions.setUpForImortFromExcelToDatabaseFunctions(tableName,csvFilePath);
			entityFunctions.fillDatabaseWithCSVusingPersitenceJPABase(manualImport);
		}
	}

	/**
	 * This method writes the contents of the buffer to a csv file. The sheet
	 * number is used to give the file a distinct name relating its contents to
	 * a specific table.
	 * 
	 * @param sheetNumber
	 *            = sheetNumber the excel sheet number (tabs on bottom)
	 * @param buffer
	 *            = The string buffer which is being built up to write to csv
	 *            file
	 * @throws IOException
	 */
	public void writeBufferToCSV(final String csvFilePath, final StringBuffer buffer,final int fileType,final StringBuffer errorBuffer) throws IOException {
		if(fileType!=0){
			final BufferedWriter bwr = new BufferedWriter(new FileWriter(new File(csvFilePath)));
			bwr.write(buffer.toString());
			bwr.flush();
			bwr.close(); 
		}
		else if(fileType==0){
			final SimpleDateFormat formatter=new SimpleDateFormat("dd_MM_YYYY_h_mm_ss");
			final String date=formatter.format(new Date());
			final BufferedWriter bwrData = new BufferedWriter(new FileWriter(new File(csvFilePath)));
			bwrData.write(buffer.toString());
			bwrData.flush();
			bwrData.close(); 
			final BufferedWriter bwrError = new BufferedWriter(new FileWriter(new File(errorLogPath+date+".csv")));
			bwrError.write(errorBuffer.toString());
			bwrError.flush();
			bwrError.close();
		}
	}

	/**
	 * This method takes in a row and does one of two things. If the row is in
	 * the Base Data sheet the contents are converted to an object using the
	 * storeRowDataInRecordObjectBaseData method The object is then passed to
	 * validation, if valid the Record object's toString is added to the buffer.
	 * If the row is not in the base data table the contents are written
	 * straight to the buffer without validation using the
	 * fillBufferWithDataForTableNonBaseData method.
	 * 
	 * @param sheetNumber
	 *            = sheetNumber the excel sheet number (tabs on bottom)
	 * @param buffer
	 *            = The string buffer which is being built up to write to csv
	 *            file.
	 * @param row
	 *            = The current row being iterated over.
	 * @param recordObject
	 *            = A record object to be filled with data if row is in the base
	 *            data table.
	 */
	public void decipherSheetNumberAndExtractData(final int sheetNumber, final StringBuffer buffer, final Row row,
			final BaseData recordObject, final StringBuffer errorBuffer) {
		switch (sheetNumber) {
			case BASE_DATA: {
				storeRowDataInRecordObjectBaseData(recordObject, row);
				if (validation.checkRecordIsValid(recordObject)) {
					formatRecordDate(recordObject);
					buffer.append(recordObject.toString());
					amountOfCorrectData++;
				}
				else {
					errorBuffer.append(recordObject.toStringLog());
					amountOfEroneousData++;
				}
				break;
			}
			default:
				fillBufferWithDataForTableNonBaseData(buffer, row, sheetNumber);
				break;
		}
	} 

	/**
	 * Sets the format of the BaseData call failure date to Mysql date format
	 * 
	 * @param record
	 *            = Base Data record with unformatted date
	 */
	public void formatRecordDate(final BaseData record) {
		final DateFormat europeanDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		final String rawDateString = record.getDate();

		try {
			final Date europeanDate = europeanDateFormat.parse(rawDateString);
			final DateFormat mySqlDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			record.setDate(mySqlDateFormat.format(europeanDate));
		}
		catch (ParseException e) {
			System.out.println("This date is unparsable");
		}
	}

	/**
	 * This method takes in a sheet number and returns the associated table name
	 * to use for the JPA insert query.
	 * 
	 * @param sheetNumber
	 *            = the excel sheet number (tabs on bottom)
	 * @return
	 */
	public String getTableName(final int sheetNumber) {
		switch (sheetNumber) {
			case BASE_DATA:
				return "base_data";
			case EVENT_CAUSE:
				return "event_cause_data";
			case FAILURE_CLASS:
				return "failure_class_table";
			case UE_TABLE:
				return "ue_table";
			case MCC_MCN:
				return "mcc_mcn_table";
			default:
				return null;
		}
	}

	/**
	 * This method iterates through the cells contained in a row. It appends the
	 * contents of each cell to the buffer. The semicolon is added after each
	 * cell as a delimiter for the JPA query. This is also the reasoning for the
	 * addition of "\n" which tells the JPA query that it has reached the end of
	 * a row.
	 * 
	 * @param buffer
	 *            = The string buffer which is being built up to write to csv
	 *            file.
	 * @param row
	 *            = The current row being iterated over.
	 */
	public void fillBufferWithDataForTableNonBaseData(final StringBuffer buffer, final Row row, final int sheetNumber) {
		final ExctractHashTableData extract=new ExctractHashTableData();
		sets = extract.fillHashTables(row, sheetNumber, sets);
		for (final Cell cell : row) {
			final String cellContents = String.valueOf(cell);
			if (cellContents.indexOf(',') != -1) {
				cellContents.replace(',', ' ');
			}
			buffer.append(cellContents + ";");
		}
		buffer.append("\n");
	}

	/**
	 * This method is called when rows in the base data table are being added.
	 * Each row is turned into a BaseData object.
	 * 
	 * @param recordObject
	 *            = This object is to be populated with the row data.
	 * @param row
	 *            = The current row being iterated over.
	 */
	public void storeRowDataInRecordObjectBaseData(final BaseData recordObject, final Row row) {
		try{
			final Date date = HSSFDateUtil.getJavaDate(row.getCell(0).getNumericCellValue());
			final String newstring = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(date);
			recordObject.setDate(newstring);
		}
		catch(RuntimeException incorrectDateFormat){
			recordObject.setDate("Not a Date");
		}


		for (int i = 1; i < 14; i++) {
			row.getCell(i).setCellType(Cell.CELL_TYPE_STRING);
		}
		recordObject.setEventId(String.valueOf(row.getCell(1)));
		recordObject.setFailureClass(String.valueOf(row.getCell(2)));
		recordObject.setUeType(String.valueOf(row.getCell(3)));
		recordObject.setMarket(String.valueOf(row.getCell(4)));
		recordObject.setOperator(String.valueOf(row.getCell(5)));
		recordObject.setCellId(String.valueOf(row.getCell(6)));
		recordObject.setDuration(String.valueOf(row.getCell(7)));
		recordObject.setCauseCode(String.valueOf(row.getCell(8)));
		recordObject.setNeVersion(String.valueOf(row.getCell(9)));
		recordObject.setImsi(String.valueOf(row.getCell(10)));
		recordObject.setHier3id(String.valueOf(row.getCell(11)));
		recordObject.setHier32id(String.valueOf(row.getCell(12)));
		recordObject.setHier321id(String.valueOf(row.getCell(13)));
	}

	private void adjustCsvPathPerThisLoop(final String address,final  int sheetNumber) {
		csvFilePath=address + "\\Temp\\CSV\\";
		setCsvFilePath(csvFilePath = csvFilePath + sheetNumber + ".csv");
	}

	public boolean isRunCsvToData() {
		return runCsvToData;
	}

	public void setRunCsvToData(final boolean runCsvToData) {
		this.runCsvToData = runCsvToData;
	}
	/**
	 * Get the csv file path value;
	 * 
	 * @return
	 */
	public String getCsvFilePath() {
		return csvFilePath;
	}

	/**
	 * Set the excel file path value;
	 * 
	 * @return
	 */
	public void setCsvFilePath(final String csvFilePath) {
		this.csvFilePath = csvFilePath;
	}

	public List<BaseData> getErroneousDataList() {
		return erroneousDataList;
	}

	public void setErroneousDataList(final List<BaseData> erroneousDataList) {
		this.erroneousDataList = erroneousDataList;
	}
	public String getErrorLogPath() {
		return errorLogPath;
	}

	public void setErrorLogPath(final String errorLogPath) {
		this.errorLogPath = errorLogPath;
	}
	public void setBase_dir(final String base_dir) {
		this.base_dir = base_dir;
	}
}
