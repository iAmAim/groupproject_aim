package com.ait.importFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.xssf.usermodel.XSSFSheet;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelFileSheetSetup {

	/**
	 * This method sets up the ApachPOI required to analyse the data and returns the sheet to
	 * be iterated over.
	 * 
	 * @param sheetNumber = The excel sheet number (tabs on bottom)
	 * @param filetoConvertToCSV = The incoming excel file.
	 * @return
	 * @throws IOException 
	 */
	public Sheet excelFileSetup(final int sheetNumber,final FileInputStream filetoConvertToCSV,final String path) throws IOException{
        HSSFWorkbook workbook;
        HSSFSheet sheet = null;
        
        final File file = new File(path);
        final String fileName = file.getName();
        final String fileType = FilenameUtils.getExtension(fileName);
        if("xls".equals(fileType)){
        	workbook = new HSSFWorkbook(filetoConvertToCSV);
        	sheet = workbook.getSheetAt(sheetNumber);
        	return sheet;
        }
        return sheet;
        
	}
}

