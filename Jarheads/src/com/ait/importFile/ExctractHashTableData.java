package com.ait.importFile;

import org.apache.poi.ss.usermodel.Row;
import com.ait.consistencycheck.ValidDataHashSets;

public class ExctractHashTableData {
	
	public ValidDataHashSets fillHashTables(final Row row,final int sheetNumber,final ValidDataHashSets sets){
		switch(sheetNumber){
			case 1:{
				final String cellCauseCode=String.valueOf(row.getCell(0));
				final Integer intCauseId= new Double(cellCauseCode).intValue();
				
				final String cellEventId=String.valueOf(row.getCell(1));
				final Integer intEventId= new Double(cellEventId).intValue();
				
				sets.populateEventIdCauseCodeHashSet(intCauseId, intEventId);
				return sets;
			}
			case 2:{
				final String cellFailureClass=String.valueOf(row.getCell(0));
				final Integer intFailureClass= new Double(cellFailureClass).intValue();
				
				sets.populateFailureClassHashSet(intFailureClass);
				return sets;
			}
			case 3:{
				final String cellTAC=String.valueOf(row.getCell(0));
				final Integer intTAC= new Double(cellTAC).intValue();
				
				sets.populateUETypeHashSet(intTAC);
				return sets;
			}
			case 4:{
				final String cellMarket=String.valueOf(row.getCell(0));
				final Integer intMarket= new Double(cellMarket).intValue();
				
				final String cellOperator=String.valueOf(row.getCell(1));
				final Integer intOperator= new Double(cellOperator).intValue();
		
				sets.populateMCC_MNCHashSet(intMarket, intOperator);
				return sets;
			}
			default:{
				return sets;
			}
		}
		
	}

}
