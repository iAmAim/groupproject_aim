package com.ait.importFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@LocalBean
public class HasFileAlreadyBeenImported {
	
	@PersistenceContext
	public  EntityManager emDuplicateChecker;
	
	@EJB
	ImportEntityManagerFunctions entityFunctions;
	
	public Set<String> keysInDatabase;
	public boolean getFromDatabase=true;
	
	public boolean hasTheFileBeenImportedBefore(String fileName) throws IOException{
		final Path file2 = Paths.get(fileName);
		final BasicFileAttributes view = Files.getFileAttributeView(file2, BasicFileAttributeView.class).readAttributes();
		final Path pathOfFile = Paths.get(fileName); 
		fileName = pathOfFile.getFileName().toString();
		final  String fileKey=fileName+view.creationTime();
		System.out.println(fileKey);
		if(getFromDatabase){
			keysInDatabase=entityFunctions.getListOfAlreadyImportedFiles();
		}
		if(keysInDatabase.contains(fileKey)){
			return false; 
		}
		else{
			final FileDetails thisFileHasNeverBeenImported=new FileDetails(fileKey);
			entityFunctions.setUpForHasFileAlreadyBeenImportedFunctions(thisFileHasNeverBeenImported);
			entityFunctions.addFileKeyToImportedAlreadyTable();
			return true;
		}
		
	}

	public void setBoolean(final boolean flag){
		this.getFromDatabase=flag;
	}

	public void setKeysInDatabase(final Set<String> keysInDatabase) {
		this.keysInDatabase = keysInDatabase;
	}

}
