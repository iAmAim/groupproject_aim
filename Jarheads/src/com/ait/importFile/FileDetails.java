package com.ait.importFile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name ="file_details")
public class FileDetails {
	
	public FileDetails(){
		
	}
	public FileDetails(final String nameAndDate){
		this.nameAndDate=nameAndDate;
	} 
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int fileId;
	
	@Column(name="nameAndDate")
	private String nameAndDate;
	
	public int getFileId() {
		return fileId;
	}
	public void setFileId(final int fileId) {
		this.fileId = fileId;
	}
	public String getName() {
		return nameAndDate;
	}
	public void setName(final String nameAndDate) {
		this.nameAndDate = nameAndDate;
	}

}
