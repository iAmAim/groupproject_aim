package com.ait.importFile;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@LocalBean
public class ImportEntityManagerFunctions {
	
	@PersistenceContext
	public  EntityManager emConvert;
	
	
	public FileDetails fileToAdd;
	public String csvFilePath;
	public String tableName;
	public FileDetails dummyFile;
	
	/**
	 * This method writes the contents of a csv file to the database.
	 */
	public void fillDatabaseWithCSVusingPersitenceJPA() {
		csvFilePath = csvFilePath.replace('\\', '/');  
		emConvert
				.createNativeQuery(
						" LOAD DATA INFILE '" + csvFilePath + "'" + " IGNORE INTO TABLE " + tableName
								+ " FIELDS TERMINATED BY ';' ESCAPED BY '\'" + " LINES TERMINATED BY '\n' "
								).executeUpdate();
	}
	public void clearDatabase(){
		emConvert.createQuery("DELETE FROM BaseData").executeUpdate();
		emConvert.createQuery("DELETE FROM FileDetails ").executeUpdate();
		final FileDetails thisFileHasNeverBeenImported=new FileDetails("dummyFile");
		setUpForHasFileAlreadyBeenImportedFunctions(thisFileHasNeverBeenImported);
		emConvert.persist(this.fileToAdd);
		System.out.println("DATABASE CLEARED");
	}
	public void fillDatabaseWithCSVusingPersitenceJPABase(final boolean manualImport) { 
		if(manualImport){
			emConvert.createQuery("DELETE FROM BaseData ").executeUpdate();
		}
		csvFilePath = csvFilePath.replace('\\', '/');
		emConvert
				.createNativeQuery(
						" LOAD DATA INFILE '"
								+ csvFilePath
								+ "'"
								+ " IGNORE INTO TABLE "
								+ tableName
								+ " FIELDS TERMINATED BY ';' ESCAPED BY '\'"
								+ " LINES TERMINATED BY '\n' "
								+ "(date_Time,event_ID,@failure_Class,UE_Type,market,operator,cell_ID,duration,cause_Code,NE_Version,IMSI,"
								+ "HIER3_ID,HIER32_ID,HIER321_ID)set callfailure_ID=null ,"
								+ "failure_Class = if(@failure_Class='',NULL,@failure_Class);").executeUpdate();
	}
	public void addFileKeyToImportedAlreadyTable(){
		emConvert.persist(this.fileToAdd);
	}
	@SuppressWarnings("unchecked")
	public Set<String> getListOfAlreadyImportedFiles(){
		final Query query = emConvert.createQuery("Select f from FileDetails f ");
		final List<FileDetails> listOfFilesAlreadyImported=(List<FileDetails>) query.getResultList();
		final Set<String> keysInDatabase=new HashSet<String>();
		keysInDatabase.add("dummy");
		for(final FileDetails filesInDB : listOfFilesAlreadyImported){
			keysInDatabase.add(filesInDB.getName());
		}
		return keysInDatabase;
	}
	public void setUpForImortFromExcelToDatabaseFunctions(final String tableName,final String csvFilePath){
		this.tableName=tableName;
		this.csvFilePath=csvFilePath;
	}
	public void setUpForHasFileAlreadyBeenImportedFunctions(final FileDetails fileToAdd){
		this.fileToAdd=fileToAdd;
	}

}
