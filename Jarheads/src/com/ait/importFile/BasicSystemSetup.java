package com.ait.importFile;

import java.io.File;

public class BasicSystemSetup {

	private String addressBase;
	private final static String addressTemp="\\Temp";
	private final static String addressCSV="\\Temp\\CSV";
	private final static String addressErrorLogs="\\Temp\\ErrorLogs";
	private final static String addressImport="\\Temp\\Import";
	private final static String addressTest="\\Temp\\Test";
	private final static String addressAuto="\\Temp\\AutoImport";
	private final static String addressCopies="\\Temp\\Copies";

	public void setBaseAddress(final String Base){
		this.addressBase=Base;
	}
	public void runChecks(){
		buildIfNotThereTemp();
		buildIfNotThereCSV();
		buildIfNotThereErrorLogs();
		buildIfNotThereImport();
		buildIfNotThereTest();
		buildIfNotThereAuto();
		buildIfNotThereAutoTemp();
		buildIfNotThereAutoCSV();
		buildIfNotThereAutoCopies();
		buildIfNotThereAutoErrorLog();
	}
	private void buildIfNotThereTemp(){
		final File doesFileExist = new File(addressBase+addressTemp);
		if (!doesFileExist.isDirectory()) {
			new File(addressBase+addressTemp).mkdir();
			new File(addressBase+addressCSV).mkdir();
			new File(addressBase+addressErrorLogs).mkdir();
			new File(addressBase+addressImport).mkdir();
			new File(addressBase+addressTest).mkdir();
		}
	}
	private void buildIfNotThereCSV(){
		final File doesFileExist = new File(addressBase+addressCSV);
		if (!doesFileExist.isDirectory()) {
			new File(addressBase+addressCSV).mkdir();
		}
	}
	private void buildIfNotThereErrorLogs(){
		final File doesFileExist = new File(addressBase+addressErrorLogs);
		if (!doesFileExist.isDirectory()) {
			new File(addressBase+addressErrorLogs).mkdir();
		}
	}
	private void buildIfNotThereImport(){
		final File doesFileExist = new File(addressBase+addressImport);
		if (!doesFileExist.isDirectory()) {
			new File(addressBase+addressImport).mkdir();
		}
	}
	private void buildIfNotThereTest(){
		final File doesFileExist = new File(addressBase+addressTest);
		if (!doesFileExist.isDirectory()) {
			new File(addressBase+addressTest).mkdir();
		}
	}
	private void buildIfNotThereAuto(){
		final File doesFileExist = new File(addressBase+addressAuto);
		if (!doesFileExist.isDirectory()) {
			new File(addressBase+addressAuto).mkdir();
		}
	}
	private void buildIfNotThereAutoTemp(){
		final File doesFileExist = new File(addressBase+addressAuto+addressTemp);
		if (!doesFileExist.isDirectory()) {
			new File(addressBase+addressAuto+addressTemp).mkdir();
		}
	}
	private void buildIfNotThereAutoCSV(){
		final File doesFileExist = new File(addressBase+addressAuto+addressCSV);
		if (!doesFileExist.isDirectory()) {
			new File(addressBase+addressAuto+addressCSV).mkdir();
		}
	}
	private void buildIfNotThereAutoCopies(){
		final File doesFileExist = new File(addressBase+addressAuto+addressCopies);
		if (!doesFileExist.isDirectory()) {
			new File(addressBase+addressAuto+addressCopies).mkdir();
		}
	}
	private void buildIfNotThereAutoErrorLog(){
		final File doesFileExist = new File(addressBase+addressAuto+addressErrorLogs);
		if (!doesFileExist.isDirectory()) {
			new File(addressBase+addressAuto+addressErrorLogs).mkdir();
		}
	}
	
	
	
}
