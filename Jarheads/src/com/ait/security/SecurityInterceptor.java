package com.ait.security;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.ext.Provider;

import org.jboss.resteasy.annotations.interception.ServerInterceptor;
import org.jboss.resteasy.core.Headers;
import org.jboss.resteasy.core.ResourceMethod;
import org.jboss.resteasy.core.ServerResponse;
import org.jboss.resteasy.spi.Failure;
import org.jboss.resteasy.spi.HttpRequest;
import org.jboss.resteasy.spi.interception.PreProcessInterceptor;
import org.jboss.resteasy.util.Base64;

import com.ait.dataAccessObject.UserDAO;
import com.ait.models.User;

/**
 * This interceptor verify the access permissions for a user based on username
 * and passowrd provided in request
 * */
@Provider
@ServerInterceptor
@Stateless
@LocalBean
@PersistenceContext(name = "testpoi")
public class SecurityInterceptor implements PreProcessInterceptor {
	private static final String AUTHORIZATION_PROPERTY = "Authorization";
	private static final String AUTHENTICATION_SCHEME = "Basic";
	private static final ServerResponse ACCESS_DENIED = new ServerResponse(
			"You do not have enough priveleges to access this resource.", 401, new Headers<Object>());;
	private static final ServerResponse INVALID_CREDENTIALS = new ServerResponse("Credentials entered are invalid.",
			401, new Headers<Object>());;
	private static final ServerResponse ACCESS_FORBIDDEN = new ServerResponse("Access to this resource is forbidden.",
			403, new Headers<Object>());;
	private static final ServerResponse SERVER_ERROR = new ServerResponse("INTERNAL SERVER ERROR", 500,
			new Headers<Object>());;

	@Override
	public ServerResponse preProcess(final HttpRequest request,final  ResourceMethod methodInvoked) throws Failure, WebApplicationException {
		final Method method = methodInvoked.getMethod();
		// Access allowed for all
		if (method.isAnnotationPresent(PermitAll.class)) {
			return null;
		}
		// Access denied for all
		if (method.isAnnotationPresent(DenyAll.class)) {
			return ACCESS_FORBIDDEN;
		}
		// Get request headers
		final HttpHeaders headers = request.getHttpHeaders();
		// Fetch authorization header
		final List<String> authorization = headers.getRequestHeader(AUTHORIZATION_PROPERTY);
		// If no authorization information present; block access
		if (authorization == null || authorization.isEmpty()) {
			return ACCESS_DENIED;
		}
		// Get encoded username and password
		final String encodedUserPassword = authorization.get(0).replaceFirst(AUTHENTICATION_SCHEME + " ", "");
		// Decode username and password
		String usernameAndPassword;
		try {
			usernameAndPassword = new String(Base64.decode(encodedUserPassword));
		}
		catch (IOException e) {
			return SERVER_ERROR;
		}
		// Split username and password tokens
		final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
		final String username = tokenizer.nextToken();
		final String password = tokenizer.nextToken();
		// Verifying Username and password
		System.out.println("Username is: " + username);
		System.out.println("Password is: " + password);
		// Verify role-based access per web service
		if (method.isAnnotationPresent(RolesAllowed.class)) {
			final RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
			final Set<String> rolesSet = new HashSet<String>(Arrays.asList(rolesAnnotation.value()));
			final User user = findUser(username, password);
			if (!isUserValid(user)) {
				return INVALID_CREDENTIALS;
			}
			if (!isUserAllowed(user, rolesSet)) {
				return ACCESS_DENIED;
			}
		}
		// Return null to continue request processing
		return null;
	}

	private boolean isUserAllowed(final User user, final Set<String> rolesSet) {
		final String userRole = user.getUserType().getName();
		return rolesSet.contains(userRole) ? true : false;
	}

	private boolean isUserValid(final User user) {
		return user != null ? true : false;
	}

	private User findUser(final String username,final  String password) {
		UserDAO dao;
		User user = null;
		try {
			dao = (UserDAO) new InitialContext().lookup("java:module/UserDAO");
			user = (User) dao.findUserByUserNameAndPassword(username, password);
		}
		catch (NamingException e) {
			e.printStackTrace();
		}
		return user;
	}

}
